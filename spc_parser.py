import spacy
from words2num import w2n

nlp = spacy.load("en_core_web_sm")


def get_np(query):
    doc = nlp(query)
    return [chunk.text for chunk in doc.noun_chunks]


def get_sentences(query):
    doc = nlp(query)
    return [x.text for x in doc.sents]


def analyze(query):
    doc = nlp(query)
    data = {
        "sentences": [x.text for x in doc.sents],
        "token": [token.text for token in doc],
        "stop": [token.text.lower() for token in doc if token.is_stop],
        "nouns": [token.text for token in doc if token.pos_ == "NOUN" or token.pos_ == "PROPN"],
        "nouns_phrases": [chunk.text for chunk in doc.noun_chunks],
        "verbs": [token.lemma_ for token in doc if token.pos_ == "VERB"],
        "idx_verbs": [token.idx for token in doc if token.pos_ == "VERB"],
        "numbers": [],
        "numbers2": [],
        "all_numbers": [token.lemma_ for token in doc if token.pos_ == "NUM"],
        "entities": [{"entity": entity.label_,
                      "word": entity.text,
                      "start": entity.start,
                      "end": entity.end
                      } for entity in doc.ents]
    }
    data["dates"] = [x for x in data["entities"] if x["entity"] == "DATE"]

    for ent in data["entities"]:
        if ent.get("entity") == "CARDINAL" and not ent.get("word").isnumeric():
            num = w2n(ent.get("word"))
            data["numbers"].append(str(num))
        elif ent.get("entity") == "CARDINAL":
            data["numbers"].append(ent.get("word"))

    numbers = [x["word"].split(" ") for x in data["entities"] if x["entity"] == "DATE"]
    numbers2 = []
    for n2 in numbers:
        number = ""
        for nw in n2:
            if nw in data["all_numbers"]:
                number += " " + nw
        if len(number) > 0:
            numbers2.append(number.strip())

    for num in numbers2:
        if not num.isnumeric():
            num = w2n(num)
        data["numbers2"].append(str(num))

    entities = []
    for ent in data["entities"]:
        if ent.get("entity") != "CARDINAL" and ent.get("entity") != "DATE" and len(ent.get("word")) < 4:
            entities.append(ent)

    print(entities)

    data["entities"] = entities
    return data
