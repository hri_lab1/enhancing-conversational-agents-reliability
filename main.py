import json
import os

from datetime import datetime, timedelta
from typing import Annotated, Union

import uvicorn
from fastapi import Depends, FastAPI, HTTPException, status, Request
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel

from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse


from pass_utility import load_json

from query import query


# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 60


users_db = load_json("users.json")


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Union[str, None] = None


class User(BaseModel):
    username: str
    email: Union[str, None] = None
    full_name: Union[str, None] = None
    disabled: Union[bool, None] = None


class UserInDB(User):
    hashed_password: str

# Aidabot
class Item(BaseModel):
    txt: str


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


app = FastAPI()
folder = "./bot/"


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(db, username: str):
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)


def authenticate_user(_users_db, username: str, password: str):
    user = get_user(_users_db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: Union[timedelta, None] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(users_db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(
    current_user: Annotated[User, Depends(get_current_user)]
):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


@app.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
):
    user = authenticate_user(users_db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/users/me/", response_model=User)
async def read_users_me(
    current_user: Annotated[User, Depends(get_current_active_user)]
):
    return current_user


@app.get("/users/me/items/")
async def read_own_items(
    current_user: Annotated[User, Depends(get_current_active_user)],
        name: str=""
):
    return [{"item_id": name, "owner": current_user.username}]


@app.get("/api2")
async def read_item(
                    current_user: Annotated[User, Depends(get_current_active_user)],
                    cmd: str = "", sub: str = "", obj: str = "", ins: str = "",
                    obj2: str = "", ins2: str = "", obj3: str = "", ins3: str = "",
                    ord: str = "", num: str = "", lng: str = "en-US", old: str = "",
                    ai: str = "", exp: str = ""):

    expand = not exp == ""

    result = query(cmd, sub, obj, ins, ord, num, lng, ins2, obj2, ins3, obj3, old, ai=ai, cu=current_user, expand=expand)
    return json.loads(result)


@app.get("/api")
async def read_item(
                    cmd: str = "", sub: str = "", obj: str = "", ins: str = "",
                    obj2: str = "", ins2: str = "", obj3: str = "", ins3: str = "",
                    ord: str = "", num: str = "", lng: str = "en-US", old: str = "", exp: str = ""
                    ):
    expand = not exp == ""
    result = query(cmd, sub, obj, ins, ord, num, lng, ins2, obj2, ins3, obj3, old, expand=expand)
    return json.loads(result)


# serve static pages
app.mount("/bot", StaticFiles(directory="./bot/"), name="static")


@app.get("/{catchall:path}", response_class=FileResponse)
def read_index(req: Request):
    # check first if requested file exists
    path = req.path_params["catchall"]
    if len(path) == 0:
        path = "index.html"
    file = folder + path

    # print('look for: ', path, file)
    if os.path.exists(file):
        return FileResponse(file)

    # otherwise return index files
    index_file = 'bot/index.html'
    return FileResponse(index_file)


if __name__ == '__main__':
    uvicorn.run(app='main:app', host='0.0.0.0', port=80)