var c_heigth;
var p_heigth;
var normal_adjust = setInterval(adjust, 1000);
var position = [0, 0];
var settings_visible = false
var help_visible = false


// Make the DIV elements draggable:
dragElement(document.getElementById("starbot"));
dragElement(document.getElementById("templates"));

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        // if present, the header is where you move the DIV from:
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        // otherwise, move the DIV from anywhere inside the DIV:
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

var getJSON = function (url, callback) {
    xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};


function user(answer) {
    msg = '<div class="user_input"><img class="user" src="user.png" alt="user" ><p class="dialogue_text">';
    msg += answer + '</p></div>';
    var dialogue = document.getElementById("dialogue");
    dialogue.innerHTML += msg;
    adjust(true)
}

function bot(answer) {
    msg = '<div class="starbot"><img class="bot" src="bot.png" alt="Starbot"><p class="dialogue_text">';
    msg += answer + '</p></div>';
    var dialogue = document.getElementById("dialogue");
    dialogue.innerHTML += msg;
    adjust(true)
    if(speaker){
        var to_say = answer.split(". ");
        for (phrase of to_say){
            if(phrase.indexOf("http")<0){
                say(phrase);
            }
        } 
    }
}

function adjust(force) {
    var local_c_heigth = document.getElementById("dialogue").clientHeight;
    var local_p_heigth = document.getElementById("starbot").clientHeight;
    var dialogue = document.getElementById('dialogue');
    dialogue.style.maxHeight = (p_heigth - 105) + "px";
    document.getElementById('help_div').style.maxHeight = (p_heigth - 95) + "px";
    document.getElementById('settings').style.maxHeight = (p_heigth - 95) + "px";
        
    if (local_c_heigth != c_heigth || local_p_heigth != p_heigth || force) {
        c_heigth = local_c_heigth;
        p_heigth = local_p_heigth;
        dialogue.scrollBy({
            top: 1000,
            behavior: 'smooth'
        });
    }
}

cycle = function () {
    var input_text = document.getElementById("user_input");
    //console.log(input_text.value)
    var query = input_text.value;
    if (query.length > 0) {
        user(query);
        input_text.value = "";
        getJSON('/starbot/?query=' + encodeURIComponent(query), function (err, data) {
            let answer;
            if (err !== null) {
                answer = "I'm sorry, but there was an error. Can you try another question?";
            } else {
                answer = data.answer
                if (data.url && answer.indexOf(data.url)>-1) {
                    answer = answer.replace(data.url, `<a target="_blank" href="${data.url}"> link </a>`)
                }
                if (data.url && (data.question_type == "99" || answer.indexOf(data.url)<0)){
                    answer = answer + ". For further details see: " + '<a target="_blank" href="' + data.url + '"> link </a>'
                    answer = answer.replace(".. ", ". ").replace(". .", ".")
                }
            }
            bot(answer);
        });
    }

}

function reduce() {
    var bot_div = document.getElementById('starbot');
    var h = window.innerHeight;
    let top, left;
    if (bot_div.style.top) {
        top = bot_div.style.top;
    } else {
        top = "400px"
    }

    if (bot_div.style.left) {
        left = bot_div.style.left;
    } else {
        left = "250px"
    }

    position = [top, left]
    bot_div.style.top = h - 32 + "px"
    bot_div.style.left = 96 + "px"

    //var starbot_header = document.getElementById('starbotheader');
    var starbot_footer = document.getElementById('starbotfooter');
    var dialogue = document.getElementById('dialogue');
    document.getElementById('enlarge').style.display = "flex";
    document.getElementById('reduce').style.display = "none";
    document.getElementById('show_settings').style.display = "none";
    document.getElementById('help').style.display = "none";
    //starbot_header.style.display = "none";
    dialogue.style.display = "none"
    starbot_footer.style.display = "none"
    bot_div.style.minHeight = "60px";
    bot_div.style.maxHeight = "60px";
    bot_div.style.maxWidth = "185px";
    bot_div.style.resize = "none";

}

function enlarge() {
    var bot_div = document.getElementById('starbot');
    bot_div.style.top = position[0];
    bot_div.style.left = position[1];
    var dialogue = document.getElementById('dialogue');
    var starbot_footer = document.getElementById('starbotfooter');
    bot_div.style.removeProperty('min-height');
    bot_div.style.removeProperty('max-height');
    bot_div.style.removeProperty('max-width');
    bot_div.style.removeProperty('resize');
    document.getElementById('enlarge').style.display = "none";
    document.getElementById('reduce').style.removeProperty('display');
    document.getElementById('show_settings').style.removeProperty('display');
    document.getElementById('help').style.removeProperty('display');
    dialogue.style.removeProperty('display');
    starbot_footer.style.removeProperty('display');
}

function settings() {
    var settings_div = document.getElementById('settings');
    var dialogue = document.getElementById('dialogue');
    if (settings_visible) {
        settings_div.style.display = "none";
        dialogue.style.removeProperty('display');
        document.getElementById('show_settings').src = 'settings.png';
        document.getElementById('restart').style.removeProperty('display');
        document.getElementById('reduce').style.removeProperty('display');
        document.getElementById('starbotfooter').style.removeProperty('display');
        document.getElementById('help').style.removeProperty('display');
        var new_activation = document.getElementById('activation').value;
        if(activation.indexOf(new_activation)<0){
            activation.push(new_activation)
            update_grammars();
        }
    } else {
        settings_div.style.display = "block";
        dialogue.style.display = "none";
        document.getElementById('show_settings').src = 'no_settings.png';
        document.getElementById('restart').style.display = "none";
        document.getElementById('reduce').style.display = "none";
        document.getElementById('starbotfooter').style.display = "none";
        document.getElementById('help').style.display = "none";
    }
    settings_visible = !settings_visible
}

function help() {
    var help_div = document.getElementById('help_div');
    var dialogue = document.getElementById('dialogue');
    if (help_visible) {
        help_div.style.display = "none";
        dialogue.style.removeProperty('display');
        document.getElementById('help').src = 'help.svg';
        document.getElementById('restart').style.removeProperty('display');
        document.getElementById('reduce').style.removeProperty('display');
        document.getElementById('starbotfooter').style.removeProperty('display');
        document.getElementById('show_settings').style.removeProperty('display');
    } else {
        help_div.style.display = "block";
        dialogue.style.display = "none";
        document.getElementById('help').src = 'no_help.svg';
        document.getElementById('restart').style.display = "none";
        document.getElementById('reduce').style.display = "none";
        document.getElementById('starbotfooter').style.display = "none";
        document.getElementById('show_settings').style.display = "none";
    }
    help_visible = !help_visible
}

function restart(){
    document.getElementById('dialogue').innerHTML='<div class="starbot"><img class="bot" src="bot.png" alt="Starbot"><p class="dialogue_text">Welcome! I\'m StarBot, your personal assistant. What can I do for you?</p></div>';
    document.getElementById('starbot').style.removeProperty("height");
    document.getElementById('starbot').style.removeProperty("top");
    document.getElementById('starbot').style.removeProperty("left");
}

document.addEventListener("keypress", function (event) {
    //alert(event.code)
    if ((event.code == "Enter" || event.code == "NumpadEnter") && document.getElementById('user_input').value.length > 0) {
        cycle();
    }
});
