var session = {
	'level': 0,
	'intent': { 'slots': {} },
	'audio': false,
	'recognition': false,
	'recon': { listening: false, activated: false },
	'help_status': 0,
	'confirmation': true,
	'token': "",
	'settings': false,
	'templates': true,
	'recycle': false,
	'exp': false,
	'turn': true,
	"in_progress": false
};

var json_call;

//data server IP address
const api = '/api2?';

var audio_recognitions_enabled = false

if (window.location.href.indexOf('https') > -1) {
	audio_recognitions_enabled = true
}

millisec_to_timeout = 90000

var systemTimeout = [];
var mic_image = 'microphone-on1.svg';
var mic_animation;
var activation = ['ehi computer', 'computer', "aidabot"]

const tags_list = ['<br/>', '<b>', '</b>', '<br/>', '<ul>', '</ul>', '<li>', '</li>', '<i>', '</i>', '</a>'];
const clear_msg = ["clear", "Clear", "CLEAR", "restart", "Restart", "RESTART"];
const grammar_expansion = ["expand your grammar", "activate your grammar", "switch the grammar", "switch grammar"]


const templates = {
	WELCOME_MSG: ['<b>Hello!</b> You can ask me to <b>describe</b>, <b>compare</b>, <b>list</b>, or <b>count</b> any scholarly entity.', 'Welcome, you can ask me to <b>describe</b>, <b>compare</b>, <b>list</b>, or <b>count</b> any scholarly entity. What would you like to try?'],
	HELLO_MSG: ['Hello! What can I do for you?', 'Hi! what could i do for you?'],
	OK_MSG: 'Ok',
	HELP_MSG: ['You can ask to <b>count</b> or <b>list</b> <i>authors</i>, <i>papers</i>, <i>conferences</i>, <i>organizations</i>, <i>topics</i> and <i>citations</i> or to <b>describe</b> and <b>compare</b> <i>authors</i>, <i>conferences</i> or <i>organizations</i>. Start a query with <b>list</b>, <b>count</b>, <b>describe</b> or <b>compare</b>.', 'You can ask a query starting with <b>count</b>, <b>list</b>, <b>describe</b> or <b>compare</b>.'],
	GOODBYE_MSG: ['Goodbye!', 'Bye!', 'See you later!'],
	FALLBACK_MSG: 'Sorry, I don\'t know that. Please try again.',
	ERROR_MSG: ["Unfortunately, I have no information on that.",
		"I apologize, but I couldn't find anything relevant to your query.",
		"That's a tough one. I searched everywhere but came up empty-handed.",
		"No luck. I tried my best but there's nothing about it.",
		"I'm sorry to disappoint you, but I have no clue about that.",
		"I wish I could help you, but that's beyond my knowledge.",
		"I feel bad for not knowing, but that's out of my scope.",
		"I regret to inform you that I have no idea about it"],
	REPROMPT_MSG: 'So, what would you like to ask?',
	TIMEOUT_MSG: ["Please accept my apologies, but the servers are currently experiencing a high volume of requests. I would appreciate it if you could come back in a few minutes.",
		" I'm sorry for the inconvenience, but the servers are overloaded right now. Please try again in a few minutes.",
		" Due to high demand, the servers are temporarily unavailable. I apologize for the trouble and ask you to return in a few minutes.",
		"The servers are currently busy handling many requests. I regret the disruption and hope you can come back in a few minutes.",
		"There is a high volume of requests and the servers are not responding. I'm very sorry for the issue and request you to come back in a few minutes."]
}

function get_negative_answer() {
	const negative_answers = templates.TIMEOUT_MSG
	return negative_answers[Math.floor(Math.random() * negative_answers.length)]
}

//carica messaggio di benvenuto all'avvio
$(document).ready(function () {
	resize();
	setAnimation()
	setTimeout(function () {
		setMessage('WELCOME_MSG');
		if (session.token == "") {
			get_token();
			resize();
		}
	}, 300);
});

//ciclo principale determinato dalla pressione del tasto invio (input utente)
$(document).on('keyup', function (e) {
	if (e.which == 13 && $('#user_input').val().length > 0 && session.token != "") {
		cycle();
	}

	if (e.which == 13 && $('#uname').val().length > 0 && $('#psw').val().length == 0 && session.token == "") {
		document.getElementById("psw").focus();
		return
	}

	if (e.which == 13 && $('#uname').val().length > 0 && $('#psw').val().length > 0 && session.token == "") {
		login();
		return
	}

	if (e.which == 13 && $('#uname').val().length == 0 && session.token == "") {
		document.getElementById("uname").focus();
		return
	}

	let go_button_hide = setInterval(function () {
		if ($('#user_input').val().length > 0) {
			$('#go_button').show()
			$('#mic').hide();
		} else {
			$('#go_button').hide()
			$('#mic').show();
			clearInterval(go_button_hide)
		}
	}, 300);
});

//aggiorna la dimensione della chat quando viene ridimensionata la finestra del browser
$(window).on("resize", function () {
	resize();
}).resize();

function screen_scroll() {
	$('#bot').scrollTop($('#bot')[0].scrollHeight - $('#bot')[0].clientHeight);
}

function resize() {
	setTimeout(function () {
		let height = $(window).height() - $('.top').height() - $('.input').height() - 20;
		if (session.help_status > 0) {
			height += - $('.help_text').height() - 20
		}
		if (session.settings) {
			height += - $('#settings').height();
		}
		$('#bot').height(height);

		let login_div = document.getElementById("auth");
		login_div.style.top = $(window).height() / 2 + "px"
		login_div.style.left = $(window).width() / 2 + "px"
		if ($(window).width() < 1200) {
			document.getElementById("templates").style.display = "none";
			
		} else {
			$("#templates").show();
			if (!session.templates) {
				let div_t = document.getElementById("in_t");
				let temp_div = document.getElementById("templates");
				div_t.style.display = "none";
				temp_div.style.top = "35px";
				temp_div.style.left = $(window).width() / 2 + "px"
				temp_div.style.width = "200px";
			} else {
				document.getElementById("close_templates").style.display = "inline";
				document.getElementById("templates").style.width = "400px"
			}
		}

	}, 500);
}


//ciclo principale
function cycle() {
	let msg;

	if (session.recycle) {
		msg = session.original_input
	} else {
		if ($('#user_input').val().length == 0) {
			return
		}
		msg = $('#user_input').val();
		$('#user_input').val("");
		setUserMessage(msg);
		if (session.in_progress) {
			return
		}
	}

	session.original_input = msg;

	if (clear_msg.indexOf(msg) > -1) {
		$('#bot').text("");
		$('.help_text').empty();
		resize();
		setMessage('WELCOME_MSG');
		return
	}

	if (session.level == 0) {
		let url_to_call = '/api2?cmd=parse'
		url_to_call += '&ins=' + encodeURIComponent(msg);
		$.ajaxSetup({
			headers: {
				'Authorization': 'Bearer ' + session.token
			}
		});
		session.in_progress = true;
		json_call = $.ajax({
			url: url_to_call,
			success: function (data) {
				session.recycle = false;
				session.in_progress = false;
				var answer;
				//console.log(data)
				if (data.query && data.query.intent && data.query.intent == "open") {
					if (data.query.biblio && data.query.biblio.length > 0) {
						let biblio = data.query.biblio.join("</li><li>")
						answer = data.query.answer + "<br> Bibliografy: <ul><li>" + biblio + "</li></ul>"
					} else if (data.query.html_answer) {
						if (data.query.source && data.query.source.length > 0) {
							answer = data.query.html_answer + open_urls(data);
						} else {
							answer = data.query.html_answer;
						}
						answer = [answer, data.query.answer];
					} else {
						answer = data.query.answer;
					}
				} else if (!data.answer && data.query && data.query.html_answer && data.query.intent == "search") {
					answer = [data.query.html_answer, data.query.answer];
				} else if (!data.answer && data.query && data.query.html_answer && data.query.intent != "search") {
					data.query.html_answer = data.query.html_answer.replace("[$1]", "[$]");
					if (data.paper_urls) {
						data.query.html_answer = set_paper_url(data.query.html_answer, data.paper_urls);
					} else {
						data.query.html_answer = data.query.html_answer.replaceAll("[$]", "");
					}
					answer = [data.query.html_answer, data.query.answer];
				} else if (!data.answer && data.query && data.query.answer && typeof (data.query.answer) == "string" && !data.query.url) {
					answer = data.query.answer;
					if (data.query.source && data.query.source.length > 0) {
						answer += " - Source: " + data.query.source;
					}
				} else if (!data.answer && data.query && data.query.answer && data.query.answer.msg) {

					if (typeof (data.query.answer.msg) == "string") {
						answer = data.query.answer.msg;
					} else {
						answer = data.query.answer.msg[0];
					}

					session.level = 1;
					session.data = data.query.answer
					let items = Object.keys(data.query.answer)

					let new_items = [[], []]
					for (let item of items) {
						if (item != "msg" && item != "paper_urls") {
							new_item = item.split("-")
							if (new_items[0].indexOf(new_item[0]) < 0) {
								new_items[0].push(new_item[0])
							}
							if (new_items[1].indexOf(new_item[1]) < 0) {
								new_items[1].push(new_item[1])
							}
						}
					}
					session.data.keys = new_items

				} else if (!data.answer && data.query && data.query.answer && data.query.url && data.query.source && data.query.source.length > 0) {
					if (typeof (data.query.url) == "string") {
						answer = data.query.answer + " - " + '<a target="_blank" href="' + data.query.url + '"> Source: ' + data.query.source + '</a>';
					} else {
						let urls = []
						for (i in data.query.url) {
							let b = 1 * 1 + 1 * i
							let url = '<a target="_blank" href="' + data.query.url[i] + '"> url' + b + '</a>'
							urls.push(url)
						}
						answer = data.query.answer + " - Source: " + data.query.source
						if (urls.length > 0) {
							answer += " - Links: " + urls.join(", ");
						}
					}
				} else if (data.result && data.result == "ko") {
					answer = get_negative_answer();
				} else {
					answer = data.answer
				}
				appendMessage(answer);
			},
			error: function (e) {
				if (e.status == 401 || e.status == "Unauthorized") {
					answer = e.responseJSON.detail;
					session.token = "";
					session.recycle = true;
					session.in_progress = false;
					get_token();
					return
				} else {
					session.recycle = false;
					answer = get_negative_answer();
					session.in_progress = false;
				}
				appendMessage(answer);
			}
		});

	} else if (session.level == 1) {
		
		if (typeof (session.data.msg) == "string") {
			if (session.data[msg]) {
				session.data[msg] = session.data[msg].replace("[$1]", "[$]");

				if (session.data.paper_urls && session.data.paper_urls[msg]) {
					session.data[msg] = set_paper_url(session.data[msg], session.data.paper_urls[msg]);
				} else {
					session.data[msg] = session.data[msg].replaceAll("[$]", "");
				}

				answer = session.data[msg];
				session.level = 0;
				session.data = {};
			} else {
				answer = session.data.msg;
			}
			appendMessage(answer);
		} else {
			let keys = session.data.keys[0];
			if (keys.indexOf(msg) > -1) {
				session.level = 2
				answer = session.data.msg[1];
				session.data.choice = msg + "-";
			} else {
				answer = session.data.msg[0];
			}
			appendMessage(answer);
		}

	} else if (session.level == 2) {
		let keys = session.data.keys[1];
		if (keys.indexOf(msg) > -1) {
			session.level = 0;
			session.data[session.data.choice + msg] = session.data[session.data.choice + msg].replace("[$1]", "[$]");

			if (session.data.paper_urls[session.data.choice + msg]) {
				session.data[session.data.choice + msg] = set_paper_url(session.data[session.data.choice + msg], session.data.paper_urls[session.data.choice + msg]);
			} else {
				session.data[session.data.choice + msg] = session.data[session.data.choice + msg].replaceAll("[$]", "");
			}

			answer = session.data[session.data.choice + msg];
			session.data = {};
		} else {
			answer = session.data.msg[1];
		}
		appendMessage(answer);
	}

}

function help(cmd) {
	if (session.settings) {
		settings();
	}
	let body_height_ini = $('body').height();
	if (session.help_status == cmd) {
		$('.help_text').empty();
		cmd = 0;
	}
	else if (cmd == 1) {
		$('.help_text').empty();
		$('.help_text').append('<p class="help_text_p">The database can be queried about <b>authors</b>, <b>papers</b>, <b>conferences</b>, <b>organizations</b>, <b>citations</b>, <b>topics</b>, and <b>journals</b>.<br/>It is possible to further filter the queries by specifying up to three <b>names</b> of particular <b>topics</b>, <b>conferences</b>, <b>organizations</b>, <b>authors</b> or <b>journals</b>.<br/>The results can be sorted according to one of the following four options: <b>publications</b>, <b>citations</b>, <b>publications in the last 5 years</b>, <b>citations in the last 5 years</b><br/>There are four types of queries:<br/>1. <b>Describe</b> (e.g .: "describe ISWC")<br/>2. <b>Compare</b> (e.g .: "compare ISWC to ESWC")<br/>3. <b>Count</b> (e.g .: "count the papers on machine learning")<br/>4. <b>List</b> (e.g .: "list the top 5 conferences with papers on rdf graph sorted by publications").<br/>You can enter a query all at once in natural language. <br/>To listen to aidabot messages, enable the speech synthesizer (speaker icon at the top right). <br/>To use the voice recognition functions click on the microphone icon (bottom right).<br>You can use <b>clear</b> or <b>restart</b> to clear the screen and start a new session.</p><a class="right" onclick="help(0)" href="javascript:void(0)" title="close help">close</a>');
	}
	else if (cmd == 2) {
		$('.help_text').empty();
		$('.help_text').append('<p class="help_text_p"><b>Speech recognition features</b> are only available through <b>encrypted connections</b> (https) on <b>Google Chrome</b>, <b>Microsoft Edge</b>, and <b>Apple Safari</b>. <br/>The audio functions use the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API" target="_blank">Web Speech API</a> which is an experimental technology and may not work correctly depending on the compatibility of the browser used, therefore the options to activate them are only available on fully compatible browsers (Google Chrome, Microsoft Edge, Apple Safari: all functions - Mozilla Firefox: only the speech syntetizer).</p><a class="right" onclick="help()" href="javascript:void(0)" title="close help">close</a>');
	}
	else {
		$('.help_text').empty();
	}
	session.help_status = cmd;
	resize()
}


function session_reset() {
	let rec = session.recognition;
	let audio = session.audio;
	let recon = session.recon;
	let token = session.token;
	session = {
		'level': 0,
		'intent': { 'slots': {} },
		'audio': audio,
		'recognition': rec,
		'recon': recon,
		'help_status': 0,
		'confirmation': true,
		'token': token,
		'settings': false,
		'templates': true,
		'recycle': false
	};
}

function setUserMessage(msg) {
	if (session.turn) {
		return
	}
	session.turn = true;
	$('#thinker').remove();
	$('#bot').append('<div class="container"><img src="user.svg" alt="Avatar" class="right"><p>' + msg + '</p></div>')
	$('#bot').scrollTop($('#bot')[0].scrollHeight - $('#bot')[0].clientHeight);
	setAnimation();
	set_timeout();
}

function setAnimation() {
	$('#bot').append('<div class="container darker" id="thinker"><img src="spinner.gif" alt="Thinking" class="center" style="width:100%;"></div>')
	$('#bot').scrollTop($('#bot')[0].scrollHeight - $('#bot')[0].clientHeight);
}

function setMessage(msg, options) {
	let value = templates[msg];
	let speak = templates[msg];
	if (Array.isArray(value)) {
		value = value[Math.floor(Math.random() * value.length)];
		speak = value;
	}
	let lst = [];
	if (options) {
		let n = 0;
		let str = value;
		while (n >= 0) {
			n = str.indexOf("$", n + 1);
			let n1 = str.indexOf("}", n) - n + 1;
			let str1 = str.substr(n, n1);
			lst.push(str1);
		}
		lst.pop();
		for (let i in options) {
			if (Array.isArray(options[i])) {
				value = value.replace('${' + i + '}', options[i][0]);
				speak = speak.replace('${' + i + '}', options[i][1]);
			} else {
				value = value.replace('${' + i + '}', options[i]);
				speak = speak.replace('${' + i + '}', options[i]);
			}

		}
		for (let i in lst) {
			value = value.replace(lst[i], '');
		}
	}

	while (value.indexOf('  ') > -1) {
		value = value.replace('  ', ' ');
	}

	value = value.replaceAll(' . ', '. ');
	value = value.replaceAll(' , ', ', ');

	appendMessage([value, speak])
}

function appendMessage(value) {
	if (!session.turn) {
		return
	}
	session.turn = false;
	let speak = '';
	if (Array.isArray(value)) {
		speak = value[1];
		value = value[0];
	} else {
		speak = value;
	}
	$('#thinker').remove();
	$('#bot').append('<div class="container darker"><div class="logo"><img src="nao.svg" alt="Avatar"></div><div class="msg"><p>' + value + '</p></div></div>');
	$('#bot').scrollTop($('#bot')[0].scrollHeight - $('#bot')[0].clientHeight);
	say(speak);
	cancel_timeout()
}

function set_timeout() {
	let new_systemTimeout = setTimeout(function () {
		json_call.abort();
		setMessage("TIMEOUT_MSG");
		session_reset()
	}, millisec_to_timeout);
	systemTimeout.push(new_systemTimeout);
}

function cancel_timeout() {
	for (let i in systemTimeout) {
		clearTimeout(systemTimeout[i]);
	}
}


function fuzzy_search(list, string) {
	let fuse = new Fuse(list, { includeScore: true })
	let result = fuse.search(string)
	if (result.length > 0 && result[0].score < 0.5) {
		return result[0].item
	}
	return ''
}

function get_help() {
	setMessage('HELP_MSG');
}



/**
** Text to Speech part
*/
if (window.speechSynthesis) {
	var voiceSelect = document.getElementById('voices') //querySelector('select');
	var synth = window.speechSynthesis;
	var voices = [];

	function populateVoiceList() {
		voices = synth.getVoices().sort(function (a, b) {
			const aname = a.name.toUpperCase(), bname = b.name.toUpperCase();
			if (aname < bname) return -1;
			else if (aname == bname) return 0;
			else return +1;
		});
		var selectedIndex = voiceSelect.selectedIndex < 0 ? 0 : voiceSelect.selectedIndex;
		voiceSelect.innerHTML = '';
		for (i = 0; i < voices.length; i++) {
			var option = document.createElement('option');
			option.textContent = voices[i].name + ' (' + voices[i].lang + ')';

			if (voices[i].default) {
				option.textContent += ' -- DEFAULT';
			}
			option.setAttribute('data-lang', voices[i].lang);
			option.setAttribute('data-name', voices[i].name);
			if (voices[i].lang.indexOf('en-') > -1) {
				voiceSelect.appendChild(option);
			}
		}

		for (i = 0; i < voices.length; i++) {
			var option = document.createElement('option');
			option.textContent = voices[i].name + ' (' + voices[i].lang + ')';

			if (voices[i].default) {
				option.textContent += ' -- DEFAULT';
			}
			option.setAttribute('data-lang', voices[i].lang);
			option.setAttribute('data-name', voices[i].name);
			if (voices[i].lang.indexOf('it-') > -1) {
				voiceSelect.appendChild(option);
			}
		}

		voiceSelect.selectedIndex = selectedIndex;
	}

	populateVoiceList();

	if (speechSynthesis.onvoiceschanged !== undefined) {
		speechSynthesis.onvoiceschanged = populateVoiceList;
	}

	function speak() {
		if (document.getElementById('txt').value.length == 0) {
			return
		}
		say(document.getElementById('txt').value)
	}

	function say(msg) {
		if (!session.audio) {
			return
		}

		for (let i in tags_list) {
			msg = msg.replaceAll(tags_list[i], '');
		}

		while (msg.indexOf('<a') != -1) {
			let i = msg.indexOf('<a');
			let f = msg.indexOf('>', i);
			msg = msg.replace(msg.substring(i, +f + 1), '')
		}

		let sentences = split_msg(msg);

		for (s in sentences) {
			let voice_msg = new SpeechSynthesisUtterance(sentences[s]);
			voice_msg.rate = 1;
			var selectedOption = voiceSelect.selectedOptions[0].getAttribute('data-name');

			for (i = 0; i < voices.length; i++) {
				if (voices[i].name === selectedOption) {
					voice_msg.voice = voices[i];
					break;
				}
			}
			window.speechSynthesis.speak(voice_msg);
		}


	}

	function split_msg(msg) {
		let result = []
		let sentences = msg.split(". ");
		for (s of sentences) {

			if (s.length > 100) {
				let ss = s.split(": ")
				result = result.concat(ss)
			} else {
				result.push(s);
			}

		}
		sentences = result
		result = []
		for (s of sentences) {

			if (s.length > 100) {
				let ss = s.split(", ")
				result = result.concat(ss)
			} else {
				result.push(s);
			}

		}
		return result
	}

	function switch_audio() {
		if (session.audio) {
			$('#speaker').attr("src", "speaker_off.svg");
			$('#speaker').attr("title", "Click to unmute");
			speaker = false;
			//window.speechSynthesis.pause();
			window.speechSynthesis.cancel();
		} else {
			$('#speaker').attr("src", "speaker_on.svg");
			$('#speaker').attr("title", "Click to mute");
			speaker = true;
			if (window.speechSynthesis.paused) {
				window.speechSynthesis.resume()
			}
		}
		session.audio = !session.audio;
	}

}

/**
** Speech Recognition part
*/
if ((window.webkitSpeechRecognition || window.SpeechRecognition) && audio_recognitions_enabled) {

	var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
	var recognition = new SpeechRecognition();

	var grammars = [
		'#JSGF V1.0; grammar utterances; public <utterance> = ' + activation.join(' | ') + ' ',
		'#JSGF V1.0; grammar utterances; public <utterance> = hello | hi '
	]
	var g_dict = {};

	// Start listening process
	/*
	function interaction() {
		recon.listening = !recon.listening;
		if (recon.listening) {
			document.getElementById('button').src = 'stop_listening.png';
			recogn_start(grammars[0]);
		} else {
			recognition.abort();
			document.getElementById('button').src = 'start_listening.png';
		}
	}*/

	function toggleStartStop() {
		session.recon.listening = !session.recon.listening;
		if (session.recon.listening) {
			document.getElementById('mic_off').src = 'microphone-on.svg';
			document.getElementById('mic_off').title = "Click to stop voice recongnition"
			recogn_start(grammars[0]);
		} else {
			recognition.abort();
			document.getElementById('mic_off').src = 'microphone-off.svg';
			document.getElementById('mic_off').title = "Click to start voice recongnition"
		}
	}

	function recogn_start(grammar) {
		if (!session.recon.listening) {
			recognition.abort();
			return
		}

		var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
		var speechRecognitionList = new SpeechGrammarList();
		speechRecognitionList.addFromString(grammar, 1);
		recognition.grammars = speechRecognitionList;
		recognition.continuous = false;
		recognition.lang = document.getElementById("rec_lang").options[document.getElementById("rec_lang").selectedIndex].value;
		recognition.interimResults = false;
		recognition.maxAlternatives = 1;

		// listening for user command
		if (session.recon.activated) {

			// fired on result
			recognition.onresult = function (event) {
				let result = event.results[0][0].transcript;
				let search = fuzzy_search(expand(grammar), result);
				document.getElementById('user_input').value = result;
				cycle();
			}

			// fired on sound end
			recognition.onend = function () {
				setTimeout(function () {
					recognition.lang = document.getElementById("rec_lang").options[document.getElementById("rec_lang").selectedIndex].value;
					session.recon.activated = false
					listening();
					recogn_start(grammars[0]);
				}, 300);
			}

		}

		// listening for activation words
		else {

			// fired on result
			recognition.onresult = function (event) {
				let result = event.results[0][0].transcript;
				let search = fuzzy_search(expand(grammar), result);
				if (activation.indexOf(search) != -1) {
					session.recon.activated = true;
					listening();
				}
			}

			// fired on sound end
			recognition.onend = function () {
				setTimeout(function () {
					recognition.lang = document.getElementById("rec_lang").options[document.getElementById("rec_lang").selectedIndex].value;
					if (session.recon.activated) {
						recogn_start(grammars[1]);
					} else {
						recogn_start(grammars[0]);
					}

				}, 300);
			}
		}

		recognition.start();

	}

	// animate microphone
	function listening() {
		if (session.recon.activated) {
			mic_animation = setInterval(function () {
				if (mic_image == 'microphone-on1.svg') {
					mic_image = 'microphone-on2.svg';
				} else {
					mic_image = 'microphone-on1.svg';
				}
				document.getElementById('mic_off').src = mic_image;
			}, 300);
		} else {
			clearInterval(mic_animation);
			document.getElementById('mic_off').src = "microphone-on.svg";
		}
	}

	function update_grammars() {
		grammars = [
			'#JSGF V1.0; grammar utterances; public <utterance> = ' + activation.join(' | ') + ' ',
			'#JSGF V1.0; grammar utterances; public <utterance> = hello | hi '
		]
	}

} else {
	document.getElementById('mic').src = "microphone-deactivated.svg";
	function listen() { }
	function toggleStartStop() { }
	function update_grammars() { }
	$('#mic').empty();
	$('#mic').append('<img src="microphone-deactivated.svg" title="Disabled" alt="Microphone" id="mic_disabled" onclick="help(2)" class="mic" style="max-width: 30px;">');
}


/**
** Fuzzy search part (using "fuse" JavaScript library)
*/

/**
** returns the closest match to [string] in the [list] with a threshold value of 0.7
*/
function fuzzy_search(list, string) {
	let fuse = new Fuse(list, { includeScore: true })
	let result = fuse.search(string)
	if (result.length > 0 && result[0].score < 0.7) {
		return result[0].item
	}
	return ""
}

/**
** extracts the list of possible utterances from the grammar
*/
function expand(grammar) {
	l = grammar.split(';');
	l.shift();
	l[0] = l[0].replace(' grammar ', '');
	let g = l.shift();
	g_dict[g] = {};
	for (let i = 0; i < l.length; i++) {
		while (l[i].indexOf(' ') == 0) {
			l[i] = l[i].replace(' ', '');
		}
		while (l[i].slice(l[i].length - 1, l[i].length) == ' ') {
			l[i] = l[i].slice(0, l[i].length - 1)
		}
		l[i] = l[i].split('=');
		for (let k in l[i]) {
			if (l[i][k].slice(0, 1) == ' ') {
				l[i][k] = l[i][k].slice(1, l[i][k].length)
			}
			if (l[i][k].slice(l[i][k].length - 1, l[i][k].lenght) == ' ') {
				l[i][k] = l[i][k].slice(0, l[i][k].length - 1)
			}
		}
		let n = l[i][0].indexOf('<') + 1
		let n2 = l[i][0].indexOf('>')
		l[i][0] = l[i][0].slice(n, n2)
		if (l[i].length == 1 && l[i].indexOf('') != -1) {
			l.splice(i, 1);
		}
		if (l[i] && l[i][1] && l[i][1].indexOf('|') != -1) {
			l[i][1] = l[i][1].split('|');
			for (let j in l[i][1]) {
				if (l[i][1][j].slice(0, 1) == ' ') {
					l[i][1][j] = l[i][1][j].slice(1, l[i][1][j].length)
				}
				if (l[i][1][j].slice(l[i][1][j].length - 1, l[i][1][j].lenght) == ' ') {
					l[i][1][j] = l[i][1][j].slice(0, l[i][1][j].length - 1)
				}
			}
		}
		if (l[i] && !Array.isArray(l[i][1])) {
			g_dict[g][l[i][0]] = [l[i][1]];
		} else if (l[i]) {
			g_dict[g][l[i][0]] = l[i][1];
		}

	}

	for (key in g_dict[g]) {
		while (verify_content(g_dict[g][key])) {
			for (let i in g_dict[g][key]) {
				let n = g_dict[g][key][i].indexOf('<') + 1
				let n2 = g_dict[g][key][i].indexOf('>')
				if (n > 0) {
					let k = g_dict[g][key][i].slice(n, n2)
					if (g_dict[g][k]) {
						for (let s in g_dict[g][k]) {
							g_dict[g][key].push(g_dict[g][key][i].replace('<' + k + '>', g_dict[g][k][s]));
						}
					}
					g_dict[g][key].splice(i, 1);
				}
			}
		}
	}
	return g_dict[g]['utterance']
}

/**
** check if there are words between <> 
*/
function verify_content(v_list) {
	for (let k1 in v_list) {
		if (v_list[k1].indexOf('<') > -1) {
			return true;
		}
	}
	return false;
}


function set_paper_url(answer, url_data) {
	let urls = url_data.urls;
	let dois = url_data.doi;
	for (i in dois) {
		let doi = dois[i];
		let p_urls = urls[i];
		let pdf = [];
		let links = [];
		let n = 0;
		let n1 = 0;
		for (j in p_urls) {
			if (p_urls[j].endsWith(".pdf")) {
				n += 1,
					pdf.push('<a target="_blank" href="' + p_urls[j] + '">pdf(' + n + ')</a>');
			} else {
				n1 += 1;
				links.push('<a target="_blank" href="' + p_urls[j] + '">link(' + n1 + ')</a>');
			}
		}

		if (doi == "" && pdf.length > 0) {
			let replacement = ". Links: ";
			if (pdf.length > 1) {
				replacement += pdf.shift() + ', <details><summary> more...</summary>';
				replacement += pdf.join(", ") + '</details>';
			} else {
				replacement += pdf.shift();
			}
			answer = answer.replace("[$]", replacement)
		} else if (doi != "" && pdf.length > 0) {
			let replacement = ". Links: ";
			replacement += '<a target="_blank" href="' + doi + '">doi</a> - ';
			if (pdf.length > 1) {
				replacement += pdf.shift() + ', <details><summary> more...</summary>';
				replacement += pdf.join(", ") + '</details>';
			} else {
				replacement += pdf.shift();
			}
			answer = answer.replace("[$]", replacement)
		} else if (doi != "" && links.length > 0) {
			let replacement = ". Links: ";
			replacement += '<a target="_blank" href="' + doi + '">doi</a> - ';
			if (pdf.length > 1) {
				replacement += links.shift() + ', <details><summary> more...</summary>';
				replacement += links.join(", ") + '</details>';
			} else {
				replacement += links.shift();
			}
			answer = answer.replace("[$]", replacement);
		} else if (doi == "" && links.length > 0) {
			let replacement = ". Links: ";
			if (pdf.length > 1) {
				replacement += links.shift() + ', <details><summary> more...</summary>';
				replacement += links.join(", ") + '</details>';
			} else {
				replacement += links.shift();
			}
			answer = answer.replace("[$]", replacement);
		} else if (doi != "") {
			answer = answer.replace("[$]", ". Links: " + '<a target="_blank" href="' + doi + '">doi</a>');
		} else {
			answer = answer.replace("[$]", ". ");
		}
	}
	return answer;
}

// Make the DIV element draggable:
dragElement(document.getElementById("templates"));
dragElement(document.getElementById("auth"));

function dragElement(elmnt) {
	var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
	if (document.getElementById(elmnt.id + "header")) {
		// if present, the header is where you move the DIV from:
		document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
	} else {
		// otherwise, move the DIV from anywhere inside the DIV:
		elmnt.onmousedown = dragMouseDown;
	}

	function dragMouseDown(e) {
		e = e || window.event;
		e.preventDefault();
		// get the mouse cursor position at startup:
		pos3 = e.clientX;
		pos4 = e.clientY;
		document.onmouseup = closeDragElement;
		// call a function whenever the cursor moves:
		document.onmousemove = elementDrag;
	}

	function elementDrag(e) {
		e = e || window.event;
		e.preventDefault();
		// calculate the new cursor position:
		pos1 = pos3 - e.clientX;
		pos2 = pos4 - e.clientY;
		pos3 = e.clientX;
		pos4 = e.clientY;
		// set the element's new position:
		elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
		elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
	}

	function closeDragElement() {
		// stop moving when mouse button is released:
		document.onmouseup = null;
		document.onmousemove = null;
	}
}

function login() {
	let name = document.getElementById("uname").value
	let psw = document.getElementById("psw").value
	if (name.lenght == 0) {
		alert("Insert username")
	}
	if (psw.lenght == 0) {
		alert("Insert password")
	}
	post(name, psw);
}

function get_token() {
	auth_div = document.getElementById("auth");
	auth.style.display = "inline"
	document.getElementById("user_input").disabled = true;
	document.getElementById("uname").focus();

}


function post(name, psw) {
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "/token", true);
	xhr.setRequestHeader('accept', 'application/json')
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send("grant_type=&username=" + name + "&password=" + psw + "&scope=&client_id=&client_secret=");
	xhr.onload = function () {
		var res = this.responseText;
		var jj = JSON.parse(res);
		document.getElementById("uname").value = "";
		document.getElementById("psw").value = "";
		if (jj.access_token) {
			session.token = jj.access_token
			document.getElementById("auth").style.display = "none";
			document.getElementById("user_input").disabled = false;
			document.getElementById("user_input").focus();
			if (session.recycle) {
				cycle();
			}
		} else {
			alert(jj.detail)
			document.getElementById("uname").focus();
		}
		return jj
	}
}

function settings() {
	if (session.help_status > 0) {
		help(session.help_status);
	}
	let settings_div = document.getElementById("settings");
	if (session.settings) {
		settings_div.style.display = "none";
		session.settings = false;
	} else {
		settings_div.style.display = "inline";
		session.settings = true;
	}
	resize()
}

function div_templates() {

	let div_t = document.getElementById("in_t");
	let temp_div = document.getElementById("templates");

	if (!session.templates) {
		document.getElementById("close_templates").style.display = "inline";
		document.getElementById("open_templates").style.display = "none";
		document.getElementById("in_t").display = "inline";
	} else {
		document.getElementById("open_templates").style.display = "inline";
		document.getElementById("close_templates").style.display = "none";
	}

	if (session.templates) {
		div_t.style.display = "none";
		temp_div.style.top = "35px";
		temp_div.style.left = $(window).width() / 2 + "px"
		temp_div.style.width = "200px";

	} else {
		div_t.style.display = "inline";
		temp_div.style.width = "400px"
		temp_div.style.top = "350px";
		temp_div.style.left = "250px"
	}

	session.templates = !session.templates
}

function open_urls(data) {
	let url;
	if (data.query.url && typeof (data.query.url) == "string" && data.query.source && data.query.source.length > 0) {
		url = " - " + '<a target="_blank" href="' + data.query.url + '"> Source: ' + data.query.source + '</a>';
	} else if (data.query.url) {
		let urls = []
		for (i in data.query.url) {
			let b = 1 * 1 + 1 * i
			let url = '<a target="_blank" href="' + data.query.url[i] + '"> url' + b + '</a>'
			urls.push(url)
		}
		url = " - Source: " + data.query.source
		if (urls.length > 0) {
			url += " - Links: " + urls.join(", ");
		}
	}
	return url
}