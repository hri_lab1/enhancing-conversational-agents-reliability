import copy
import json

from fuzzywuzzy import process
from config import threshold as fuzzy_threshold
from sentence_transformers.util import cos_sim

import aidaLogic
import language


def verify_dsc_data2(ins):
    ins2 = ins
    if ins.lower().startswith("the "):
        ins2 = ins[4:]
    data = json.loads(aidaLogic.dsc_finder(ins2))
    if data['result'] != 'ok' and ins != ins2:
        data = json.loads(aidaLogic.dsc_finder(ins))
    return data


def verify_data2(ins):
    ins2 = ins
    if ins.lower().startswith("the "):
        ins2 = ins[4:]
    data = json.loads(aidaLogic.find_match(ins2))
    if data['result'] != 'ok' and ins != ins2:
        data = json.loads(aidaLogic.find_match(ins))
    if data['result'] == 'ok' and data['obj_id'] == 4:
        result = json.loads(aidaLogic.check_author(data))
    elif data['result'] == 'ok' and (data['obj_id'] == 2 or data['obj_id'] == 6):
        result = json.loads(aidaLogic.check_conference(data))
    elif data['result'] == 'ok' and data['obj_id'] == 1:
        result = json.loads(aidaLogic.check_topic_for_conference(data))
    elif data['result'] == 'k2':
        result = json.loads(aidaLogic.check_topic(data, ins, False))
    else:
        result = data
    return result


class Grammar:
    def __init__(self, grammar, model=None, gr_add=language.aidabot_grammar_add):
        self.open_questions = []
        self.search_questions = []
        self.dsc_questions = []
        self.model = model
        self.grammar = grammar
        self.grammar_add = gr_add
        self.questions = []
        self.final_questions = []
        self.threshold = 0.75
        self.free_questions = []
        self.wild_questions = []

    # extracts the list of possible utterances from the grammar
    def expand(self, grammar, g_dict=None):
        if g_dict is None:
            g_dict = {}
        grammar = grammar.replace('\n', '')
        ph_list = grammar.split(';')
        ph_list.pop(0)
        ph_list[0] = ph_list[0].replace(' grammar ', '', 1)
        g = ph_list.pop(0)
        g_dict[g] = {}

        for i in range(len(ph_list)):
            ph_list[i] = ph_list[i].strip()
            ph_list[i] = ph_list[i].split('=')

            for k in range(len(ph_list[i])):
                ph_list[i][k] = ph_list[i][k].strip()

            n = ph_list[i][0].find('<') + 1
            n2 = ph_list[i][0].find('>')
            ph_list[i][0] = ph_list[i][0][n: n2]

            if len(ph_list[i]) == 1 and ph_list[i][0] == '':
                del ph_list[i]

            if ph_list[i] and ph_list[i][1] and ph_list[i][1].find('|') != -1:
                ph_list[i][1] = ph_list[i][1].split('|')

                for j in range(len(ph_list[i][1])):
                    ph_list[i][1][j] = ph_list[i][1][j].strip()

            if ph_list[i] is not None and isinstance(ph_list[i][1], list):
                g_dict[g][ph_list[i][0]] = ph_list[i][1]
            elif ph_list[i] is not None:
                g_dict[g][ph_list[i][0]] = [ph_list[i][1]]

        for key in g_dict[g]:
            while self.verify_content(g_dict[g][key]):
                for i in range(len(g_dict[g][key])):
                    n = g_dict[g][key][i].find('<') + 1
                    n2 = g_dict[g][key][i].find('>')
                    if n > 0:
                        k = g_dict[g][key][i][n: n2]
                        if g_dict[g][k] is not None:
                            for s in range(len(g_dict[g][k])):
                                g_dict[g][key].append(g_dict[g][key][i].replace('<' + k + '>', g_dict[g][k][s]))

                        del g_dict[g][key][i]

        result = g_dict[g][[x for x in g_dict[g]][-1]]
        for n in range(len(result)):
            result[n] = result[n].replace('  ', ' ')
        distinct_result = []
        for q in result:
            if q not in distinct_result:
                distinct_result.append(q)
        return distinct_result

    # check if there are words between <>
    @staticmethod
    def verify_content(v_list):
        for k1 in range(len(v_list)):
            if '<' in v_list[k1]:
                return True
        return False

    def purge(self):
        purged_questions = []
        for question in self.questions:
            if not (('paper' in question or 'papers' in question) and 'publications' in question):
                purged_questions.append(question)
        self.questions = purged_questions
        purged_questions = []
        for question in self.free_questions:
            if not (('paper' in question or 'papers' in question) and 'publications' in question):
                purged_questions.append(question)
        self.free_questions = purged_questions

    def get_questions(self, data, expand=False):
        objects = ["topics", "conferences", "organizations", "authors", "journals"]

        if expand:
            grammar = self.grammar + self.grammar_add
        else:
            grammar = self.grammar

        r_numbers = data.get('numbers')
        r_numbers2 = data.get('numbers2')

        if r_numbers2 is None:
            r_numbers2 = []

        if r_numbers is None:
            r_numbers = []

        if len(r_numbers2) == 0:
            grammar = grammar.replace("public <order> = <orders> | <orders> in the last 5 years | <orders> in the "
                                      "last <num2> years ;", "public <order> = <orders> | <orders> in the last 5 "
                                                             "years ;")

        nums = 'public <num> = ' + ' | '.join(list(map(str, r_numbers))) + ' ; '
        nums2 = 'public <num2> = ' + ' | '.join(list(map(str, r_numbers2))) + ' ; '

        grammar = grammar.replace('public <num> = 2 ;', nums)
        grammar = grammar.replace('public <num2> = ;', nums2)

        ok = 0
        oks = []

        instances = data.get("instances")
        sentence = data.get("sentences")[0]
        to_ord_instances = []
        for ins in instances:
            if ins.get("orig_ins") in sentence:
                ind = sentence.index(ins.get("orig_ins"))
                to_ord_instances.append([ind, ins])
            else:
                to_ord_instances.append([9999, ins])
        sorted_instances = sorted(to_ord_instances, key=lambda x: x[0], reverse=False)
        instances = [x[1] for x in sorted_instances]
        data["instances"] = instances

        if instances is not None:

            for inst in instances:
                if inst.get("result") == 'ok':
                    if inst.get("obj_id") == 2:
                        if inst.get("orig_ins") == inst.get("item") :
                            inst["ins"] = inst.get("item")
                            ok += 1
                            oks.append(inst)
                        elif inst.get("orig_ins") == inst.get("name"):
                            inst["ins"] = inst.get("name")
                            ok += 1
                            oks.append(inst)
                        elif self.get_similar(inst.get("orig_ins"), [inst.get("item")])[0][0] > self.threshold:
                            inst["ins"] = inst.get("item")
                            ok += 1
                            oks.append(inst)
                        elif self.get_similar(inst.get("orig_ins"), [inst.get("name")])[0][0] > self.threshold:
                            inst["ins"] = inst.get("name")
                            ok += 1
                            oks.append(inst)

                    else:
                        if inst.get("orig_ins") == inst.get("item"):
                            ok += 1
                            oks.append(inst)
                        elif self.get_similar(inst.get("orig_ins"), [inst.get("item")])[0][0] > self.threshold:
                            ok += 1
                            oks.append(inst)

                if inst.get("result") == 'ka':
                    if inst.get("obj_id") == 2:
                        if inst.get("orig_ins") == inst.get("item")[0].get("acronym") or self.get_similar(inst.get("orig_ins"), [inst.get("item")[0].get("acronym")])[0][0] > self.threshold:
                            ok += 1
                            oks.append(inst)
                    else:
                        if inst.get("orig_ins") == inst.get("item")[0].get("name") or self.get_similar(inst.get("orig_ins"), [inst.get("item")[0].get("name")])[0][0] > self.threshold:
                            ok += 1
                            oks.append(inst)
                if inst.get("result") == 'k2':
                    inst["oks"] = []
                    num = inst.get("num")
                    keys = inst.get("keys")
                    for i, n in enumerate(num):
                        if n > 0:
                            key = keys[i]
                            sim = self.get_similar(inst.get("orig_ins"), key)
                            if sim[0][0] > self.threshold:
                                obj = objects[i]
                                new_inst = {"result": "ok", "object": obj, "obj_id": i + 1,
                                            "item": sim[0][1], "orig_ins": inst.get("orig_ins")}
                                if i == 3 or i == 1:
                                    d = verify_data2(sim[0][1])
                                    if d.get("result") == "ok":
                                        new_inst["id"] = d.get("id")
                                inst["oks"].append(new_inst)
                    if len(inst["oks"]) > 0:
                        oks.append(inst)

        okks = self.k2_elaboration(oks)

        new_okks = []
        for group in okks:
            new_group = []
            items = []
            for check_ins in group:
                if check_ins.get("item") not in items:
                    new_group.append(check_ins)
                    if check_ins.get("item") is not None:
                        items.append(check_ins.get("item"))
            new_okks.append(new_group)
        okks = new_okks
        total_generated_questions = self.expand(grammar)
        self.questions = [q for q in total_generated_questions if q.startswith("01") or q.startswith("02")]
        self.dsc_questions = [q for q in total_generated_questions if q.startswith("03") or q.startswith("04")]
        self.search_questions = [q for q in total_generated_questions if q.startswith("05")]
        self.open_questions = [q for q in total_generated_questions if q.startswith("06")]
        self.free_questions = []
        self.wild_questions = []
        for q in self.questions:
            new_q = q.replace("{}", "").replace("  ", " ").replace(" vs ", "").strip()
            self.free_questions.append(new_q)
            if "conference" not in new_q and "journal" not in new_q:

                if "paper" not in new_q:
                    new_q2 = new_q + " with papers published by a journal"
                    new_q3 = new_q + " with papers accepted by a conference"
                else:
                    new_q2 = new_q + " published by a journal"
                    new_q3 = new_q + " accepted by a conference"
                self.wild_questions.append(new_q2)
                self.wild_questions.append(new_q3)

        self.free_questions = list(set(self.free_questions))
        self.wild_questions = list(set(self.wild_questions))

        self.purge()

        a = []
        p = []
        co = []
        o = []
        ci = []
        j = []
        t = []
        questions = [a, p, co, o, ci, j, t]
        subs = ["author", "paper", "conference", "organization", "citation", "journal", "topic"]

        for q in self.questions:
            qq = q.split(" ")
            for w in qq:
                if w.endswith("s"):
                    w = w[:-1]
                if w in subs:
                    questions[subs.index(w)].append(q)
                    break

        final_questions = []
        self.final_questions = []
        for j, in_oks in enumerate(okks):
            s_questions = []
            for i, l in enumerate(questions):
                stringa = []
                sub = language.preps.get(subs[i])
                flag = True
                objs = []
                obj = ""

                in_oks = self.order_ins(in_oks, data.get("sentences")[0])
                for ins in in_oks[:3]:
                    if ins.get("object") is not None:
                        objs.append(ins["obj_id"])
                        prep = sub.get(ins.get("object"))
                        if len(prep) == 0:
                            flag = False
                        if ins.get("result") == "ok":
                            if ins.get("obj_id") == 2:
                                obj = ins.get("ins") if ins.get("ins") is not None else ins.get("item")
                            else:
                                obj = ins.get("item")

                        else:
                            if ins.get("obj_id") == 2:
                                obj = ins.get("item")[0].get("acronym")
                            else:
                                obj = ins.get("item")[0].get("name")
                        if isinstance(prep, list):
                            stringa.append([x + " " + obj for x in prep])
                        elif isinstance(prep, str):
                            stringa.append(prep + " " + obj)

                    for qqq in self.search_questions:
                        final_questions.append(qqq.format(obj))
                        s_questions.append(qqq.format(obj))

                    for qqq in self.open_questions:
                        final_questions.append(qqq.format(obj))
                        s_questions.append(qqq.format(obj))

                cond1 = (i == 2 and (5 in objs or 2 in objs)) or (i == 5 and (2 in objs or 5 in objs))
                conf = 0
                jour = 0
                for ob in objs:
                    if ob == 2:
                        conf += 1
                    if ob == 5:
                        jour += 1
                cond2 = (i == 1 and (conf > 1 or jour > 1)) or (i == 3 and 3 in objs) or (i == 0 and 4 in objs)

                if flag and not (2 in objs and 5 in objs) and not cond1 and not cond2:
                    for q in l:
                        quest = self.generate_questions(q, stringa)
                        final_questions += quest
                        s_questions += quest
                        if len(in_oks[:3]) < 3 and i != 2 and i != 5 and 2 not in objs and 5 not in objs:
                            q1 = q + " published by a journal"
                            q2 = q + " accepted by a conference"
                            s_questions.append(q1)
                            s_questions.append(q2)
                            self.wild_questions.append(q1)
                            self.wild_questions.append(q2)

            self.final_questions.append({"instances": in_oks, "questions": s_questions})

        final_questions += self.dsc_instances(data)

        final_questions += ["02 list", "01 count", "01 how many"]

        if len(final_questions) < 7:
            for question in self.questions:
                if not question.startswith("compare") and not question.startswith("describe") and not question.startswith("03 ") and not question.startswith("04 "):
                    final_questions.append(question.format("").strip().replace("  ", " "))
            final_questions = list(set(final_questions))

        paper_instances = data.get("paper_ins")
        search_questions = []
        for qqq in self.search_questions:
            for paper_ins in paper_instances:
                # print(qqq, paper_ins)
                final_questions.append(qqq.format(paper_ins))
                search_questions.append([qqq.format(paper_ins), paper_ins])
        self.search_questions = search_questions

        open_questions = []
        for qqq in self.open_questions:
            for paper_ins in paper_instances:
                # print(qqq, paper_ins)
                final_questions.append(qqq.format(paper_ins))
                open_questions.append([qqq.format(paper_ins), paper_ins])
        self.open_questions = open_questions

        final_questions += self.free_questions
        final_questions += self.wild_questions
        final_questions = list(set(final_questions))
        self.questions = final_questions


        # print("le domande sono: ", len(final_questions))
        return self.questions

    def get_similar(self, ins, texts):
        new_texts = []
        if isinstance(ins, str) and isinstance(texts, list):
            found = process.extract(ins, texts, limit=3)
            for data in found:
                if data[1] > fuzzy_threshold:
                    new_texts.append([data[1]/100, data[0]])
        if self.model is None:
            return None
        if len(texts) == 0 or len(ins) == 0:
            return [[0, ""]]
        embeddings = self.model.encode(texts)
        emb_question = self.model.encode(ins)
        scores = cos_sim(emb_question, embeddings)

        scored_texts = []
        for i, score in enumerate(scores[0]):
            scored_texts.append((round(score.item(), 4), texts[i]))
        scored_texts += new_texts
        sorted_scored_texts = sorted(scored_texts, key=lambda x: x[0], reverse=True)
        return sorted_scored_texts

    def dsc_instances(self, data):
        objects = ["authors", "conferences", "conferences", "organizations"]
        compare = [q for q in self.dsc_questions if q.startswith('04')]
        describe = [q for q in self.dsc_questions if q.startswith('03')]
        ok = 0
        oks = []
        k2 = 0
        k2s = []
        k2oks = []
        k2ok = 0

        instances = data.get("dsc_instances")
        if instances is not None:
            for inst in instances:
                if inst.get("result") == 'ok':
                    if inst.get("obj_id") != 2:
                        if inst.get("orig_ins") == inst.get("item").get("name") or self.get_similar(inst.get("orig_ins"), [inst.get("item").get("name")])[0][0] > self.threshold:
                            ok += 1
                            oks.append(inst)
                    else:
                        if inst.get("orig_ins") == inst.get("item").get("acronym") or self.get_similar(inst.get("orig_ins"), [inst.get("item").get("acronym")])[0][0] > self.threshold:
                            ok += 1
                            oks.append(inst)

                if inst.get("result") == 'ka':
                    if inst.get("obj_id") == 2:
                        if inst.get("orig_ins") == inst.get("item")[0].get("acronym") or self.get_similar(inst.get("orig_ins"), [inst.get("item")[0].get("acronym")])[0][0] > self.threshold:
                            ok += 1
                            oks.append(inst)
                    else:
                        if inst.get("orig_ins") == inst.get("item")[0].get("name") or self.get_similar(inst.get("orig_ins"), [inst.get("item")[0].get("name")])[0][0] > self.threshold:
                            ok += 1
                            oks.append(inst)

                if inst.get("result") == 'k2':
                    num = inst.get("num")
                    keys = inst.get("keys")
                    for i, n in enumerate(num):
                        if n > 0:
                            key = keys[i]
                            if i == 1:
                                k = [x.get("acronym") for x in key]
                            else:
                                k = [x.get("name") for x in key]
                            sim = self.get_similar(inst.get("orig_ins"), k)
                            if sim[0][0] > self.threshold:
                                k2ok += 1
                                obj = objects[i]
                                index = k.index(sim[0][1])
                                new_inst = {"result": "ok", "object": obj, "obj_id": i + 1, "item": key[index],
                                            "k2": inst}
                                k2oks.append(new_inst)
                    k2 += 1
                    k2s.append(inst)

        okks = []
        if k2ok == 1:
            for inst in k2oks:
                new_list = oks + [inst]
                okks.append(new_list)
        elif k2ok == 2:
            okks.append(k2oks)
        else:
            okks.append(oks)

        final_questions = []
        for j, in_oks in enumerate(okks):
            a = []
            c = []
            c1 = []
            o = []
            j = []
            objs = [a, c, c1, o, j]

            s_questions = []
            for ins in in_oks:
                # noinspection PyTypeChecker
                objs[ins.get("obj_id") - 1].append(ins)

            if len(c) + len(c1) >= 2:
                if len(c) == 2:
                    item1 = c[0].get("item").get("acronym") if c[0].get("result") == "ok" else c[0].get("item")[0].get(
                        "acronym")
                    item2 = c[1].get("item").get("acronym") if c[1].get("result") == "ok" else c[1].get("item")[0].get(
                        "acronym")
                elif len(c) == 1:
                    item1 = c[0].get("item").get("acronym") if c[0].get("result") == "ok" else c[0].get("item")[0].get(
                        "acronym")
                    item2 = c1[0].get("item").get("name") if c1[0].get("result") == "ok" else c1[0].get("item")[0].get(
                        "name")
                else:
                    item1 = c1[0].get("item").get("name") if c1[0].get("result") == "ok" else c1[0].get("item")[0].get(
                        "name")
                    item2 = c1[1].get("item").get("name") if c1[1].get("result") == "ok" else c1[1].get("item")[0].get(
                        "name")
                new_compare = [q.format(item1, item2) for q in compare]
                final_questions += new_compare
                s_questions += new_compare

            elif len(a) >= 2:
                item1 = a[0].get("item").get("name") if a[0].get("result") == "ok" else a[0].get("item")[0].get("name")
                item2 = a[1].get("item").get("name") if a[1].get("result") == "ok" else a[1].get("item")[0].get("name")
                new_compare = [q.format(item1, item2) for q in compare]
                final_questions += new_compare
                s_questions += new_compare

            elif len(o) >= 2:
                item1 = o[0].get("item").get("name") if o[0].get("result") == "ok" else o[0].get("item")[0].get("name")
                item2 = o[1].get("item").get("name") if o[1].get("result") == "ok" else o[1].get("item")[0].get("name")
                new_compare = [q.format(item1, item2) for q in compare]
                final_questions += new_compare
                s_questions += new_compare
            else:
                final_questions.append("04 compare")

            if len(c) > 0:
                for conf in c:
                    if conf.get("result") == "ok":
                        item1 = conf.get("item").get("acronym")
                    else:
                        item1 = conf.get("item")[0].get("acronym")
                    new_describe = [q.format(item1) for q in describe]
                    final_questions += new_describe
                    s_questions += new_describe

            if len(c1) > 0:
                for conf in c1:
                    if conf.get("result") == "ok":
                        item1 = conf.get("item").get("name")
                    else:
                        item1 = conf.get("item")[0].get("name")
                    new_describe = [q.format(item1) for q in describe]
                    final_questions += new_describe
                    s_questions += new_describe

            elif len(a) > 0:
                for auth in a:
                    item1 = auth.get("item").get("name") if auth.get("result") == "ok" else auth.get("item")[0].get(
                        "name")
                    new_describe = [q.format(item1) for q in describe]
                    final_questions += new_describe
                    s_questions += new_describe

            elif len(o) > 0:
                for org in o:
                    item1 = org.get("item").get("name") if org.get("result") == "ok" else org.get("item")[0].get("name")
                    new_describe = [q.format(item1) for q in describe]
                    final_questions += new_describe
                    s_questions += new_describe
            else:
                final_questions.append("03 describe")
                final_questions.append("03 who is")
                final_questions.append("03 what is")

            self.final_questions.append({"dsc_instances": in_oks, "questions": s_questions})

            return final_questions

    def k2_elaboration(self, ins_list):
        if len(ins_list) == 0:
            return []
        ordered_list = []
        ok = True
        for ins in ins_list:
            if ins.get("result") == "k2":
                ok = False
                ordered_list.insert(0, ins)
            else:
                ordered_list.append(ins)
        if ok:
            return [ordered_list]

        first_ins = ordered_list[0]
        ordered_list.remove(first_ins)
        if len(ordered_list) == 0:
            return [[x] for x in first_ins.get("oks")]

        if ordered_list[0].get("result") == "k2":
            ordered_list = self.k2_elaboration(ordered_list)

        result_list = [[x] for x in first_ins.get("oks")]

        new_result_list = []
        for ins_group in result_list:
            if len(ordered_list) > 0 and isinstance(ordered_list[0], list):
                for ins2_group in ordered_list:
                    new_list = ins_group + ins2_group
                    new_list = sorted(new_list, key=lambda x: x.get("obj_id") if x.get("obj_id") is not None else 0,
                                      reverse=True)
                    new_result_list.append(new_list)
            else:
                new_list = copy.deepcopy(ordered_list)
                for ins in ins_group:
                    new_list.append(ins)
                new_list = sorted(new_list, key=lambda x: x.get("obj_id") if x.get("obj_id") is not None else 0,
                                  reverse=True)
                new_result_list.append(new_list)
        return new_result_list

    @staticmethod
    def order_ins(instances, sentence):
        to_ord_instances = []
        for ins in instances:
            if ins.get("orig_ins") in sentence:
                ind = sentence.index(ins.get("orig_ins"))
            else:
                ind = 0
            to_ord_instances.append([ind, ins])
        sorted_instances = sorted(to_ord_instances, key=lambda x: x[0], reverse=False)
        instances = [x[1] for x in sorted_instances]
        return instances

    @staticmethod
    def generate_questions(q, stringa):
        flag = False
        for x in stringa:
            if isinstance(x, list):
                flag = True

        if not flag:
            return [q.format(", ".join(stringa))]

        lung = len(stringa)
        stringhe = []
        if lung == 0:
            return []
        if lung >= 1:
            if isinstance(stringa[0], list):
                for x in stringa[0]:
                    stringhe.append(x)
            else:
                stringhe.append(stringa[0])

        if lung >= 2:
            new_stringhe = []
            if isinstance(stringa[1], list):
                for x in stringhe:
                    for y in stringa[1]:
                        new_stringhe.append(x + ", " + y)

            else:
                for x in stringhe:
                    new_stringhe.append(x + ", " + stringa[1])
            stringhe = new_stringhe

        if lung == 3:
            new_stringhe = []
            if isinstance(stringa[2], list):
                for x in stringhe:
                    for y in stringa[2]:
                        new_stringhe.append(x + ", " + y)
            else:
                for x in stringhe:
                    new_stringhe.append(x + ", " + stringa[2])
            stringhe = new_stringhe
        new_stringhe = []
        for x in stringhe:
            new_stringhe.append(q.format(x))
        return new_stringhe


