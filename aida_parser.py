import json
import re
import time
import urllib
import urllib.parse
import urllib.request

from sentence_transformers.util import cos_sim

import grammar as gr
import open_domain
import paper_search
import language
from grammar import Grammar
from language import *
import aidaLogic
import config

from models import *
from spc_parser import analyze as get_data, get_np


def stop_words_verify(data):
    instances = data.get("ins")
    stop = data.get("stop")
    if len(instances) == 0:
        return instances
    new_instances = []
    for ins in instances:
        ws = ins.split(" ")
        new_ws = []
        for word in ws:
            if word.lower() not in stop:
                new_ws.append(word)
        if len(new_ws) > 0:
            new_instances.append(ins)
    return new_instances


class Parser:
    def __init__(
            self, grammar, url=data_server_url, ver_words=words_to_verify,  # answer_text=context,
            pers_questions=personal_questions, threshold=0.70, questions_log_file=config.questions_log_file,
            questions_file=config.questions_file, debug=config.debug, grammar_add=language.aidabot_grammar_add):
        self.ai = "odt"
        self.debug = debug
        self.questions_file = questions_file
        self.questions_log_file = questions_log_file
        self.threshold = threshold
        # if answer_text is None:
        #     answer_text = []
        if ver_words is None:
            ver_words = []
        if pers_questions is None:
            pers_questions = []
        self.data_server_url = url
        self.model = model
        self.grammar = Grammar(grammar, model, gr_add=grammar_add)
        self.ver_words = ver_words
        # self.answer_text = answer_text
        self.pers_questions = [key for key in pers_questions]
        self.pers_answers = pers_questions
        self.query = {}
        self.syns = {}

    def synonyms(self, question):
        for key in syns:
            for w in syns[key]:
                w_ = " "+ w + " "
                t1 = w_ in question or question.endswith(" "+ w) or question.startswith(w + " ")
                t2 = w_ in question.lower() or question.lower().endswith(" "+ w) or question.lower().startswith(w + " ")

                if t1:
                    question = question.replace(w, key)
                    self.syns[key] = w
                elif t2:
                    question = question.lower().replace(w, key)


        words = question.lower().split(" ")
        words_normal = question.split(" ")
        for key in syns:
            for w in syns[key]:
                if w in words:
                    i = words.index(w)
                    words_normal[i] = key
        return " ".join(words_normal)

    @staticmethod
    def quote_verify(data):
        instances = []
        papers = []

        for q in data.get("quoted"):
            new_instance = gr.verify_data2(q)
            new_papers = json.loads(aidaLogic.find(q))

            if new_instance.get("result") == "ok":
                new_instance["orig_ins"] = q
                instances.append(new_instance)

            if new_papers.get("result") == "ok" and len(new_papers.get("papers")) > 0:
                new_papers = new_papers.get("papers")
                if new_papers[0].get("mode") == 0:
                    papers.append(new_papers[0])


        quoted_text_list = [x.get("orig_ins") for x in instances]
        quoted_paper_text_list = [x.get("title") for x in papers]
        if len(quoted_text_list) > 0:
            quoted_text = " ".join(quoted_text_list).lower()
            not_quoted_text = " ".join(data.get("sentences")).lower()

            for t in quoted_text_list:
                not_quoted_text = not_quoted_text.replace('"' + t.lower() + '"', "")

            new_ins_list = []

            for np in data.get("ins"):
                np = np.replace('"', '')

                if np.lower() not in quoted_text.lower() or (
                        np.lower() in quoted_text.lower() and np.lower() in not_quoted_text.lower()):
                    if np not in new_ins_list:
                        new_ins_list.append(np)

            data["ins"] = new_ins_list + quoted_text_list

        elif len(quoted_paper_text_list) > 0:
            quoted_text = " ".join(quoted_paper_text_list).lower()
            not_quoted_text = " ".join(data.get("sentences")).lower()

            for t in quoted_paper_text_list:
                not_quoted_text = not_quoted_text.replace('"' + t.lower() + '"', "")

            new_ins_list = []

            for np in data.get("ins"):
                np = np.replace('"', '')

                if np.lower() not in quoted_text.lower() or (
                        np.lower() in quoted_text.lower() and np.lower() in not_quoted_text.lower()):
                    if np not in new_ins_list:
                        new_ins_list.append(np)

            data["ins"] = new_ins_list + quoted_paper_text_list


    def verify_data(self, cmd, ins):
        try:
            response = urllib.request.urlopen(self.data_server_url + cmd + urllib.parse.quote(str(ins)))
            data = json.load(response)
            assert isinstance(data, dict)
            return data
        except OSError:
            return {}

    @staticmethod
    def check_finder_results(data, query, text, instances, flag2):
        if data.get("result") == "ok":
            key = data.get("item")
            if isinstance(key, dict):
                key = key.get("name")
                flag2 = True
            if key.lower() in query.lower():

                t = text.split(" ")
                k = key.lower().split(" ")
                text = []
                for word in t:
                    if word not in k:
                        text.append(word)
                text = " ".join(text)
                if key not in instances:
                    instances.append(key)
                    flag2 = True
                while "  " in text:
                    text = text.replace("  ", " ").strip()

        elif data.get("result") == "k2":
            keys = data.get("keys")
            k = [x for y in keys for x in y]
            k_sorted = sorted(k, key=lambda x: len(x), reverse=True)
            for key in k_sorted:
                if isinstance(key, dict):
                    key = key.get("name")
                    flag2 = True
                if key.lower() in query.lower():
                    t = text.split(" ")
                    k = key.lower().split(" ")
                    text = []
                    for word in t:
                        if word not in k:
                            text.append(word)
                    text = " ".join(text)
                    instances_text = " ".join(instances).lower()
                    if key.lower() not in instances_text:
                        instances.append(key)
                        flag2 = True
                    while "  " in text:
                        text = text.replace("  ", " ").strip()
        return flag2, instances, text

    def finder(self, query, data):
        starting = ['compare ', 'equate ', 'match ', 'relate ', "describe ", "is ", "what ", "which ", "who ", "are "]
        finder_key_words = ['list', 'count', 'describe', 'compare', 'author', 'organizations', 'topic', 'citation',
                            'publication', 'authors', 'papers', 'conferences', 'organizations', 'topics', 'citations',
                            'publications', 'article', 'articles', 'journals', 'top', 'best', 'main', 'tell', 'years',
                            'year', 'important', 'name', 'names', "how", "many", "which", "help", "search"]
        starting += [x + " " for x in language.key_words]
        ending = [" " + x for x in language.key_words]
        # text = query.lower()
        text = []
        for w in data["token"]:
            if w not in data["stop"]:
                text.append(w)
        text = " ".join(text)

        for w in finder_key_words:
            text = text.replace(" " + w + " ", " ")
        for w in ending:
            if text.endswith(w):
                text = text.replace(w, "")

        for w in starting:
            if text.startswith(w):
                text = text.replace(w, "")
        while "  " in text:
            text = text.replace("  ", " ")
        flag = True
        instances = []
        i = 0
        while flag and i < 4:
            i += 1
            flag2 = False
            data = gr.verify_data2(text)
            data2 = gr.verify_dsc_data2(text)

            flag2, instances, text = self.check_finder_results(data, query, text, instances, flag2)
            flag2, instances, text = self.check_finder_results(data2, query, text, instances, flag2)

            instances = list(set(instances))

            if flag2 and len(text.replace(" ", "")) > 0:
                flag = True
            else:
                flag = False
        new_instances = []
        for ins in instances:
            if len(ins.split(" ")) > 1:
                new_instances.append(ins)
        return new_instances

    def find_valid_entities(self, data):
        # check entities for keywords
        if len(data['entities']) > 0:
            entities = []
            for entity in data['entities']:
                new_entity = []
                ws = entity['word'].split(" ")
                for w in ws:
                    if w.lower() not in language.entity_keywords:
                        new_entity.append(w)
                entity['word'] = " ".join(new_entity)
                if len(entity['word']) > 0:
                    entities.append(entity)
            data['entities'] = entities

        # get list of NER entities
        en = []
        ins = []

        if len(data['entities']) > 0:
            for entity in data['entities']:
                en.append(entity['word'].lower())
                ins.append(entity['word'])

        if len(data['dates']) > 0:
            for d in data['dates']:
                en.append(d["word"].lower())

        # check noun_phrases for keywords
        if len(data['nouns_phrases']) > 0:
            nouns_phrases = []
            for nouns_phrase in data['nouns_phrases']:
                new_nouns_phrase = []
                ws = nouns_phrase.split(" ")
                for w in ws:
                    if w.lower() not in language.key_words and w.lower() not in data.get("all_numbers"):
                        new_nouns_phrase.append(w)
                nouns_phrases.append(" ".join(new_nouns_phrase))

            verified_nouns_phrases = []

            for np in nouns_phrases:
                if np in get_np(np):
                    verified_nouns_phrases.append(np)

            data['nouns_phrases'] = verified_nouns_phrases

        # check nouns for keywords
        if len(data['nouns']) > 0:
            nouns = []
            for noun in data['nouns']:
                if noun.lower() not in language.key_words:
                    nouns.append(noun)
            data['nouns'] = nouns

        # check noun_phrases for entities
        if len(data['nouns_phrases']) > 0 and len(data['entities']) > 0:
            ents = []
            for ent in en:
                xw = ent.split(" ")
                ents.append({"force": len(xw), "ent": xw, "text": ent})

            nouns_phrases = []
            for word in data['nouns_phrases']:
                np = word.lower().split(" ")
                force = len(np)
                new_np = ""
                for wnp in np:
                    flag = False
                    for ent in ents:
                        if force < ent.get("force") and wnp in ent.get("text"):
                            flag = True
                            break
                    if not flag:
                        new_np += " " + wnp

                new_np = new_np.strip()
                if len(new_np) > 0:
                    nouns_phrases.append(new_np)

            verified_nouns_phrases = []

            for np in nouns_phrases:
                if np in get_np(np):
                    verified_nouns_phrases.append(np)

            data['nouns_phrases'] = verified_nouns_phrases

        # check nouns for noun_phrases
        if len(data['nouns']) > 0:
            nouns = []
            for noun in data["nouns"]:
                if noun not in " ".join(data["nouns_phrases"]):
                    nouns.append(noun)
            data['nouns'] = nouns

        # check nouns for entities
        if len(data['nouns']) > 0:
            nouns = []
            for word in data['nouns']:
                if word.lower() not in " ".join(en):
                    nouns.append(word)
            data["nouns"] = nouns
        ins += data["nouns"] + data["nouns_phrases"]

        # Verify quoted ins
        data['ins'] = list(set(ins))
        self.quote_verify(data)

        new_ins = []
        for inst in data['ins']:
            if inst.endswith("'s"):
                inst = inst[:-2]

            new_ins.append(inst)
        data['ins'] = new_ins

        # check for finder results
        finder_string = " ".join(data["finder"]).lower()
        new_ins_list = []
        for ins in data['ins']:
            if ins.lower() not in finder_string:
                new_ins_list.append(ins)
        new_ins_list = list(set(new_ins_list)) + data["finder"]
        data['ins'] = new_ins_list

        # control for first names
        new_ins_list = []
        for np in data['ins']:
            if np.lower() not in names:
                new_ins_list.append(np)
        data['ins'] = new_ins_list

        # control for stop words
        data["ins"] = stop_words_verify(data)

        # check for residual words
        verified_ordered_ins = []
        for np in data['ins']:
            if np.lower() not in residual_key_words:
                verified_ordered_ins.append(np)
        data['ins'] = verified_ordered_ins

        # check for lower/upper ins
        data["ins"] = list(set(data['ins']))
        sorted_ins = sorted(data['ins'], key=lambda x: len(x), reverse=True)
        new_ins_set = []
        new_ins_string = ""
        for ins in sorted_ins:
            ins_string = " | ".join(new_ins_set).lower()
            if ins.lower() == ins and ins.lower() not in ins_string:
                new_ins_set.append(ins)
                new_ins_string = " | ".join(new_ins_set)
            elif ins.lower() != ins and ins.lower() in ins_string:
                new_ins_string = new_ins_string.replace(ins.lower(), ins)
                new_ins_set = ins_string.split(" | ")
            elif ins.lower() != ins and ins.lower() not in ins_string and ins not in new_ins_string:
                new_ins_set.append(ins)
                new_ins_string = " | ".join(new_ins_set)
        data["ins"] = new_ins_string.split(" | ")

        # ORDER INS
        sorted_ins = sorted(data['ins'], key=lambda x: len(x), reverse=True)
        new_ins_list = []
        for ins in sorted_ins:
            ins_string = " ".join(new_ins_list)
            if ins not in ins_string:
                new_ins_list.append(ins)
        data['ins'] = new_ins_list
        sentence = " ".join(data.get("sentences"))
        to_ord_instances = []
        for i in data['ins']:
            if i in sentence:
                ind = sentence.index(i)
            else:
                ind = 0
            to_ord_instances.append([ind, i])
        sorted_instances = sorted(to_ord_instances, key=lambda x: x[0], reverse=False)
        ordered_ins = [x[1] for x in sorted_instances]
        data["ins"] = ordered_ins

        # control for instances
        instances = []
        dsc_instances = []
        for inst in data["ins"]:
            ws = inst.split()
            d = gr.verify_data2(inst)  # self.verify_data('cmd=fnd&ins=', inst)
            if d.get("result") != "ok" and len(ws) > 1:
                new_ws = []
                for w in ws:
                    if w not in language.key_words:
                        new_ws.append(w)
                new_ws = " ".join(new_ws)
                f = gr.verify_data2(new_ws)  # self.verify_data('cmd=fnd&ins=', new_ws)
                if f.get("result") != "ko" and f.get("result") != "kk":
                    d = f

            if d.get("result") != "ok" and len(ws) > 2:
                f = self.find_data(inst)
                if f is not None:
                    d = f

            if d.get("result") != "ko" and d.get("result") != "kk":
                d["orig_ins"] = inst
                instances.append(d)

            d = gr.verify_dsc_data2(inst)
            if d.get("result") != "ko" and d.get("result") != "kk":
                d["orig_ins"] = inst
                dsc_instances.append(d)

        data["instances"] = instances  # self.check_instances(instances, data)
        data["dsc_instances"] = dsc_instances
        return data

    def parse(self, question, v2=False, ai="odt", expand=False):
        # raise Exception("bye")
        self.ai = ai
        total_time = time.time() * 1000
        tt = time.time() * 1000
        if question is None or question == '':
            question = 'Hello!'
        question = ''.join([question[:1].lower(), question[1:]])
        question = self.synonyms(question)
        emb_question = self.model.encode(question)

        data = get_data(question)

        if self.debug:
            print("extract data from question ", time.time() * 1000 - tt)
            tt = time.time() * 1000

        qlist = re.findall('\"(.*?)\"', question)
        finder_list = self.finder(question, data)

        data["quoted"] = qlist
        data["finder"] = finder_list

        data = self.find_valid_entities(data)

        # paper_ins_list = self.in_paper_finder(question, data)
        # data["paper_ins"] = paper_ins_list

        verbs = data.get("verbs")

        data["syns"] = self.syns

        if len(verbs)>0 and (verbs[0] == "define" or verbs[0] == "search"):
            if verbs[0] == "define":
                result = self.open_domain_responder(data, {"data": data})
            else:
                result = self.paper_responder(data, {"data": data})
            self.save_log(result, question)
            return result

        if self.debug:
            print("NER and ins verify ", time.time() * 1000 - tt)
            tt = time.time() * 1000

        questions = self.grammar.get_questions(data, expand=expand) + self.pers_questions

        new_quest = [x[3:] if x[:2].isnumeric() else x for x in questions]
        if self.debug:
            print("grammar elaboration ", time.time() * 1000 - tt)
            tt = time.time() * 1000

        embeddings = self.model.encode(new_quest)

        scores = cos_sim(emb_question, embeddings)

        scored_questions = []
        for i, score in enumerate(scores[0]):
            if score >= self.threshold:
                scored_questions.append((round(score.item(), 4), questions[i]))
        sorted_scored_questions = sorted(scored_questions, key=lambda x: x[0], reverse=True)

        if self.debug:
            print("similarity process ", time.time() * 1000 - tt)
            tt = time.time() * 1000

        new_list = []

        # Replace illegal values for ordering
        if len(data['numbers2']) != 0:
            order = 'in the last ' + data['numbers2'][0] + ' years'
            if data['numbers2'][0].isnumeric() and 2 < int(data['numbers2'][0]) < 8:
                for question in sorted_scored_questions:
                    new_list.append((question[0], question[1].replace(order, 'in the last 5 years')))
            else:
                for question in sorted_scored_questions:
                    new_list.append((question[0], question[1].replace(order, '')))
        else:
            new_list = sorted_scored_questions

        result = {"questions": new_list[:5]}

        if len(new_list) > 0 and new_list[0][1] in self.pers_questions:
            answer = self.pers_answers[new_list[0][1]]
            if isinstance(answer, list):
                answer = answer[int(random.randint(0, len(answer) - 1))]
            result["answer"] = answer

        result["data"] = data

        if len(result["questions"]) > 0 and result.get("answer") is None:
            query = {}
            q = result.get("questions")[0][1]
            if q.startswith("01 "):
                query = {"intent": "count"}
            elif q.startswith("03 "):  # or q.startswith("who")
                query = {"intent": "describe"}
            elif q.startswith("04 "):
                query = {"intent": "compare"}
            elif q.startswith("02 "):
                query = {"intent": "list"}
            elif q.startswith("05 "):
                query = {"intent": "search"}
            elif q.startswith("06 "):
                query = {"intent": "open"}
            intent = query.get("intent")
            if intent is not None and intent == "count" or intent == "list":
                if result.get("data").get("dsc_instances") is not None:
                    del result.get("data")["dsc_instances"]
                sub = None
                sub_id = None
                order = None
                qq = q.split(" ")
                for w in qq:
                    if w.endswith("s"):
                        w = w[:-1]
                    if w in dictionary.get("sub").get(intent):
                        sub = w
                        sub_id = dictionary.get("sub").get(intent).index(w)
                        break
                if sub is not None:
                    query["subject"] = sub + "s"
                    query["sub_id"] = sub_id + 1
                if intent == 'list' and len(data['numbers']) > 0:
                    query["num"] = data['numbers'][0]
                if intent == 'list':
                    if "most cited" in q:
                        order = 2
                    if qq[-1] == "publications":
                        order = 1
                    if qq[-1] == "citations":
                        order = 2
                    if qq[-1] == "years":
                        if qq[-6] == "publications":
                            order = 3
                        if qq[-6] == "citations":
                            order = 4
                if order is not None:
                    query["order"] = dictionary.get("order").get("ways")[order - 1]
                    query["order_id"] = order

                ver_questions = self.grammar.final_questions
                for group in ver_questions:
                    if result["questions"][0][1] in group.get("questions"):
                        query["instances"] = group.get("instances")
                        break
                    else:
                        query["instances"] = result.get("data")["instances"]

            if intent is not None and intent == "describe" or intent == "compare":
                if result.get("data").get("instances") is not None:
                    del result.get("data")["instances"]

                ver_questions = self.grammar.final_questions
                for group in ver_questions:
                    if result["questions"][0][1] in group.get("questions"):
                        query["instances"] = group.get("dsc_instances")
                        break
                    else:
                        query["instances"] = result.get("data")["dsc_instances"]


            if intent is not None and (intent == "search" or intent == "open"):
                if data.get("papers") is None:
                    self.get_papers(data)
                ver_questions = self.grammar.final_questions
                for group in ver_questions:
                    if result["questions"][0][1] in group.get("questions"):
                        query["instances"] = group.get("instances")
                        break
                    else:
                        query["instances"] = result.get("data")["instances"]

            query["question"] = result["questions"][0][1]
            query["score"] = result["questions"][0][0]

            if " accepted by a conference" in query["question"]:
                # noinspection PyTypeChecker
                query["instances"].append({
                    "result": "ok",
                    "object": "conferences",
                    "obj_id": 2,
                    "item": "*"
                })

            if " published by a journal" in query["question"]:
                # noinspection PyTypeChecker
                query["instances"].append({
                    "result": "ok",
                    "object": "journals",
                    "obj_id": 5,
                    "item": "*"
                })

            self.query = query

            if self.debug:
                print("intent discovery ", time.time() * 1000 - tt)
                tt = time.time() * 1000

            ans = self.get_answer(result, v2)
            if isinstance(ans, str) and len(ans) > 0:
                query["answer"] = answer_clean(ans)
                query["html_answer"] = ans
            else:
                query["answer"] = ans
            if self.debug:
                print("get answer ", time.time() * 1000 - tt)
            result["query"] = query

        elif len(result["questions"]) == 0 :
            query = {"intent": "open"}
            if self.debug:
                tt = time.time() * 1000
            if data.get("papers") is None:
                self.get_papers(data)
            od = open_domain.open_domain(data, ai=self.ai)
            query["answer"] = od.get("answer")
            query["url"] = od.get("url")
            query["biblio"] = od.get("biblio")
            query["source"] = od.get("source")
            result["query"] = query
            if self.debug:
                print("get best contest ", time.time() * 1000 - tt)

        if isinstance(result, dict) and result.get("query") is not None and result.get("query").get(
                "answer") is not None and isinstance(result.get("query").get("answer"), dict) and (result.get(
            "query").get("answer").get("result") == "ko" or result.get("query").get("answer") == {}):
            result = self.open_domain_responder(data, result)

        # Save log
        self.save_log(result, question)

        if self.debug:
            print("Total time ", time.time() * 1000 - total_time)


        if isinstance(result, dict) and result.get("query") is not None:
            qa = result.get("query").get("answer")
            if qa is not None:
                result["query"]["answer"] = self.syns_answer_adjuster(qa)

            qa = result.get("query").get("html_answer")
            if qa is not None:
                result["query"]["html_answer"] = self.syns_answer_adjuster(qa)

        return result

    def syns_answer_adjuster(self, qa):
        if len(self.syns) > 0 and qa is not None:
            qa = json.dumps(qa)
            for key in self.syns:
                qa = qa.replace(key, self.syns[key])
            qa = json.loads(qa)
        return qa

    def save_log(self, result, question):
        if question != "hi":
            with open(self.questions_log_file, "a", encoding="utf-8") as text_file:
                print(json.dumps(result), file=text_file)
            with open(self.questions_file, "a", encoding="utf-8") as text_file:
                print(question, file=text_file)

    def syns_adjuster(self, data):
        new_sentences = []
        for key in self.syns:
            for sentence in data.get("sentences"):
                sentence = sentence.replace(key, self.syns[key])
                new_sentences.append(sentence)
        data["sentences"] = new_sentences
        return data


    def open_domain_responder(self, data, result):
        query = {"intent": "open"}
        data = self.syns_adjuster(data)
        self.get_papers(data)
        od = open_domain.open_domain(data, ai=self.ai)
        query["answer"] = od.get("answer")
        query["url"] = od.get("url")
        query["source"] = od.get("source")
        query["biblio"] = od.get("biblio")
        result["query"] = query
        return result

    def paper_responder(self, data, result):
        query = {"intent": "open"}
        data = self.syns_adjuster(data)
        self.get_papers(data)
        od = paper_search.get_paper_info(data=data)
        query["answer"] = od.get("answer")
        query["url"] = od.get("url")
        query["source"] = od.get("source")
        result["query"] = query
        return result

    @staticmethod
    def get_papers(data, num=10):

        interrogative = {"?"}
        words = set(data.get("token"))
        words.difference_update(set(data.get("stop")),set(data.get("verbs")),interrogative)
        words = list(words)
        new_words = []
        check_words = language.key_words + language.residual_key_words + language.entity_keywords
        for w in words:
            if w.lower() not in check_words:
                new_words.append(w)
        words = new_words

        # get data from aida papers
        papers = {"result": "ok", "papers": []}
        if data.get("quoted") is not None and len(data.get("quoted")) > 0:
            paper = json.loads(aidaLogic.find(" ".join(data.get("quoted")), num))
            if paper.get("papers") is not None:
                papers["papers"] += paper.get("papers")
                data["papers"] = papers
                return


        if data.get("ins") is not None and len(data.get("ins"))>0:
            n = int(num/len(data.get("ins")))+1
            for ins in data.get("ins"):
                paper = json.loads(aidaLogic.find(ins, n))
                if paper.get("papers") is not None:
                    papers["papers"] += paper.get("papers")

        paper = json.loads(aidaLogic.find(" ".join(data['sentences']), num))
        if paper.get("papers") is not None:
            papers["papers"] += paper.get("papers")

        for w in words:
            n = int(num / len(words)) + 1
            paper = json.loads(aidaLogic.find(w, n ))
            if paper.get("papers") is not None:
                papers["papers"] += paper.get("papers")

        uni_papers = []
        for pap in papers["papers"]:
            del(pap["mode"])
            pap = json.dumps(pap)
            if pap not in uni_papers:
                uni_papers.append(pap)
        uni_papers = [json.loads(x) for x in uni_papers]
        papers["papers"] = uni_papers

        data["papers"] = papers

    def find_data(self, inst):
        splitted_inst = inst.split(" ")
        for i, word in enumerate(splitted_inst):
            if i < len(splitted_inst) - 1:
                ins = (" ".join([word, splitted_inst[i + 1]])).lower()
                d = gr.verify_data2(ins)  # self.verify_data('cmd=fnd&ins=', ins)
                if d.get("result") == "ok":
                    return d
                if d.get("result") == "ka":
                    name = d.get("item")[0].get("name")
                    acronym = d.get("item")[0].get("acronym")
                    texts = []
                    if name is not None:
                        texts.append(name)
                    if acronym is not None:
                        texts.append(acronym)
                    sim = self.grammar.get_similar(ins, texts)
                    if sim[0][0] > self.grammar.threshold:
                        return d
        return None

    def get_answer(self, total_result, v2=False):
        query = self.query
        intent = query.get("intent")
        total_instances = query.get("instances")
        if total_instances is None:
            total_instances = []
        instances = []
        k2 = []
        ka = []
        flag = True
        if total_instances is None:  # or len(total_instances) == 0:
            flag = False
        else:
            for ins in total_instances:
                r = ins.get("result")
                if r == "ok" and ins.get('k2') is None:
                    instances.append(ins)
                else:
                    flag = False
                    if ins.get('k2') is not None or r == "k2":
                        k2.append(ins)
                    else:
                        ka.append(ins)

        if flag or intent == "search" or intent == "open":
            return self.make_answer(instances, total_result, v2)
        else:
            answers = {}
            multi_urls = {}
            if len(k2) == 1 and len(ka) == 0:
                ins = k2[0]

                if ins.get("result") == "ok" and ins.get('k2') is not None:
                    ins = ins.get('k2')


                if intent == "describe" or intent == "compare":
                    ins['cmd'] = 'dsc'
                    msg = k2_message(ins)
                    answers["msg"] = msg
                    for i, n in enumerate(ins.get("keys")):
                        if len(n) > 0:
                            for j, k in enumerate(n):
                                ins_id = str(k.get("id")) if i == 0 else k.get("name")
                                d = json.loads(aidaLogic.dsc_finder(ins_id))
                                answers[j + 1] = self.make_answer(instances + [d], total_result, v2)

            elif len(k2) == 2 and len(ka) == 0:
                ins1 = k2[0]
                ins2 = k2[1]
                if ins1.get("result") == "ok" and ins1.get('k2') is not None:
                    ins1 = ins1.get('k2')
                if ins2.get("result") == "ok" and ins2.get('k2') is not None:
                    ins2 = ins2.get('k2')

                if intent == "compare":
                    ins1['cmd'] = 'dsc'
                    ins2['cmd'] = 'dsc'
                    answers["msg"] = [k2_message(ins1), k2_message(ins2)]
                    group1 = []
                    for i, n in enumerate(ins1.get("keys")):
                        if len(n) > 0:
                            for j, k in enumerate(n):
                                ins_id = str(k.get("id")) if i == 0 else k.get("name")
                                d = json.loads(aidaLogic.dsc_finder(ins_id))
                                group1.append(d)
                    group2 = []
                    for i, n in enumerate(ins2.get("keys")):
                        if len(n) > 0:
                            for j, k in enumerate(n):
                                ins_id = str(k.get("id")) if i == 0 else k.get("name")
                                d = json.loads(aidaLogic.dsc_finder(ins_id))
                                group2.append(d)
                    for i, in1 in enumerate(group1):
                        for j, in2 in enumerate(group2):
                            comb = instances + [in1, in2]
                            answers[str(i + 1) + "-" + str(j + 1)] = self.make_answer(comb, total_result, v2)

            if intent == "compare" or intent == "describe":
                get_function = self.get_dsc_correct_ka_data
            else:
                get_function = self.get_correct_ka_data

            if len(ka) == 1 and len(k2) == 0:
                ins = ka[0]
                msg = ka_message(ins)
                answers["msg"] = msg
                new_instances = get_function(ins)


                for i, new_ins in enumerate(new_instances):
                    answers[i + 1] = self.make_answer(instances + [new_ins], total_result, v2)
                    multi_urls[i + 1] = total_result.get("paper_urls")
                    answers["paper_urls"] = multi_urls

            elif len(ka) == 2 and len(k2) == 0:
                ins1 = ka[0]
                ins2 = ka[1]
                answers["msg"] = [ka_message(ins1), ka_message(ins2)]
                new_instances_1 = get_function(ins1)
                new_instances_2 = get_function(ins2)
                for i, new_ins_1 in enumerate(new_instances_1):
                    for j, new_ins_2 in enumerate(new_instances_2):
                        comb = instances + [new_ins_1, new_ins_2]
                        answers[str(i + 1) + "-" + str(j + 1)] = self.make_answer(comb, total_result, v2)
                        multi_urls[str(i + 1) + "-" + str(j + 1)] = total_result.get("paper_urls")
                        answers["paper_urls"] = multi_urls

            instances = []
            for ins in total_instances:
                instances.append(ins)
            return answers

    @staticmethod
    def get_correct_ka_data(ins):
        objs = ['topics', 'conferences', 'organizations', 'authors', 'papers', 'journals']
        obj_id = ins.get("obj_id")
        ind = int(obj_id-1)
        if ind < 0:
            ind = 0
        if ind > 5:
            ind = 5
        ins_object = objs[ind]
        new_instances = []
        for i, item in enumerate(ins.get("item")):
            new_ins = {"result": "ok", "object": ins_object, "obj_id": obj_id}
            if obj_id == 2:
                new_ins["item"] = item.get("acronym")
            else:
                new_ins["item"] = item.get("name")
            if obj_id == 2 or obj_id == 4:
                new_ins["id"] = item.get("id")
            new_instances.append(new_ins)
        return new_instances

    @staticmethod
    def get_dsc_correct_ka_data(ins):
        obj_id = ins.get("obj_id")
        items = ins.get("item")
        new_instances = []
        for item in items:
            idx = str(item.get('id'))
            if obj_id == 4:
                idx = '0000000000' + str(idx)
            new_ins = json.loads(aidaLogic.dsc_finder(idx))
            new_instances.append(new_ins)
        return new_instances

    def make_answer(self, instances, total_result, v2):
        query = self.query
        sub = query.get("sub_id")
        intent = query.get("intent")
        result = {"result": "ko", "answer": "Query not implemented yet"}
        ins = ""
        obj = ""
        ins2 = ""
        obj2 = ""
        ins3 = ""
        obj3 = ""
        lun = len(instances)
        req_props = {}
        v2_req_props = {}
        v2_result = {}
        if lun > 0:
            obj = instances[0].get("obj_id")
            obj_name = instances[0].get("object")
            if obj_name is not None and obj_name not in req_props:
                req_props[obj_name] = []
                v2_req_props[obj_name] = []
            if obj_name is not None:
                req_props[obj_name].append(instances[0].get("item"))
                v2_req_props[obj_name].append(instances[0].get("item"))

            if instances[0].get("item") == "*":
                ins = instances[0].get("item")
            else:
                ins = instances[0].get("item") if obj != 4 and obj != 2 else instances[0].get("id")

        if lun > 1:
            obj2 = instances[1].get("obj_id")

            obj_name = instances[1].get("object")
            if obj_name is not None and obj_name not in req_props:
                req_props[obj_name] = []
            if obj_name is not None:
                req_props[obj_name].append(instances[1].get("item"))

            if instances[1].get("item") == "*":
                ins2 = instances[1].get("item")
            else:
                ins2 = instances[1].get("item") if obj2 != 4 and obj2 != 2 else instances[1].get("id")

        if lun > 2:
            obj3 = instances[2].get("obj_id")

            obj_name = instances[2].get("object")
            if obj_name is not None and obj_name not in req_props:
                req_props[obj_name] = []
            if obj_name is not None:
                req_props[obj_name].append(instances[2].get("item"))

            if instances[2].get("item") == "*":
                ins3 = instances[2].get("item")
            else:
                ins3 = instances[2].get("item") if obj3 != 4 and obj3 != 2 else instances[2].get("id")

        if intent == "count":
            if ins is not None and ins2 is not None and ins3 is not None:
                result = json.loads(aidaLogic.how(sub, obj, ins, ins2, obj2, ins3, obj3))
                if v2:
                    v2_result = json.loads(aidaLogic.how(sub, obj, ins, "", "", "", ""))
                    v2_result["subject"] = query.get("subject")
                result["subject"] = query.get("subject")

                if lun == 0:
                    result["all"] = True
                    if v2:
                        v2_result["all"] = True
                else:
                    result["all"] = False
                    if v2:
                        v2_result["all"] = False

                result["req_props"] = req_props
                if v2:
                    v2_result["req_props"] = v2_req_props
                    v2_result = count_query_answer(v2_result)
                    total_result["v2_result"] = v2_result
                result = count_query_answer(result)
            else:
                result = ko_message()

        elif intent == "list":
            num = query.get("num") if query.get("num") is not None else 10
            order_id = query.get("order_id") if query.get("order_id") is not None else ("1" if sub != 2 else "2")
            condition = ins is not None and ins2 is not None and ins3 is not None
            if lun == 0:
                result = json.loads(
                    aidaLogic.lst(str(sub), "", "", "", "", "", "", "", str(order_id)))

                result["num"] = num
                result["order"] = int(order_id)
                result["sub_id"] = sub
                result["subject"] = query.get("subject")
                result["req_props"] = req_props

                urls_data = self.get_urls(result)
                if urls_data is not None:
                    total_result["paper_urls"] = urls_data

                result = list_query_answer(result)

                if v2:
                    v2_result = json.loads(
                        aidaLogic.lst(str(sub), str(obj), ins, "", "", "", "", str(num), str(order_id)))
                    v2_result["num"] = num
                    v2_result["order"] = int(order_id)
                    v2_result["sub_id"] = sub
                    v2_result["subject"] = query.get("subject")
                    v2_result["req_props"] = v2_req_props
                    v2_result = list_query_answer(v2_result)
                    total_result["v2_result"] = v2_result

            elif condition and num is not None and order_id is not None:
                result = json.loads(
                    aidaLogic.lst(str(sub), str(obj), ins, ins2, str(obj2), ins3, str(obj3), str(num),
                                  str(order_id)))
                result["num"] = num
                result["order"] = int(order_id)
                result["sub_id"] = sub
                result["subject"] = query.get("subject")
                result["req_props"] = req_props
                urls = self.get_urls(result)
                if urls is not None:
                    total_result["paper_urls"] = urls
                result = list_query_answer(result)
                if v2:
                    try:
                        v2_result = json.loads(
                            aidaLogic.lst(str(sub), str(obj), ins, "", "", "", "", str(num), str(order_id)))
                        v2_result["num"] = num
                        v2_result["order"] = int(order_id)
                        v2_result["sub_id"] = sub
                        v2_result["subject"] = query.get("subject")
                        v2_result["req_props"] = v2_req_props
                        v2_result = list_query_answer(v2_result)
                        total_result["v2_result"] = v2_result
                    except:
                        v2_result = {"result": "ko", "answer": "There was an error!"}
                        total_result["v2_result"] = v2_result
            else:
                result = ko_message()
        elif intent == "describe":
            if lun > 0 and instances[0].get("k2") is not None:
                instances[0] = instances[0].get("k2")
                result = ko_message()
            elif lun > 0:
                result = dsc(instances[0])
        elif intent == "compare":
            if lun > 0 and instances[0].get('k2') is not None or instances[1].get('k2') is not None:
                return ko_message()
            result = cmp(instances[0], instances[1])
        elif intent == "search":
            od = paper_search.get_paper_info(total_result.get("data"))
            result = od.get("answer")
            self.query["url"] = od.get("url")
            self.query["source"] = od.get("source")
        elif intent == "open":
            od = open_domain.open_domain(total_result.get("data"), ai=self.ai)
            result = od.get("answer")
            self.query["url"] = od.get("url")
            self.query["source"] = od.get("source")
            self.query["biblio"] = od.get("biblio")
        return result

    @staticmethod
    def get_urls(result):
        if result['result'] == 'ok' and result.get('lst') is not None and len(result['lst']) > 0 and result.get(
                "subject") == "papers":
            urls = []
            dois = []
            for element in result['lst']:
                if element.get("urls") is not None:
                    urls.append(element.get("urls"))
                else:
                    urls.append("")
                doi = element.get("doi")
                if doi is not None and not (doi == "" or len(doi) < 3):
                    doi = "https://doi.org/" + doi
                dois.append(doi)
            return {"urls": urls, "doi": dois}
        return None

    @staticmethod
    def in_paper_finder(question, data):
        paper_ins = []
        if data.get("ins") is not None and len(data.get("ins"))>0:
            n = int(10/len(data.get("ins")))+1
            for ins in data.get("ins"):
                paper = json.loads(aidaLogic.find(ins, n))
                # data["papers"] = paper
                for p in paper.get("papers"):
                    if p.get("mode") == 0:
                        paper_ins.append(ins)
                        break
        print(paper_ins)
        return paper_ins

