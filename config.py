
# debug mode
debug_mode = True  # False
debug = True


# aidalogic section
# elasticsearch_host = 'IP of server where elasticsearch runs'
elasticsearch_host = "192.168.1.150"

# papers index
index = 'aida_2020'

# authors index
author_index = 'authors_2020'

# dsc authors index
dsc_authors_index = 'authors'

# dsc conferences index
dsc_conferences_index = 'conferences'

# dsc organizations index
dsc_organizations_index = 'organizations'

# fuzzy search threshold
threshold = 85

questions_log_file = 'questions_log.txt'
questions_file = 'questions.txt'
names_file = "names.json"

