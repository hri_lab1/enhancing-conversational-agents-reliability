# import sys
import json
import datetime
from elasticsearch import Elasticsearch
# from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from config import elasticsearch_host as host
from config import index, author_index, dsc_authors_index, dsc_conferences_index, dsc_organizations_index, threshold


es = Elasticsearch([{'host': host, 'port': 9200, 'scheme': 'http'}], timeout=120)


def authors_name(authors):
    msg = ''
    if len(authors) > 1:
        msg = ' et al.'
    for a in authors:
        if a['order'] == 1:
            msg = a['name'] + msg
    return msg


def result_combinator(res1, res2, res3):
    objects = ["topics", "conferences", "organizations", "authors", "journals"]
    res1 = json.loads(res1)
    if res2 is None or res3 is None or (res1['result'] == 'ok' and '-' in res1['item']):
        return json.dumps(res1)
    results = [res1, json.loads(res2), json.loads(res3)]

    keys = [[], [], [], [], [], []]
    num = [0, 0, 0, 0, 0, 0]
    for res in results:
        if res['result'] == 'ok' and res['item'] not in keys[res['obj_id'] - 1]:
            num[res['obj_id'] - 1] += 1
            keys[res['obj_id'] - 1].append(res['item'])
        if res['result'] == 'k2':
            for i in range(len(res['num'])):
                for key in res['keys'][i]:
                    if key not in keys[i]:
                        keys[i].append(key)
                        num[i] += 1

    if sum(num) == 1:
        obj_id = num.index(1)
        item = keys[obj_id][0]
        return json.dumps({'result': 'ok', 'object': objects[obj_id], 'obj_id': obj_id + 1, 'item': item})

    if 1 < sum(num) < 11:
        return json.dumps({'result': 'k2', 'num': num, 'keys': keys})

    if sum(num) > 10:
        return json.dumps({'result': 'kk', 'num': num})

    # nessun risultato
    return json.dumps({'result': 'ko', 'object': '', 'obj_id': 0, 'keys': [], 'num': 0})


# finds correspondence between the parameter and one of the database fields in the fields list
def find_match(ins):
    # da eliminare una volta corretti i dati su elasticsearch
    if ins == 'eswc' or ins == 'ESWC':
        return json.dumps({"result": "ok", "object": "conferences", "obj_id": 2, "item": "ESWC", "id": "1180003657"})

    if '-' in ins:
        result1 = find_match(ins.replace('-', ''))
        result2 = find_match(ins.replace('-', ' '))
    else:
        result1 = None
        result2 = None

    fields = ["cso_enhanced_topics", "confseries", "affiliation", "name", "journame", "confseriesname"]
    es_index = [index, index, author_index, author_index, index, index]
    objects = ["topics", "conferences", "organizations", "authors", "journals", "conferences"]

    obj_id = 0
    res0 = []
    res1 = []
    res2 = []
    res3 = []
    found_hits = []
    found_keys = []
    found_keywords = []
    cat_found = 0
    keywords_found = 0
    keys = [[], [], [], [], [], []]
    keys2 = [[], [], [], [], [], []]

    # ricerca per chiave esatta
    for i in range(6):
        query_body = {"track_total_hits": 'true', "size": 0, "query": {"match_phrase": {fields[i] + ".keyword": ins}},
                      "aggs": {"A": {"cardinality": {"field": fields[i] + ".keyword"}}}}
        res0.append(es.search(index=es_index[i], body=query_body))
        found_keywords.append(res0[i]['aggregations']['A']['value'])

        if found_keywords[i] > 0:
            keywords_found = keywords_found + 1

    if keywords_found == 1:
        obj_id = found_keywords.index(max(found_keywords))
        result = json.dumps({'result': 'ok', 'object': objects[obj_id], 'obj_id': obj_id + 1, 'item': ins})
        if result2 is not None:
            result = result_combinator(result, result1, result2)
        return result

    if keywords_found > 1:
        for i in range(6):
            if found_keywords[i] > 0:
                keys[i] = [ins]

                found_keywords[i] = 1
        result = json.dumps({'result': 'k2', 'num': found_keywords, 'keys': keys})
        if result2 is not None:
            result = result_combinator(result, result1, result2)
        return result

    # ricerca per frase
    for i in range(6):
        res1.append(es.search(index=es_index[i],
                              body={"track_total_hits": 'true', "size": 0, "query": {"match_phrase": {fields[i]: ins}},
                                    "aggs": {"A": {"cardinality": {"field": fields[i] + ".keyword"}}}}))
        res2.append(es.search(index=es_index[i],
                              body={"track_total_hits": 'true', "size": 0, "query": {"match_phrase": {fields[i]: ins}},
                                    "aggs": {"A": {"terms": {"field": fields[i] + ".keyword", "size": 100}}}}))
        res3.append(es.search(index=es_index[obj_id], body={"track_total_hits": 'true', "size": 0, "query": {
            "match_phrase": {fields[obj_id] + ".keyword": ins.lower()}}, "aggs": {
            "A": {"cardinality": {"field": fields[obj_id] + ".keyword"}}}}))
        found_hits.append(res1[i]['aggregations']['A']['value'])
        found_keys.append(res3[i]['aggregations']['A']['value'])

        if found_hits[i] > 0:
            cat_found = cat_found + 1
            for j in res2[i]['aggregations']['A']['buckets']:
                keys2[i].append(j['key'].lower())
                keys[i].append(j['key'])

    # risolve il problema che es non restituisce i singoli valori dei topics nelle aggregazioni
    # perché sono contenuti in un array
    keys[0] = [s for s in keys[0] if ins.lower() in s]
    keys2[0] = keys[0]

    found_hits[0] = len(keys[0])
    obj_id = found_hits.index(max(found_hits))
    for i in range(6):
        keys[i] = keys[i][:3]
        keys2[i] = keys2[i][:3]

    # caso con più di tre risultati in una categoria
    if max(found_hits) > 3 and cat_found > 1:
        # result = json.dumps({'result': 'kk', 'num': found_hits})
        keys = [keys[0][:3], keys[1][:3], keys[2][:3], keys[3][:3], keys[4][:3]]
        found_hits = [len(a) for a in keys]
        result = json.dumps({'result': 'k2', 'num': found_hits, 'keys': keys})
        result = result_combinator(result, result1, result2)
        return result

    # caso con 3 o meno risultati in più di una categoria
    if cat_found > 1:
        result = json.dumps({'result': 'k2', 'num': found_hits, 'keys': keys})
        result = result_combinator(result, result1, result2)
        return result

        # caso con 1 risultato utile
    num = found_hits[obj_id]
    num2 = found_keys[obj_id]

    if num == 1 or num2 == 1 or (ins.lower() in keys2[obj_id]):
        if ins.lower() in keys2[obj_id]:
            item = keys[obj_id][keys2[obj_id].index(ins.lower())]
        else:
            item = keys[obj_id][0]

        result = json.dumps({'result': 'ok', 'object': objects[obj_id], 'obj_id': obj_id + 1, 'item': item})
        result = result_combinator(result, result1, result2)
        return result

        # caso con più di tre risultati in una sola categoria
    elif num > 3 or sum(found_hits) > 10:
        keys = [keys[0][:3], keys[1][:3], keys[2][:3], keys[3][:3], keys[4][:3]]
        found_hits = [len(a) for a in keys]
        result = json.dumps({'result': 'k2', 'num': found_hits, 'keys': keys})
        # result = json.dumps({'result': 'kk', 'num': found_hits})
        result = result_combinator(result, result1, result2)
        return result

    # ricerca fuzzy
    res0 = []
    found = [[], [], [], [], [], []]
    keys = [[], [], [], [], [], []]
    source = ["cso_enhanced_topics", "confseries", "affiliation", ["name", "affiliation"], "journame", "confseriesname"]

    # ricerca in es come per la ricerca esatta ma con il parametro fuzziness e con match al posto di match_phrase
    for i in range(6):
        res0.append(es.search(index=es_index[i], body={"size": 10, "track_total_hits": "true", "_source": source[i],
                                                       "query": {"match": {
                                                           fields[i]: {"query": ins, "fuzziness": "auto",
                                                                       "max_expansions": 50, "prefix_length": 0}}}}))

        # elimina i doppioni
        voice = []
        for data in res0[i]['hits']['hits']:
            if data['_source'][fields[i]] not in voice:
                voice.append(data['_source'][fields[i]])
                found[i].append(data['_source'][fields[i]])

    # genera una lista per i topic a partire dalle liste recuperate nei 10 documenti da es, eliminando i doppioni
    flat = []
    flat_list = [item for topic in found[0] for item in topic]
    for element in flat_list:
        if element not in flat:
            flat.append(element)
    found[0] = flat

    # estrae i valori più probabili e calcola il punteggio: se ci sono valori sopra soglia
    # li salva nella lista delle chiavi più probabili
    for i in range(6):
        found[i] = process.extract(ins, found[i], limit=3)
        for data in found[i]:
            if data[1] > threshold:
                keys[i].append(data[0])

    # prende solo i primi tre valori per ogni campo e verifica se siamo in presenza
    # di un unico valore candidato e in tal caso lo restituisce
    keys = [keys[0][:3], keys[1][:3], keys[2][:3], keys[3][:3], keys[4][:3], keys[5][:3]]
    num = [len(a) for a in keys]
    if sum(num) == 1:
        obj_id = num.index(1)
        item = keys[obj_id][0]
        return json.dumps({'result': 'ok', 'object': objects[obj_id], 'obj_id': obj_id + 1, 'item': item})

    for i in range(6):
        flat = []
        for data in found[i]:
            if data[1] > (threshold - 15):
                flat.append(data[0])
            found[i] = flat[:3]

    num = [len(a) for a in found]

    if sum(num) == 1:
        obj_id = num.index(1)
        item = found[obj_id][0]

        result = json.dumps({'result': 'ok', 'object': objects[obj_id], 'obj_id': obj_id + 1, 'item': item})
        result = result_combinator(result, result1, result2)
        return result

    if 1 < sum(num) < 11:
        result = json.dumps({'result': 'k2', 'num': num, 'keys': found})
        result = result_combinator(result, result1, result2)
        return result

    if sum(num) > 10:
        return json.dumps({'result': 'kk', 'num': num})

    # nessun risultato
    return json.dumps({'result': 'ko', 'object': '', 'obj_id': 0, 'keys': [], 'num': 0})


# disambiguazione conferenze omonime per ricerca fnd
def check_conference(result):
    # da eliminare una volta corretti i dati su elasticsearch
    if result['item'] == 'ESWC':
        return json.dumps(result)
    if result["obj_id"] == 2:
        res = es.search(index=dsc_conferences_index,
                        body={"track_total_hits": "true", "query": {"match_phrase": {"acronym": result['item']}}})
    else:
        res = es.search(index=dsc_conferences_index,
                        body={"track_total_hits": "true", "query": {"match_phrase": {"name": result['item']}}})
    hits = res['hits']['total']['value']
    if hits == 1:
        conference_id = res['hits']['hits'][0]['_source']['id']
        result['id'] = conference_id
        result["obj_id"] = 2
        result["name"] = res['hits']['hits'][0]['_source']["name"]
        result['item'] = res['hits']['hits'][0]['_source']["acronym"]
    elif hits > 1:
        conferences = []
        for conf in res['hits']['hits']:
            conferences.append(
                {'name': conf['_source']['name'], 'acronym': conf['_source']['acronym'], 'id': conf['_source']['id']})
        result['result'] = 'ka'
        result['item'] = conferences
    elif hits < 1:
        result['result'] = 'ko'
    return json.dumps(result)


# ricerca ultima affiliazione per id autore
def get_last_affiliation(_id):
    res = es.search(index=dsc_authors_index, body={"track_total_hits": "true", "query": {"match_phrase": {'id': _id}}})
    if res['hits']['total']['value'] == 1:
        return res['hits']['hits'][0]['_source'].get('last_affiliation').get('affiliation_name')
    query_body = {"_source": ["year", "authors.id", "authors.name", "authors.affiliation"], "track_total_hits": 'true',
                  "size": 10000, "sort": [{"year": {"order": "desc"}}], "query": {
            "bool": {"must": [{"match_phrase": {"authors.id": _id}}, {"exists": {"field": "authors.affiliation"}}]}}}
    res = es.search(index=index, body=query_body)
    for paper in res['hits']['hits']:
        for author in paper['_source']['authors']:
            if author['id'] == _id and author.get('affiliation') is not None:
                return author['affiliation']
    return None


# disambiguazione autori omonimi per ricerca fnd
def check_author(result):
    query_body = {"track_total_hits": "true", "query": {
        "bool": {"must": [{"match_phrase": {"name.keyword": result['item']}}, {"exists": {"field": "name"}}]}},
                  "aggs": {"a": {"terms": {"field": "id", "size": 100}}}}
    res = es.search(index=author_index, body=query_body)
    hits = res['hits']['total']['value']
    unique_id_num = len(res['aggregations']['a']['buckets'])
    if hits == 1 or unique_id_num == 1:
        author_id = res['hits']['hits'][0]['_source']['id']
        result['id'] = author_id
    else:
        authors = []
        authors2 = []
        affiliations = []
        paper = ''

        for author in res['aggregations']['a']['buckets']:  # res['hits']['hits']:
            query_body = {"track_total_hits": "true", "sort": [{"citationcount": {"order": "desc"}}],
                          "query": {"match_phrase": {"authors.id": author['key']}}}  # author['_source']['id']}}}
            res_author = es.search(index=index, body=query_body)
            author_publications = res_author['hits']['total']['value']
            if author_publications > 0:
                paper = res_author['hits']['hits'][0]['_source']['papertitle']

            last_affiliation = get_last_affiliation(author['key'])

            if last_affiliation is not None and last_affiliation in affiliations:
                aut_ind = affiliations.index(last_affiliation)

                if author_publications > authors[aut_ind]['publications']:
                    authors[aut_ind] = {'name': result['item'], 'id': author['key'], 'affiliation': last_affiliation,
                                        'publications': author_publications, 'paper': paper}
            elif last_affiliation is not None:
                affiliations.append(last_affiliation)
                new_author = {'name': result['item'], 'id': author['key'], 'affiliation': last_affiliation,
                              'publications': author_publications, 'paper': paper}
                authors.append(new_author)
            else:
                authors2.append(
                    {'name': result['item'], 'id': author['key'], 'publications': author_publications, 'paper': paper})

        if len(authors) > 1:
            result['result'] = 'ka'
            result['item'] = sorted(authors, key=lambda k: k['publications'], reverse=True)[:9]
        elif len(authors) == 1:
            result['id'] = authors[0]['id']
        else:
            result['result'] = 'ka'
            result['item'] = authors2

    return json.dumps(result)

def check_topic_for_conference(data):
    acro = data.get("item").title()
    result = json.loads(find_match(acro))
    if result.get("result") == "ok" and result.get("obj_id") == 2:
        k2_ins = {"result": "k2", "num": [1, 1, 0, 0, 0], "keys": [[data.get("item")], [result.get("item")], [], [], []]}
        return json.dumps(k2_ins)
    return json.dumps(data)

# disambiguazione autori omonimi per ricerca dsc
def dsc_check_author(query):
    authors = []
    authors2 = []
    affiliations = []
    res = es.search(index=dsc_authors_index,
                    body={"size": 100, "track_total_hits": "true", "sort": [{"publications": {"order": "desc"}}],
                          "query": {"match_phrase": {"name": query}}})

    for author in res['hits']['hits']:
        author_publications = author['_source'].get('publications')
        if 'last_affiliation' in author['_source'] and 'affiliation_name' in author['_source']['last_affiliation']:
            affiliation = author['_source']['last_affiliation']
            if affiliation.get('affiliation_name') in affiliations:
                aut_ind = affiliations.index(affiliation.get('affiliation_name'))
                if author_publications > authors[aut_ind]['publications']:
                    authors[aut_ind] = {'name': author['_source']['name'], 'id': author['_source']['id'],
                                        'affiliation': affiliation.get('affiliation_name'),
                                        'publications': author_publications}
            else:
                affiliations.append(affiliation.get('affiliation_name'))
                authors.append({'name': author['_source']['name'], 'id': author['_source']['id'],
                                'affiliation': affiliation.get('affiliation_name'),
                                'publications': author_publications})
        else:
            authors2.append({'name': author['_source']['name'], 'id': author['_source']['id'],
                             'publications': author_publications})
    authors.extend(authors2)
    if len(authors) == 1:
        for author in res['hits']['hits']:
            if author['_source']['id'] == authors[0]['id']:
                return [author['_source']]
    return authors[:9]


# disambiguazione organizzazioni omonime per ricerca dsc
def dsc_check_org(query):
    organizations = []
    organizations2 = []
    countries = []
    res = es.search(index=dsc_organizations_index,
                    body={"size": 100, "track_total_hits": "true", "sort": [{"publications_5": {"order": "desc"}}],
                          "query": {"match_phrase": {"name": query}}})

    for organization in res['hits']['hits']:
        org_publications = organization['_source'].get('publications_5')
        if 'country' in organization['_source']:
            country = organization['_source']['country']
            if country in countries:
                org_ind = countries.index(country)
                if org_publications > organizations[org_ind]['publications']:
                    organizations[org_ind] = {'name': organization['_source']['name'],
                                              'id': organization['_source']['id'],
                                              'country': country,
                                              'publications': org_publications}
            else:
                countries.append(country)
                organizations.append({'name': organization['_source']['name'], 'id': organization['_source']['id'],
                                      'country': country,
                                      'publications': org_publications})
        else:
            organizations2.append({'name': organization['_source']['name'], 'id': organization['_source']['id'],
                                   'publications': org_publications})
    organizations.extend(organizations2)
    if len(organizations) == 1:
        for organization in res['hits']['hits']:
            if organization['_source']['id'] == organizations[0]['id']:
                return [organization['_source']]
    return organizations[:9]


def author_data(author_id):
    blacklist = ['lecture notes in computer science', 'arxiv software engineering']
    top_pub_conf = []
    top_cit_conf = []

    res1 = es.search(index=index,
                     body={"size": 0, "track_total_hits": "true", "query": {"match_phrase": {"authors.id": author_id}},
                           "aggs": {"a": {"cardinality": {"field": "authors.id"}}}})
    res2 = es.search(index=index,
                     body={"size": 0, "track_total_hits": "true", "query": {"match_phrase": {"authors.id": author_id}},
                           "aggs": {"a": {"terms": {"field": "confseries.keyword", "size": 3}}}})
    res3 = es.search(index=index,
                     body={"size": 0, "track_total_hits": "true", "query": {"match_phrase": {"authors.id": author_id}},
                           "aggs": {"a": {"terms": {"field": "confseries.keyword"},
                                          "aggs": {"the_sum": {"sum": {"field": "citationcount"}},
                                                   "citation_bucket_sort": {
                                                       "bucket_sort": {"sort": [{"the_sum": {"order": "desc"}}],
                                                                       "size": 3}}}}}})
    res4 = es.search(index=index,
                     body={"size": 0, "track_total_hits": "true", "query": {"match_phrase": {"authors.id": author_id}},
                           "aggs": {"a": {"terms": {"field": "journame.keyword", "size": 3 + len(blacklist)}}}})

    co_authors = res1['aggregations']['a']['value'] - 1
    top_journals = []
    for journal in res4['aggregations']['a']['buckets']:
        first = journal['key'].split(' ')[0]
        if journal['key'] not in blacklist and first != 'arxiv':
            top_journals.append({'name': journal['key'], 'publications': journal['doc_count']})

    if co_authors < 0:
        co_authors = 0
    top_pub = res2['aggregations']['a']['buckets']
    top_cit = res3['aggregations']['a']['buckets']
    for conf in top_pub:
        top_pub_conf.append({'name': conf['key'], 'publications': conf['doc_count']})
    for conf in top_cit:
        top_cit_conf.append({'name': conf['key'], 'citations': int(conf['the_sum']['value'])})
    return {'co_authors': co_authors, 'top_pub_conf': top_pub_conf, 'top_cit_conf': top_cit_conf,
            'top_journals': top_journals[:3]}


def dsc_finder(query):
    # da eliminare una volta corretti i dati su elasticsearch
    # if query == 'eswc' or query == 'Extended Semantic Web Conference':
    #     query = 'European Semantic Web Conference'

    if '-' in query:
        result1 = dsc_finder(query.replace('-', ''))
        result2 = dsc_finder(query.replace('-', ' '))
    else:
        result1 = None
        result2 = None

    dsc_indexes = [dsc_authors_index, dsc_conferences_index, dsc_conferences_index, dsc_organizations_index]
    dsc_exact_fields = ['name.keyword', 'acronym', 'name.keyword', 'name.keyword']
    dsc_fields = ['name', 'acronym', 'name', 'name']
    objects = ['authors', 'conferences', 'conferences', 'organizations']
    res = []
    num = []
    keys = [[], [], [], []]
    result = {'result': 'ko'}

    # ricerca esatta per gridid per organizzazione
    if 'grid.' in query and query.index('grid.') == 0:
        res = es.search(index=dsc_organizations_index,
                        body={"track_total_hits": "true", "query": {"match_phrase": {"gridid": query}}})
        if res['hits']['total']['value'] == 1:
            result = ({'result': 'ok', 'obj_id': 4, 'object': objects[3],
                       'item': res['hits']['hits'][0]['_source']})
        return json.dumps(result)

    # ricerca esatta per id per autore
    if query.isnumeric() and len(query) <= 10:
        res = es.search(index=dsc_authors_index,
                        body={"track_total_hits": "true", "query": {"match_phrase": {'id': query}}})
        if res['hits']['total']['value'] == 1:
            result = ({'result': 'ok', 'obj_id': 1, 'object': objects[0],
                       'item': {**res['hits']['hits'][0]['_source'],
                                **author_data(res['hits']['hits'][0]['_source']['id'])}})
        return json.dumps(result)

    # ricerca esatta per id per organizzazione
    if query.isnumeric() and len(query) > 10:
        query = int(query)
        res = es.search(index=dsc_organizations_index,
                        body={"track_total_hits": "true", "query": {"match_phrase": {'id': query}}})
        if res['hits']['total']['value'] == 1:
            result = ({'result': 'ok', 'obj_id': 4, 'object': objects[3],
                       'item': res['hits']['hits'][0]['_source']})
        return json.dumps(result)

    # ricerca esatta
    for i in range(len(dsc_indexes)):
        ins = query.lower() if i != 3 else query
        res.append(es.search(index=dsc_indexes[i], body={"track_total_hits": "true", "query": {
            "match_phrase": {dsc_exact_fields[i]: ins}}}))
        num.append(res[i]['hits']['total']['value'])

    obj_id = num.index(max(num))
    if sum(num) == 1:
        result = ({'result': 'ok', 'obj_id': obj_id + 1, 'object': objects[obj_id],
                   'item': res[obj_id]['hits']['hits'][0]['_source']})
        if obj_id == 0:
            result['item'] = {**result['item'], **author_data(res[obj_id]['hits']['hits'][0]['_source']['id'])}
        result = dsc_result_combinator(result, result1, result2)
        return result

    elif sum(num) > 1 and obj_id == 0:
        auth_list = dsc_check_author(query)
        if len(auth_list) > 1:
            result = {'result': 'ka', 'obj_id': obj_id + 1, 'object': objects[obj_id], 'item': auth_list}
        else:
            result = ({'result': 'ok', 'obj_id': obj_id + 1, 'object': objects[obj_id],
                       'item': {**auth_list[0], **author_data(auth_list[0]['id'])}})

        result = dsc_result_combinator(result, result1, result2)
        return result

    elif sum(num) > 1 and obj_id == 3:

        org_list = dsc_check_org(query)
        if len(org_list) > 1:
            result = {'result': 'ka', 'obj_id': obj_id + 1, 'object': objects[obj_id], 'item': org_list}
        else:
            result = {'result': 'ok', 'obj_id': obj_id + 1, 'object': objects[obj_id], 'item': org_list[0]}

        result = dsc_result_combinator(result, result1, result2)
        return result

    # ricerca per frase
    res = []
    num = []
    for i in range(len(dsc_indexes)):
        res.append(es.search(index=dsc_indexes[i],
                             body={"track_total_hits": "true", "query": {"match_phrase": {dsc_fields[i]: query}}}))
        num.append(res[i]['hits']['total']['value'])
    obj_id = num.index(max(num))

    if sum(num) == 1:
        result = ({'result': 'ok', 'obj_id': obj_id + 1, 'object': objects[obj_id],
                   'item': res[obj_id]['hits']['hits'][0]['_source']})
        if obj_id == 0:
            # noinspection PyTypeChecker
            result['item'] = {**result['item'], **author_data(res[obj_id]['hits']['hits'][0]['_source']['id'])}

        result = dsc_result_combinator(result, result1, result2)
        return result

    if 1 < max(num) <= 3:
        names = ['last affiliation', 'acronym', 'acronym', 'name']
        for i in range(len(dsc_indexes)):
            for data in res[i]['hits']['hits']:
                item = data['_source']
                key = {'id': item['id'], 'name': item['name']}
                if names[i] in item:
                    key[names[i]] = item[names[i]]
                keys[i].append(key)
        result = {'result': 'k2', 'num': num, 'keys': keys}

        result = dsc_result_combinator(result, result1, result2)
        return result

    if max(num) > 3 and obj_id == 3:
        org_list = dsc_check_org(query)
        if len(org_list) > 1:
            result = {'result': 'ka', 'obj_id': obj_id + 1, 'object': objects[obj_id], 'item': org_list}
        else:
            result = {'result': 'ok', 'obj_id': obj_id + 1, 'object': objects[obj_id], 'item': org_list[0]}

        result = dsc_result_combinator(result, result1, result2)
        return result

    if max(num) > 3:
        result = {'result': 'kk', 'num': num}

        result = dsc_result_combinator(result, result1, result2)
        return result

    # ricerca fuzzy
    es_index = [dsc_authors_index, dsc_conferences_index, dsc_conferences_index, dsc_organizations_index]
    dsc_fields = ['name', 'acronym', 'name', 'name']
    objects = ['authors', 'conferences', 'conferences', 'organizations']
    res = []
    num = []
    keys = [[], [], [], []]
    found = [[], [], [], []]
    result = {'result': 'ko'}
    source = ['name', 'acronym', 'name', 'name']

    # ricerca in es come per la ricerca esatta ma con il parametro fuzziness e con match al posto di match_phrase
    for i in range(4):
        res.append(es.search(index=es_index[i], body={"size": 10, "track_total_hits": "true", "_source": source[i],
                                                      "query": {"match": {
                                                          dsc_fields[i]: {"query": query, "fuzziness": "auto",
                                                                          "max_expansions": 50, "prefix_length": 0}}}}))

        # elimina i doppioni
        voice = []
        for data in res[i]['hits']['hits']:
            if data['_source'][dsc_fields[i]] not in voice:
                voice.append(data['_source'][dsc_fields[i]])
                found[i].append(data['_source'][dsc_fields[i]])

    # estrae i valori più probabili e calcola il punteggio: se ci sono valori sopra soglia
    # li salva nella lista delle chiavi più probabili
    for i in range(4):
        found[i] = process.extract(query, found[i], limit=3)
        for data in found[i]:
            if data[1] > threshold:
                keys[i].append(data[0])

    # prende solo i primi tre valori per ogni campo e verifica se siamo in presenza
    # di un unico valore candidato e in tal caso lo restituisce
    keys = [keys[0][:3], keys[1][:3], keys[2][:3], keys[3][:3]]
    num = [len(a) for a in keys]
    if sum(num) == 1:
        obj_id = num.index(1)
        item = keys[obj_id][0]
        i = obj_id
        ok_res = es.search(index=dsc_indexes[i],
                           body={"track_total_hits": "true", "query": {"match_phrase": {dsc_exact_fields[i]: item}}})

        result = (
            {'result': 'ok', 'obj_id': obj_id + 1, 'object': objects[obj_id],
             'item': ok_res['hits']['hits'][0]['_source']})
        if i == 0:
            result['item'] = {**result['item'], **author_data(ok_res['hits']['hits'][0]['_source']['id'])}

        result = dsc_result_combinator(result, result1, result2)
        return result

    for i in range(4):
        flat = []
        for data in found[i]:
            if data[1] > (threshold - 15):
                flat.append(data[0])
            found[i] = flat[:3]

    num = [len(a) for a in found]

    if sum(num) == 1:
        obj_id = num.index(1)
        item = found[obj_id][0]
        i = obj_id
        ok_res = es.search(index=dsc_indexes[i],
                           body={"track_total_hits": "true", "query": {"match_phrase": {dsc_exact_fields[i]: item}}})
        result = (
            {'result': 'ok', 'obj_id': obj_id + 1, 'object': objects[obj_id],
             'item': ok_res['hits']['hits'][0]['_source']})
        if i == 0:
            result['item'] = {**result['item'], **author_data(ok_res['hits']['hits'][0]['_source']['id'])}

        result = dsc_result_combinator(result, result1, result2)
        return result

    if 1 < sum(num) < 10:
        keys = [[], [], [], []]
        names = ['', 'acronym', 'acronym', '']
        for i in range(len(dsc_indexes)):
            for element in found[i]:
                duplicate_name_control = []
                ok_res = es.search(index=dsc_indexes[i], body={"track_total_hits": "true", "query": {
                    "match_phrase": {dsc_exact_fields[i]: element}}})
                for data in ok_res['hits']['hits']:

                    item = data['_source']
                    key = {'id': item['id'], 'name': item['name']}

                    if len(names[i]) > 0 and names[i] in item:
                        key[names[i]] = item[names[i]]
                    if item['name'] not in duplicate_name_control:
                        keys[i].append(key)
                        duplicate_name_control.append(item['name'])

        num = [len(a) for a in keys]
        result = {'result': 'k2', 'num': num, 'keys': keys}
        result = dsc_result_combinator(result, result1, result2)
        return result

    if sum(num) > 10:
        return json.dumps({'result': 'kk', 'num': num})

    # nessun risultato
    return json.dumps({'result': 'ko', 'object': '', 'obj_id': 0, 'keys': [], 'num': 0})


def dsc_result_combinator(res1, res2, res3):
    objects = ["topics", "conferences", "organizations", "authors"]
    if res2 is None or res3 is None or (res1['result'] == 'ok' and '-' in res1['item']['name']):
        return json.dumps(res1)
    results = [res1, json.loads(res2), json.loads(res3)]
    keys = [[], [], [], []]
    ids = [[], [], [], []]
    items = [[], [], [], []]
    num = [0, 0, 0, 0]
    for res in results:
        if res['result'] == 'ok':
            if res['item']['id'] not in ids[res['obj_id'] - 1]:
                ids[res['obj_id'] - 1].append(res['item']['id'])
                num[res['obj_id'] - 1] += 1
                items[res['obj_id'] - 1].append(res['item'])
                item = {'id': res['item']['id'], 'name': res['item']['name']}
                if 1 < res['obj_id'] < 4:
                    item['acronym'] = res['item']['acronym']
                keys[res['obj_id'] - 1].append(item)
        if res['result'] == 'k2':
            for i in range(len(res['num'])):
                for key in res['keys'][i]:
                    if key['id'] not in ids[i]:
                        ids[i].append(key['id'])
                        keys[i].append(key)
                        num[i] += 1

    if sum(num) == 1:
        obj_id = num.index(1)
        item = items[obj_id][0]
        return json.dumps({'result': 'ok', 'object': objects[obj_id], 'obj_id': obj_id + 1, 'item': item})

    if 1 < sum(num) < 11:
        return json.dumps({'result': 'k2', 'num': num, 'keys': keys})

    if sum(num) > 10:
        return json.dumps({'result': 'kk', 'num': num})

    # nessun risultato
    return json.dumps({'result': 'ko', 'object': '', 'obj_id': 0, 'keys': [], 'num': 0})


def check_topic(data, ins, old):
    if len(data['keys'])>5:
        data['keys'][1] += data['keys'][5]
        del data['keys'][5]
    if len(data['num']) > 5:
        data['num'][1] += data['num'][5]
        del data['num'][5]
    # TODO da disattivare per i journal omonimi di topics
    if old and data['num'][0] == 1 and data['keys'][0][0] == ins:
        return json.dumps({"result": "ok", "object": "topics", "obj_id": 1, "item": ins})
    for topic in data['keys'][0]:
        if topic in data['keys'][3]:
            data['keys'][3].remove(topic)
            data['num'][3] -= 1
    return json.dumps(data)


def how(sub, obj, ins, ins2, obj2, ins3, obj3):
    if ins == 'no':
        ins = ""
        obj = ''

    orig_filters = []
    sub_fields = ['authors.id', '', 'conferenceseriesid', 'authors.affiliation.keyword', 'citationcount',
                  'journame.keyword']
    obj_fields = ['cso_enhanced_topics.keyword', 'conferenceseriesid', 'authors.affiliation.keyword', 'authors.id',
                  'journame.keyword']
    if not isinstance(sub, str):
        sub = str(sub)
    if not isinstance(obj, str):
        obj = str(obj)
    if not isinstance(obj2, str):
        obj2 = str(obj2)
    if not isinstance(obj3, str):
        obj3 = str(obj3)
    if not isinstance(ins, str):
        ins = str(ins)
    if not isinstance(ins2, str):
        ins2 = str(ins2)
    if not isinstance(ins3, str):
        ins3 = str(ins3)

    if sub.isnumeric() and 0 < int(sub) < 7:
        sub_field = sub_fields[int(sub) - 1]
    else:
        sub_field = sub_fields[0]

    obj_field = 0
    obj_field2 = 0
    obj_field3 = 0
    all_c = False
    all_j = False
    if len(ins) > 0 and obj.isnumeric and 0 < int(obj) < 6:
        obj_field = obj_fields[int(obj) - 1]
        orig_filters.append([int(obj), ins, obj_field])
    if len(ins2) > 0 and obj2.isnumeric and 0 < int(obj2) < 6:
        obj_field2 = obj_fields[int(obj2) - 1]
        orig_filters.append([int(obj2), ins2, obj_field2])
    if len(ins3) > 0 and obj3.isnumeric and 0 < int(obj3) < 6:
        obj_field3 = obj_fields[int(obj3) - 1]
        orig_filters.append([int(obj3), ins3, obj_field3])

    result = json.dumps({'result': 'ko', 'msg': 'Query not yet implemented or query not applicable'})

    t = []
    c = []
    o = []
    a = []
    j = []
    filters = [t, c, o, a, j]
    nof = 0
    for f in orig_filters:
        if f[0] == 2 and f[1] == '*' and not all_c:
            all_c = True
            nof += 1
        elif f[0] == 5 and f[1] == '*' and not all_j:
            all_j = True
            nof += 1
        elif f[1] != '*' and f[0] - 1 >= 0:
            filters[f[0] - 1].append(f)
            nof += 1
        else:
            pass

    message = -1
    sit_a = (len(c) > 0 and len(j) > 0) or (all_c and all_j)
    sit_b = (len(c) > 0 and all_j) or (len(j) > 0 and all_c)
    sit_c = (sub == '3' and len(j) > 0) or (sub == '3' and all_c)
    sit_d = (sub == '3' and all_j) or (sub == '3' and len(c) > 0)
    sit_e = (sub == '6' and all_j) or (sub == '6' and len(c) > 0)
    sit_f = (sub == '6' and all_c) or (sub == '6' and len(j) > 0)
    sit_g = (sub == '1' and len(a) > 0)
    sit_h = (sub == '4' and len(o) > 0) or (sub == '5' and len(t) > 0)
    sit_i = (sub == '4' and len(a) > 1 and len(j) > 0 and len(t) > 0 and len(c) > 0 and len(o) > 0)

    if sit_a or sit_b or sit_c or sit_d or sit_e or sit_f or sit_g or sit_h or sit_i:
        return result

    if nof == 0 and sub != '5':
        query = {"match_all": {}}
        if sub == '1':
            message = get_card_aggs(author_index, "id", "", "", query)

        if sub == '2':
            message = get_hits(index, "", "", "", query)

        if sub == '3' or sub == '6':
            message = get_card_aggs(index, sub_field, "", "", query)

        if sub == '4':
            message = get_card_aggs(author_index, "affiliation.keyword", "", "", query)

    if nof == 1:
        if sub == '1' and len(a) == 0:
            if all_c or all_j:
                query = {"exists": {"field": obj_field}}
                message = get_card_aggs(index, sub_field, obj_field, ins, query)

            elif len(c) == 1 or len(t) == 1 or len(j) == 1:
                message = get_card_aggs(index, sub_field, obj_field, ins)
            elif len(o) == 1:
                message = get_card_aggs(author_index, "id", "affiliation.keyword", ins)

        if sub == '2':
            if all_c or all_j:
                query = {"exists": {"field": obj_field}}
            else:
                query = {"match_phrase": {obj_field: ins}}

            message = get_hits(index, sub_field, obj_field, ins, query)

        if (sub == '3' and len(c) == 0) or (sub == '6' and len(j) == 0):
            message = get_card_aggs(index, sub_field, obj_field, ins)

        if sub == '4' and len(o) == 0 and len(a) == 0:
            if all_c or all_j:
                query = {"exists": {"field": obj_field}}
                message = get_card_aggs(index, sub_field, obj_field, ins, query)

            else:
                message = get_card_aggs(index, sub_field, obj_field, ins)

        if sub == '4' and len(o) == 0 and len(a) == 1:
            message = get_card_aggs("authors_2020", "affiliation.keyword", "id", ins)

        if sub == '5':
            if all_c or all_j:
                query = {"exists": {"field": obj_field}}
                message = get_sum_aggs(index, sub_field, obj_field, ins, query)
            else:
                message = get_sum_aggs(index, sub_field, obj_field, ins)

    if nof == 2:
        query = {"bool": {"must": []}}
        if all_c:
            query["bool"]["must"].append({"exists": {"field": obj_fields[1]}})
        elif all_j:
            query["bool"]["must"].append({"exists": {"field": obj_fields[4]}})

        for i in filters:
            if len(i) > 0:
                for k in i:
                    query["bool"]["must"].append({"match_phrase": {k[2]: k[1]}})

        # cases sub == authors
        if sub == '1' and len(a) == 0:
            if len(o) == 0 and len(c) < 2 and len(j) < 2:
                message = get_card_aggs(index, sub_field, obj_field, ins, query)

            if len(c) == 2:
                res1 = get_term_aggs(index, sub_field, c[0][2], c[0][1])
                res2 = get_term_aggs(index, sub_field, c[1][2], c[1][1])
                message = str(len(list(set(res1) & set(res2))))

            if len(j) == 2:
                res1 = get_term_aggs(index, sub_field, j[0][2], j[0][1])
                res2 = get_term_aggs(index, sub_field, j[1][2], j[1][1])
                message = str(len(list(set(res1) & set(res2))))

            if len(o) == 1:
                res1 = get_term_aggs(author_index, "id", "affiliation.keyword", o[0][1])
                res2 = get_term_aggs(index, sub_field, "", "", query)
                message = str(len(list(set(res1) & set(res2))))

        if sub == '2' and len(c) < 2 and len(j) < 2:
            message = get_hits(index, sub_field, obj_field, ins, query)

        if (sub == '3' and len(c) == 0) or (sub == '6' and len(j) == 0):
            query["bool"]["must"].append({"exists": {"field": sub_field}})
            message = get_card_aggs(index, sub_field, obj_field, ins, query)

        if sub == '4' and len(o) == 0 and len(c) < 2 and len(j) < 2 and len(a) == 0:
            message = get_card_aggs(index, sub_field, obj_field, ins, query)

        if sub == '5' and len(c) < 2 and len(j) < 2:
            message = get_sum_aggs(index, sub_field, obj_field, ins, query)

    if nof == 3:
        query = {"bool": {"must": []}}
        if all_c:
            query["bool"]["must"].append({"exists": {"field": obj_fields[1]}})
        elif all_j:
            query["bool"]["must"].append({"exists": {"field": obj_fields[4]}})

        for i in filters:
            if len(i) > 0:
                for k in i:
                    query["bool"]["must"].append({"match_phrase": {k[2]: k[1]}})

        if sub == '1' and len(a) == 0 and len(o) < 2:
            if len(o) == 1 and len(c) == 0 and len(j) == 0:
                res1 = get_term_aggs(index, sub_field, "", "", query)
                res2 = get_term_aggs(author_index, "id", "affiliation.keyword", o[0][1])
                message = len(list(set(res1) & set(res2)))

            if len(t) == 3 or ((len(t) == 2 or len(t) == 1) and len(c) == 0 and len(j) == 0 and len(o) == 0):
                message = str(len(get_term_aggs(index, sub_field, "", "", query)))

            if len(c) == 1 and len(o) < 2 and len(j) == 0:
                if len(t) == 1:
                    q1 = {
                        "bool": {"must": [{"match_phrase": {t[0][2]: t[0][1]}}, {"match_phrase": {c[0][2]: c[0][1]}}]}}
                    res1 = get_term_aggs(index, sub_field, "", "", q1)
                    res2 = get_term_aggs(author_index, "id", "affiliation.keyword", o[0][1])
                    message = len(list(set(res1) & set(res2)))
                else:
                    message = get_card_aggs(index, sub_field, "", "", query)

            if len(c) == 2:
                if len(t) == 1:
                    q1 = {
                        "bool": {"must": [{"match_phrase": {t[0][2]: t[0][1]}}, {"match_phrase": {c[0][2]: c[0][1]}}]}}
                    q2 = {
                        "bool": {"must": [{"match_phrase": {t[0][2]: t[0][1]}}, {"match_phrase": {c[1][2]: c[1][1]}}]}}
                    res1 = get_term_aggs(index, sub_field, "", "", q1)
                    res2 = get_term_aggs(index, sub_field, "", "", q2)
                    message = str(len(list(set(res1) & set(res2))))

                else:
                    res1 = get_term_aggs(index, sub_field, c[0][2], c[0][1])
                    res2 = get_term_aggs(index, sub_field, c[1][2], c[1][1])
                    res3 = get_term_aggs(author_index, "id", "affiliation.keyword", o[0][1])
                    message = str(len(list(set(res1) & set(res2) & set(res3))))

            if len(c) == 3:
                res1 = get_term_aggs(index, sub_field, c[0][2], c[0][1])
                res2 = get_term_aggs(index, sub_field, c[1][2], c[1][1])
                res3 = get_term_aggs(index, sub_field, c[2][2], c[2][1])
                message = str(len(list(set(res1) & set(res2) & set(res3))))

            if len(j) == 1:
                if len(o) == 1:
                    res1 = get_term_aggs(index, sub_field, "", "", query)
                    res2 = get_term_aggs(author_index, "id", "affiliation.keyword", o[0][1])
                    message = str(len(list(set(res1) & set(res2))))
                else:
                    message = get_card_aggs(index, sub_field, "", "", query)

            if len(j) == 2:
                if len(t) == 1:
                    q1 = {
                        "bool": {"must": [{"match_phrase": {t[0][2]: t[0][1]}}, {"match_phrase": {j[0][2]: j[0][1]}}]}}
                    q2 = {
                        "bool": {"must": [{"match_phrase": {t[0][2]: t[0][1]}}, {"match_phrase": {j[1][2]: j[1][1]}}]}}
                    res1 = get_term_aggs(index, sub_field, "", "", q1)
                    res2 = get_term_aggs(index, sub_field, "", "", q2)
                    message = str(len(list(set(res1) & set(res2))))

                else:
                    res1 = get_term_aggs(index, sub_field, j[0][2], j[0][1])
                    res2 = get_term_aggs(index, sub_field, j[1][2], j[1][1])
                    res3 = get_term_aggs(author_index, "id", "affiliation.keyword", o[0][1])
                    message = str(len(list(set(res1) & set(res2) & set(res3))))

            if len(j) == 3:
                res1 = get_term_aggs(index, sub_field, j[0][2], j[0][1])
                res2 = get_term_aggs(index, sub_field, j[1][2], j[1][1])
                res3 = get_term_aggs(index, sub_field, j[2][2], j[2][1])
                message = str(len(list(set(res1) & set(res2) & set(res3))))

            if len(o) == 0 and len(c) < 2:
                message = get_card_aggs(index, sub_field, "", "", query)

        if sub == '2' and len(c) < 2 and len(j) < 2:
            message = get_hits(index, sub_field, obj_field, ins, query)

        if ((sub == '3' and len(c) == 0) or (sub == '6' and len(j) == 0)) and not (all_j or all_c):
            query["bool"]["must"].append({"exists": {"field": sub_field}})
            message = get_card_aggs(index, sub_field, "", "", query)

        if sub == '4' and len(o) == 0 and len(c) < 2 and len(j) < 2 and len(a) == 0:
            message = get_card_aggs(index, sub_field, "", "", query)

        if sub == '5' and len(c) < 2 and len(j) < 2:
            message = get_sum_aggs(index, sub_field, "", "", query)

    if message != -1:
        result = json.dumps({'hits': message, 'result': 'ok'})
    return result


def get_term_aggs(local_index, sub_field, obj_field, ins, query=None):
    if query is None:
        query = {"match_phrase": {obj_field: ins}}
    res = es.search(index=local_index,
                    body={"track_total_hits": 'true', "size": 0,
                          "query": query,
                          "aggs": {"A": {"terms": {"field": sub_field, "size": 50000}}}})
    return [x['key'] for x in res['aggregations']['A']['buckets']]


def get_sum_aggs(local_index, sub_field, obj_field, ins, query=None):
    if query is None:
        query = {"match_phrase": {obj_field: ins}}
    res = es.search(index=local_index,
                    body={"track_total_hits": 'true', "size": 0,
                          "query": query,
                          "aggs": {"Sum": {"sum": {"field": sub_field}}}})
    return str(int(res['aggregations']['Sum']['value']))


def get_card_aggs(local_index, sub_field, obj_field, ins, query=None):
    if query is None:
        query = {"match_phrase": {obj_field: ins}}
    res = es.search(index=local_index,
                    body={"track_total_hits": 'true', "size": 0,
                          "query": query,
                          "aggs": {"A": {"cardinality": {"field": sub_field}}}})
    return str(res['aggregations']['A']['value'])


def get_hits(local_index, sub_field, obj_field, ins, query=None):
    if query is None:
        query = {"match_phrase": {obj_field: ins}}
    res = es.search(index=local_index, body={"track_total_hits": 'true', "size": 0, "query": query})
    return str(res['hits']['total']['value'])


def lst(sub, obj, ins, ins2, obj2, ins3, obj3, num='5', order='1'):

    if ins == 'no' or ins == 'all':
        ins = ""
        obj = ''

    if sub is None or sub == "" or sub == "None":
        return json.dumps({'result': 'ko', 'msg': 'Query not yet implemented or query not applicable'})

    if not isinstance(sub, str):
        sub = str(sub)
    if not isinstance(obj, str):
        obj = str(obj)
    if not isinstance(obj2, str):
        obj2 = str(obj2)
    if not isinstance(obj3, str):
        obj3 = str(obj3)
    if not isinstance(order, str):
        order = str(order)
    if not isinstance(num, str):
        num = str(num)
    if not isinstance(ins, str):
        ins = str(ins)
    if not isinstance(ins2, str):
        ins2 = str(ins2)
    if not isinstance(ins3, str):
        ins3 = str(ins3)

    orig_filters = []
    if num.isnumeric():
        num = int(num)
    else:
        num = 5

    if order.isnumeric():
        order = int(order)
    else:
        order = 1

    result_lst = []
    orig_filters = []
    sub_fields = ['authors.id', '', 'conferenceseriesid', 'authors.affiliation.keyword', 'cso_enhanced_topics.keyword',
                  'journame.keyword']
    obj_fields = ['cso_enhanced_topics.keyword', 'conferenceseriesid', 'authors.affiliation.keyword', 'authors.id',
                  'journame.keyword']
    order_fields = ['citationcount', 'citationcount_5']

    if sub.isnumeric() and 0 < int(sub) < 7:
        sub_field = sub_fields[int(sub) - 1]
    else:
        sub_field = sub_fields[0]

    obj_field = 0
    obj_field2 = 0
    obj_field3 = 0
    all_c = False
    all_j = False
    if len(ins) > 0 and obj.isnumeric and 0 < int(obj) < 6:
        obj_field = obj_fields[int(obj) - 1]
        orig_filters.append([int(obj), ins, obj_field])
    if len(ins2) > 0 and obj2.isnumeric and 0 < int(obj2) < 6:
        obj_field2 = obj_fields[int(obj2) - 1]
        orig_filters.append([int(obj2), ins2, obj_field2])
    if len(ins3) > 0 and obj3.isnumeric and 0 < int(obj3) < 6:
        obj_field3 = obj_fields[int(obj3) - 1]
        orig_filters.append([int(obj3), ins3, obj_field3])

    result = {'result': 'ko', 'msg': 'Query not yet implemented or query not applicable'}

    t = []
    c = []
    o = []
    a = []
    j = []
    filters = [t, c, o, a, j]
    nof = 0
    for f in orig_filters:
        if f[0] == 2 and f[1] == '*' and not all_c:
            all_c = True
            nof += 1
        elif f[0] == 5 and f[1] == '*' and not all_j:
            all_j = True
            nof += 1
        elif f[1] != '*' and f[0] - 1 >= 0:
            filters[f[0] - 1].append(f)
            nof += 1
        else:
            pass

    message = -1
    sit_a = (len(c) > 0 and len(j) > 0) or (all_c and all_j)
    sit_b = (len(c) > 0 and all_j) or (len(j) > 0 and all_c)
    sit_c = (sub == '3' and len(j) > 0) or (sub == '3' and all_c)
    sit_d = (sub == '3' and all_j) or (sub == '3' and len(c) > 0)
    sit_e = (sub == '6' and all_j) or (sub == '6' and len(c) > 0)
    sit_f = (sub == '6' and all_c) or (sub == '6' and len(j) > 0)
    sit_g = (sub == '1' and len(a) > 0) or (sub == '2' and (order == 1 or order == 3))
    sit_h = (sub == '4' and len(o) > 0) or (sub == '5' and len(t) > 0)
    sit_i = (sub == '4' and len(a) > 0)

    if sit_a or sit_b or sit_c or sit_d or sit_e or sit_f or sit_g or sit_h or sit_i:
        return json.dumps(result)

    if nof == 0:
        if sub != '2' and order == 1:
            result = get_lst_agg_pub(sub_field, num)

        elif sub != '2' and order == 3:
            result = get_lst_agg_pub5(sub_field, num)

        elif sub != '2' and (order == 2 or order == 4):
            result = get_lst_agg_cit(sub_field, num, order, order_fields)

        elif sub == '2' and (order == 2 or order == 4):
            result = get_lst_papers(num, order, order_fields)

    elif nof == 1:

        if all_c:
            query = [{"exists": {"field": obj_fields[1]}}]
        elif all_j:
            query = [{"exists": {"field": obj_fields[4]}}]
        else:
            query = []

        if sub != '2' and order == 1 and not (sub == '1' and len(o) > 0):
            if not all_j and not all_c:
                query.append({"match_phrase": {obj_field: ins}})
            result = get_lst_agg_pub(sub_field, num, query)

        elif sub != '2' and (order == 2 or order == 4) and not (sub == '1' and len(o) > 0):
            if not all_j and not all_c:
                query = {"match_phrase": {obj_field: ins}}
            result = get_lst_agg_cit(sub_field, num, order, order_fields, query)

        elif sub != '2' and order == 3 and not (sub == '1' and len(o) > 0):
            if not all_j and not all_c:
                query = {"match_phrase": {obj_field: ins}}
            result = get_lst_agg_pub5(sub_field, num, query)

        elif sub == '2' and (order == 2 or order == 4):
            if not all_j and not all_c:
                query = {"match_phrase": {obj_field: ins}}
            result = get_lst_papers(num, order, order_fields, query)

        elif sub == '1' and len(o) == 1 and order == 1:
            if not all_j and not all_c:
                query = None
            result = get_lst_authors_pub(sub_field, num, affiliation=ins, query=query)
            return json.dumps(result)

        elif sub == '1' and len(o) == 1 and (order == 2 or order == 4):
            if not all_j and not all_c:
                query = None
            result = get_lst_authors_cit(sub_field, num, affiliation=ins, query=query)
            return json.dumps(result)

        elif sub == '1' and len(o) == 1 and order == '3':
            if not all_j and not all_c:
                query = None
            result = get_lst_authors_pub5(sub_field, num, affiliation=ins, query=query)
            return json.dumps(result)

    else:
        query = {"bool": {"must": []}}
        if all_c:
            query["bool"]["must"].append({"exists": {"field": obj_fields[1]}})
        elif all_j:
            query["bool"]["must"].append({"exists": {"field": obj_fields[4]}})

        for i in filters:
            if len(i) > 0:
                for k in i:
                    query["bool"]["must"].append({"match_phrase": {k[2]: k[1]}})

        if sub != '2' and order == 1 and not (sub == '1' and len(o) > 0):
            result = get_lst_agg_pub(sub_field, num, query)

        elif sub != '2' and (order == 2 or order == 4) and not (sub == '1' and len(o) > 0):
            result = get_lst_agg_cit(sub_field, num, order, order_fields, query)

        elif sub != '2' and order == 3 and not (sub == '1' and len(o) > 0):
            result = get_lst_agg_pub5(sub_field, num, query)

        elif sub == '2' and (order == 2 or order == 4):
            if len(o) == 1 and len(a) == 1:
                # TODO Filter for author from organization
                pass
            result = get_lst_papers(num, order, order_fields, query)

        elif sub == '1' and len(o) == 1 and order == 1:
            result = get_lst_authors_pub(sub_field, num, affiliation=o[0][1], query=query)
            return json.dumps(result)

        elif sub == '1' and len(o) == 1 and (order == 2 or order == 4):
            result = get_lst_authors_cit(sub_field, num, order=order, affiliation=o[0][1], query=query)
            return json.dumps(result)

        elif sub == '1' and len(o) == 1 and order == '3':
            result = get_lst_authors_pub5(sub_field, num, affiliation=o[0][1], query=query)
            return json.dumps(result)

    # sostituisce il nome all'id nella lista dei risultati
    if (sub == '1' or sub == '3') and result.get("lst") is not None:
        result['lst'] = get_names_for_list(sub, result['lst'])

    return json.dumps(result)


def get_lst_authors_pub5(sub_field, num=None, affiliation=None, query=None):
    result_lst = []
    obj_field = "authors.affiliation.keyword"
    year = datetime.datetime.now().year
    body = {"size": 0, "track_total_hits": 'true', "query": {
        "bool": {"must": []}}, "aggs": {"names": {"terms": {"field": sub_field, "size": num * 5}}}}

    body["query"]["bool"]["must"].append({"range": {"year": {"gte": year - 5, "lte": year}}})

    if query is not None:
        if isinstance(query, list):
            body["query"]["bool"]["must"] += query
        else:
            body["query"]["bool"]["must"].append(query)

    if affiliation is not None:
        body["query"]["bool"]["must"].append({"match_phrase": {obj_field: affiliation}})

    res = es.search(index=index, body=body)
    a = res['aggregations']['names']['buckets']
    for i in a:
        res1 = es.search(index=author_index, body={"size": 1, "track_total_hits": 'true', "query": {
            "bool": {"must": [{"match_phrase": {"id": i["key"]}}, {"match_phrase": {"affiliation": affiliation}}]}}})
        if res1['hits']['total']['value'] > 0:
            author_name = res1['hits']['hits'][0]['_source'].get('name')
            result_lst.append({"name": author_name, "papers": i["doc_count"]})
        if len(result_lst) >= num:
            break
    return {"lst": result_lst, 'result': 'ok'}


def get_lst_authors_cit(sub_field, num=None, order=2, affiliation=None, query=None):
    order_fields = ['citationcount', 'citationcount_5']
    obj_field = "authors.affiliation.keyword"
    result_lst = []
    if query is None:
        if affiliation is not None:
            q = {"match_phrase": {obj_field: affiliation}}
        else:
            q = {"match_all": {}}
    else:
        q = {"bool": {"must": []}}
        if isinstance(query, list):
            q["bool"]["must"] += query
        else:
            q["bool"]["must"].append(query)

    n = int(int(order) / 2 - 1)
    res = es.search(index=index,
                    body={"size": 0, "track_total_hits": 'true', "query": q, "aggs": {
                        "names": {"terms": {"field": sub_field, "size": 50000},
                                  "aggs": {"the_sum": {"sum": {"field": order_fields[n]}}, "citation_bucket_sort": {
                                      "bucket_sort": {"sort": [{"the_sum": {"order": "desc"}}], "size": num * 5}}}}}})
    a = res['aggregations']['names']['buckets']

    for i in a:
        res1 = es.search(index=author_index, body={"size": 1, "track_total_hits": 'true', "query": {
            "bool": {"must": [{"match_phrase": {"id": i["key"]}}, {"match_phrase": {"affiliation": affiliation}}]}}})

        if res1['hits']['total']['value'] > 0:
            author_name = res1['hits']['hits'][0]['_source'].get('name')
            result_lst.append(
                {"name": author_name, "papers": i["doc_count"], "citations": i["the_sum"]["value"]})
        if len(result_lst) >= num:
            break
    return {"lst": result_lst, 'result': 'ok'}


def get_lst_authors_pub(sub_field, num=None, affiliation=None, query=None):
    obj_field = "authors.affiliation.keyword"
    result_lst = []
    if query is None:
        if obj_field is not None and affiliation is not None:
            q = {"match_phrase": {obj_field: affiliation}}
        else:
            q = {"match_all": {}}
    else:
        q = {"bool": {"must": []}}
        if isinstance(query, list):
            q["bool"]["must"] += query
        else:
            q["bool"]["must"].append(query)

    res = es.search(index=index, body={"size": 0, "track_total_hits": 'true', "query": q,
                                       "aggs": {"names": {"terms": {"field": sub_field, "size": num * 5}}}})

    a = res['aggregations']['names']['buckets']

    for i in a:
        res1 = es.search(index=author_index, body={"size": 1, "track_total_hits": 'true', "query": {
            "bool": {"must": [{"match_phrase": {"id": i["key"]}}, {"match_phrase": {"affiliation": affiliation}}]}}})

        if res1['hits']['total']['value'] > 0:
            author_name = res1['hits']['hits'][0]['_source'].get('name')
            result_lst.append({"name": author_name, "papers": i["doc_count"]})
        if len(result_lst) >= num:
            break

    return {"lst": result_lst, 'result': 'ok'}


def get_lst_papers(num, order, order_fields, query=None):
    result_lst = []
    if query is None:
        q = {"match_all": {}}
    else:
        q = {"bool": {"must": []}}
        if isinstance(query, list):
            q["bool"]["must"] += query
        else:
            q["bool"]["must"].append(query)

    n = int(int(order) / 2 - 1)
    res = es.search(index=index, body={"track_total_hits": 'true', "size": num,
                                       "_source": ["papertitle", order_fields[n], "authors", "urls", "doi"],
                                       "query": q,
                                       "sort": [{order_fields[n]: {"order": "desc"}}]})
    a = res['hits']['hits']
    for i in a:
        author = authors_name(i['_source']['authors'])
        urls = i['_source'].get("urls")
        doi = i['_source'].get("doi")
        result_lst.append(
            {"name": i['_source']["papertitle"], "citations": i['_source'][order_fields[n]], "author": author, "urls": urls, "doi": doi})
    return {"lst": result_lst, 'result': 'ok'}


def get_lst_agg_pub5(sub_field, num, query=None):
    result_lst = []
    year = datetime.datetime.now().year
    body = {"size": 0, "track_total_hits": 'true',
            "query": {"bool": {"must": [{"range": {"year": {"gte": year - 5, "lte": year}}}]}},
            "aggs": {"names": {"terms": {"field": sub_field, "size": num}}}}
    if query is not None:
        if isinstance(query, list):
            body["query"]["bool"]["must"] += query
        else:
            body["query"]["bool"]["must"].append(query)

    res = es.search(index=index, body=body)
    a = res['aggregations']['names']['buckets']
    for i in a:
        result_lst.append({"name": i["key"], "papers": i["doc_count"]})

    return {"lst": result_lst, 'result': 'ok'}


def get_lst_agg_pub(sub_field, num, query=None):
    result_lst = []
    if query is None:
        q = {"match_all": {}}
    else:
        q = {"bool": {"must": []}}
        if isinstance(query, list):
            q["bool"]["must"] += query
        else:
            q["bool"]["must"].append(query)

    res = es.search(index=index, body={"size": 0, "track_total_hits": 'true', "query": q,
                                       "aggs": {"names": {"terms": {"field": sub_field, "size": num}}}})
    a = res['aggregations']['names']['buckets']
    for i in a:
        result_lst.append({"name": i["key"], "papers": i["doc_count"]})
    return {"lst": result_lst, 'result': 'ok'}


def get_lst_agg_cit(sub_field, num, order, order_fields, query=None):
    result_lst = []
    if query is None:
        q = {"match_all": {}}
    else:
        q = {"bool": {"must": []}}
        if isinstance(query, list):
            q["bool"]["must"] += query
        else:
            q["bool"]["must"].append(query)

    n = int(int(order) / 2 - 1)

    body = {"size": 0, "track_total_hits": 'true', "query": q,
            "aggs": {"names": {"terms": {"field": sub_field, "size": 50000},
                               "aggs": {"the_sum": {"sum": {"field": order_fields[n]}},
                                        "citation_bucket_sort": {"bucket_sort": {
                                            "sort": [{"the_sum": {"order": "desc"}}],
                                            "size": num}}}}}}

    res = es.search(index=index, body=body)
    a = res['aggregations']['names']['buckets']
    for i in a:
        result_lst.append({"name": i["key"], "papers": i["doc_count"], "citations": i["the_sum"]["value"]})
    return {"lst": result_lst, 'result': 'ok'}


def get_names_for_list(sub, result_lst):
    item_lst = []
    for item in result_lst:
        if type(item['name']) is int:
            if sub == '1':
                res1 = es.search(index=author_index, body={"size": 1, "track_total_hits": 'true',
                                                           "query": {"match_phrase": {'id': item['name']}}})
                author_name = res1['hits']['hits'][0]['_source'].get('name')
                item['name'] = author_name
                item_lst.append(item)
            if sub == '3':
                res1 = es.search(index=dsc_conferences_index, body={"size": 1, "track_total_hits": 'true',
                                                                    "query": {
                                                                        "match_phrase": {'id': item['name']}}})
                conf_name = res1['hits']['hits'][0]['_source'].get('name') + ' (' + res1['hits']['hits'][0][
                    '_source'].get('acronym') + ')'
                item['name'] = conf_name
                item_lst.append(item)
    return item_lst


def find(ins, num=5, mode=True):
    way = -1
    if not isinstance(num, int):
        num = 5
    result = {"result": "ko"}
    res = []

    query_body = {"sort": [{"citationcount": {"order": "desc"}}],
                  "size": 2*num, "track_total_hits": "true",
                  "_source": ["papertitle", "abstract", "citationcount", "urls", "doi", "authors", "year"],
                  "query": {"match_phrase": {"papertitle": ins}}}
    res0 = (es.search(index="aida_2020", body=query_body)).get("hits").get("hits")

    if len(res0) > 0:
        for paper in res0:
            if paper.get("_source").get("abstract") is not None:
                res.append(paper)
        res = res[:num]
        way = 0

    if len(res0) == 0 or len(res) == 0:
        query_body = {"sort": [{"citationcount": {"order": "desc"}}],
                      "size": num, "track_total_hits": "true",
                      "_source": ["papertitle", "abstract", "citationcount", "urls", "doi", "authors", "year"],
                      "query": {"match_phrase": {"abstract": ins}}}
        res1 = (es.search(index="aida_2020", body=query_body)).get("hits").get("hits")

        if len(res1) > 0:
            res = res1[:num]
            way = 2
        elif mode:
            query_body = {"size": 2*num, # "track_total_hits": "true",
                          "_source": ["papertitle", "abstract", "citationcount", "urls", "doi", "authors", "year"],
                          "query": {"match": {"abstract": {"query": ins, "fuzziness": "auto", "max_expansions": 25,
                                                           "prefix_length": 0}}}}
            res2 = (es.search(index="aida_2020", body=query_body)).get("hits").get("hits")

            if len(res2) > 0:
                res = res2[:num]
                way = 3

    if len(res) > 0:
        result["result"] = "ok"
        result["papers"] = []
        for paper in res:
            p = {"title": paper.get("_source").get("papertitle"),
                 "abstract": paper.get("_source").get("abstract"),
                 "authors": paper.get("_source").get("authors"),
                 "citations": paper.get("_source").get("citationcount"),
                 "urls": paper.get("_source").get("urls"),
                 "doi": paper.get("_source").get("doi"),
                 "year": paper.get("_source").get("year"),
                 "mode": way
                 }
            result["papers"].append(p)

    return json.dumps(result)
