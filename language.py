import json
import random
import config


def load_json(file__name):
    data_file = open(file__name, "r", encoding='utf-8')
    file = json.loads(data_file.read())
    data_file.close()
    return file


names = load_json(config.names_file)

data_server_url = 'http://localhost/api?pass=123abc&'

dictionary = {
    'type': ['list', 'count', 'describe', 'compare'],
    'sub': {
        'list': ['author', 'paper', 'conference', 'organization', 'topic', 'journal'],
        'count': ['author', 'paper', 'conference', 'organization', 'citation', 'journal']
    },
    "objects": ["topics", "conferences", "organizations", "authors", "journals"],

    'order': {
        'verbs': ['sorted'],
        'ways': ['publications', 'citations', 'publications in the last 5 years',
                 'citations in the last 5 years']
    }
}

syns = {
    "authors": ["researchers"],
    "organizations": ["universities", "companies", "organisation"],
    "topics": ["fields of research", "research fields"],
    "papers": ["publications"],
    "ontology": ["knowledge graph", "knowledge graphs"]
}

key_words = ['who', 'what', 'list', 'count', 'describe', 'compare', 'author', 'paper', 'conference', 'organization',
             'topic', 'citation', 'publication', 'journal', 'authors', 'papers', 'conferences', 'organizations',
             'topics', 'citations', 'publications', 'article', 'articles', 'journals', 'top', 'best', 'main', 'please',
             'tell', 'years', 'year', 'important', 'name', 'names', "how", "many", "which", "help", "most", "cited", "tools"]

entity_keywords = ['who', 'what', 'list', 'count', 'describe', 'compare', 'author', 'paper', 'topic', 'citation',
                   'publication', 'authors', 'papers', 'conferences', 'organizations', 'topics', 'citations',
                   'publications', 'article', 'articles', 'journals', 'top', 'best', 'main', 'please', 'tell',
                   'years', 'year', 'important', 'name', 'names', "how", "many", "which", "help", "most", "cited"]

residual_key_words = ["the", "me", "a", "an", "this", "those", "that", "these", "i", "you", "differences", "to", "one",
                      "from", "born", "sort", "sorted", "all", "terms", "number", "numbers", "search", "for", "tools",
                      "the university", "the highest number"]

words_to_verify = ['top', 'best', 'first', 'main', 'list', 'better']

list_of_numbers = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
    'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
    'zero', '1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th',
    'zero', 'first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth'
]

aidabot_grammar = '''
    #JSGF V1.0; 
    grammar utterances;
    public <num> = 2 ;
    public <num2> = ;
    public <verb> = are | is ;
    public <w> = what | which ;
    public <preps> = sorted by | by | by number of | with more ;
    public <orders> = publications | citations ;
    public <super> = top | most important | main | most cited ; 
    public <sub_c> = papers | authors | conferences | organizations | citations | journals ;
    public <sub_d> = author | conference | organization ;
    public <sub_l> = <num> papers | <num> authors | <num> conferences | <num> organizations | <num> topics | <num> journals ;
    public <sub_l2> = papers | authors | conferences | organizations | topics | journals ;
    public <order> = <orders> | <orders> in the last 5 years ;
    public <utterance> = 
          01 count <sub_c> {} 
        | 01 how many <sub_c> {} 
        | 02 list the <super> <sub_l> {} <preps> <order> 
        | 02 list the <super> <sub_l> {} 
        | 02 which are the <super> <sub_l> {} <preps> <order> 
        | 02 which are the <super> <sub_l> {} 
        | 02 list <sub_l> {} <preps> <order> 
        | 02 list <sub_l> {} 
        | 02 <w> <verb> <sub_l2> {}’s 
        | 03 describe {} 
        | 03 who is {} 
        | 03 what is the <sub_d> {} about 
        | 04 compare {} vs {} 
        | 04 which are the differences between {} and {} 
        | 04 which are the differences among the {} and the {} 
    '''

aidabot_grammar_add='''
        | 05 search for {} 
        | 05 search papers for {} 
        | 05 search in papers for {} 
        | 05 search papers about {} 
        | 05 search papers containing {} 
        | 06 what is {} 
    '''

personal_questions = {
    '00 What is your name?': [
        "My name is Aida-Bot.",
        "I'm Aida-Bot, your personal assistant."],
    '00 How do you feel?': [
        "I feel good, thanks.",
        "Very well, thanks."],
    '00 Do you speak English?': [
        "Yes, I do",
        "I currently only speak English."],
    '00 What can I ask you?': [
        "You can ask me to count, list, describe or compare authors, conferences or organizations.",
        "You can ask me to compare two authors, two conferences or two organizations",
        "You can ask me to count papers, citations, authors, conferences, organizations or journals.",
        "You can ask me to list papers, authors, topics, organizations, conferences or journals."],
    '00 can you help me please?': [
        "You can ask me to count, list, describe or compare authors, conferences or organizations.",
        "You can ask me to compare two authors, two conferences or two organizations",
        "You can ask me to count papers, citations, authors, conferences or organizations.",
        "You can ask me to list papers, authors, topics, organizations or conferences."],
    '00 Help': [
        "You can ask me to count, list, describe or compare authors, conferences or organizations.",
        "You can ask me to compare two authors, two conferences or two organizations",
        "You can ask me to count papers, citations, authors, conferences, organizations or journals.",
        "You can ask me to list papers, authors, topics, organizations, conferences or journals."],
    '00 What can I do?': [
        "You can ask me to count, list, describe or compare authors, conferences or organizations.",
        "You can ask me to compare two authors, two conferences or two organizations",
        "You can ask me to count papers, citations, authors, conferences or organizations.",
        "You can ask me to list papers, authors, topics, organizations or conferences."],
    '00 What are the sorting options': [
        "The methods of ordering the lists are: publications and citations, global or in the last 5 years ",
        "Sorting options: publications and citations, total or from the last 5 years"],
    '00 Who are you?': [
        "I am your personal assistant for scholarly data.",
        "I am Aida-bot."],
    '00 Who am I?': [
        "You are one of my favorite users.",
        "You are one of my beloved users!"],
    '00 What do you think of me?': [
        "You are one of my favorite users.",
        "You are one of my beloved users!"],
    '00 What is you duty?': [
        "I'm here to answer your questions",
        "Say the word, I'm ready!"],
    '00 I love you': [
        "I love you too, but maybe it's better if we're just friends!",
        "I am very honored, but it is better if we are just friends!"],
    '00 I like you': [
        "I like you too, I'm happy if we're friends!",
        "Thank you, I am very honored"],
    '00 love you': "Love you too",
    '00 Hello': [
        "Hi, what can I do for you?",
        "Welcome, I'm AidaBot, what can I do for you?"],
    '00 Hello aidabot': [
        "Hi, what can I do for you?",
        "Welcome, what can I do for you?"],
    '00 What is AidaBot?': [
        "I'm your personal assistant",
        "I'm a bot and I can answer to query about Scholarly Data."],
    '00 What are you doing?': [
        "I'm waiting for your queries.",
        "I'm here to answer your questions."],
    '00 What do you do?': [
        "I'm waiting for your queries.",
        "I'm here to answer your questions."],
    '00 Goodbye': [
        'Goodbye!',
        'Bye!',
        'See you later!'],
    '00 bye': [
        'Goodbye!',
        'Bye!',
        'See you later!'],
    '00 What do you suggest?': [
        "I suggest you to start with a new query, like 'count papers on machine learning'",
        "I suggest you a new query, like 'list top papers on rdf graph'"],
    '00 I like it': [
        "Thanks, I really appreciate it.",
        "Thank you so much."],
    '00 Thanks': [
        "Thanks to you as well",
        "You are welcome",
        "I really appreciate it."],
    '00 Thank you': [
        "Thanks to you as well",
        "You are welcome",
        "I really appreciate it."],
    "00 I hate you": ["No you don't"],
    '00 I think you are awsome': [
        "Thanks, I really appreciate it.",
        "Thank you so much.",
        "Thanks, you are very kind"],
    "00 What can you do for me?": [
        "I can count, list, describe or compare authors, conferences or organizations.",
        "I can answer yous queries about Scholarly Data."],
    "How old are you?": ["I think I am 4", "I am very young"],
    "00 When was the semantic web born": [
        "Berners-Lee had envisioned the Semantic Web by at least 1994, only a few years after he began developing the WWW in 1989. He unveiled his idea for the Semantic Web at the First International WWW Conference, held in 1994, which resulted in the formation of the W3C.",
        "Berners-Lee had envisioned the Semantic Web by at least 1994 but he originally expressed his vision of the Semantic Web in 1999 as follows: I have a dream for the Web [in which computers] become capable of analyzing all the data on the Web – the content, links, and transactions between people and computers. A \"Semantic Web\", which makes this possible, has yet to emerge, but when it does, the day-to-day mechanisms of trade, bureaucracy and our daily lives will be handled by machines talking to machines. The \"intelligent agents\" people have touted for ages will finally materialize."]

}

preps = {
    'author': {
        'topics': 'who have written papers on',
        'conferences': ['who have written papers for', "of the conference", "of"],
        'organizations': ['from the', "come from", "from"],
        'authors': '',
        'journals': 'with papers published by'
    },
    'paper': {
        'topics': ['on', 'about', 'with topic'],
        'conferences': ['accepted at', 'at', 'in', 'in the conference', 'at the', 'were published by', 'published by'],
        'organizations': ['written by authors from', 'from'],
        'authors': ['written by', 'by', 'published by'],
        'journals': ['published by', "at", 'in', 'in the journal', 'are published by', 'were published by']
    },
    'conference': {
        'topics': ['with papers on', 'on', 'in'],
        'conferences': '',
        'organizations': ['with papers by authors from', 'with authors from the'],
        'authors': ['with papers written by ', 'with papers by'],
        'journals': ''
    },
    'organization': {
        'topics': 'with papers on',
        'conferences': 'with papers at',
        'organizations': '',
        'authors': 'with',
        'journals': 'with papers published by'
    },
    'topic': {
        'topics': '',
        'conferences': ['of papers from', 'of'],
        'organizations': ['of papers written by authors from the', 'of'],
        'authors': ['of papers written by the author', 'of'],
        'journals': ['of papers published by', 'of']
    },
    'citation': {
        'topics': 'of papers on',
        'conferences': 'of papers from',
        'organizations': 'of papers written by authors from ',
        'authors': 'of papers written by the author',
        'journals': 'of papers published by'
    },
    'journal': {
        'topics': 'with papers on',
        'conferences': '',
        'organizations': 'with papers by authors from',
        'authors': ['with papers of', "have papers by", "with papers by"],
        'journals': ''
    }
}

# TODO save embeddings of personal questions to speed up answer time?

tags_list = ['<b>', '</b>', '<i>', '</i>', '<br/>', '<li>', '</li>', '<ul>', '</ul>', '<br>']
compare_valid_combinations = ['1-1', '2-2', '2-3', '3-2', '3-3', '4-4']
compare_categories = ['author', 'conference', 'conference', 'organization']
list_verbs = ['is', 'are']
orders = ['publications', 'citations', 'publications in the last 5 years', 'citations in the last 5 years']
list_order = ['publication', 'citation', 'publication in the last 5 years', 'citation in the last 5 years']
templates = {
    'WELCOME_MSG': [
        '<b>Hello!</b> You can ask me to <b>describe</b>, <b>compare</b>, <b>list</b>, or <b>count</b> any '
        'scholarly entity.',
        'Welcome, you can ask me to <b>describe</b>, <b>compare</b>, <b>list</b>, or <b>count</b> any '
        'scholarly entity. What would you like to try?'],
    'HELLO_MSG': ['Hello! What can I do for you?', 'Hi! what could i do for you?'],
    'HELP_MSG': [
        'You can ask to <b>count</b> or <b>list</b> <i>authors</i>, <i>papers</i>, <i>conferences</i>, '
        '<i>organizations</i>, <i>topics</i> and <i>citations</i> or to <b>describe</b> or <b>compare</b> '
        '<i>authors</i>, <i>conferences</i> or <i>organizations</i>. Start a query with <b>list</b>, '
        '<b>count</b>, <b>describe</b> or <b>compare</b>.', 'You can ask a query starting with <b>count</b>, '
                                                            '<b>list</b>, <b>describe</b> or <b>compare</b>.'],
    'GOODBYE_MSG': ['Goodbye!', 'Bye!', 'See you later!'],

    'FALLBACK_MSG': 'Sorry, I don\'t know that. Please try again.',
    'ERROR_MSG': 'Sorry, there was an error. Please try again.',

    'HOMONYMS_MSG': 'I found different homonyms (list sorted by number of publications): ${msg} ',

    'SUBJECT_REQUEST_MSG': 'I can count <b>papers</b>, <b>authors</b>, <b>conferences</b>, '
                           '<b>organizations</b> and <b>citations</b>. What do you want me to count?',
    'SUBJECT_WRONG_MSG': "Sorry, I can\'t count <b>${sub}</b>. I can count <b>papers</b>, <b>authors</b>, "
                         "<b>conferences</b>, <b>organizations</b> and <b>citations</b>. What do you prefer?",
    'SUBJECT_REQUEST_REPROMPT_MSG': 'I can count <b>papers</b>, <b>authors</b>, <b>conferences</b>, '
                                    '<b>organizations</b> and <b>citations</b>. What do you prefer?',
    'INSTANCE_MSG': "what is the <b>name</b> of the ${list} whose <b>${sub}</b> I should count? You can say "
                    "<b>all</b> for the full list",
    'INSTANCE2_MSG': "what is the <b>name</b> of the ${list} whose <b>${sub}</b> I should count?",
    'ITEM_MSG': "Searching for <b>${ins}</b>, I got: ${msg}. To choose, say the number. Which one is correct?",
    'INTENT_CONFIRMATION_1_MSG': "Do you want to know how many <b>${sub}</b> ${prep} ${obj} are in the AIDA "
                                 "database?",
    'INTENT_CONFIRMATION_2_MSG': "Do you want to know how many <b>${sub}</b> ${prep} <b>${ins}</b> ${obj} are "
                                 "in the AIDA database?",
    'TOO_GENERIC_MSG': "Your search for <b>${ins}</b> got ${results}. You need to try again and to be more "
                       "specific. Could you tell me the exact name?",
    'NO_RESULT_MSG': "Your search for ${ins} got no result. You need to try again. What could I search for"
                     "you?",
    'QUERY_1_MSG': "I found <b>${num}</b> ${sub} ${prep} <b>${ins}</b> ${obj}. You can ask to perform another "
                   "query on the data contained in the AIDA database or ask for Help. What would you like to "
                   "try?",
    'QUERY_2_MSG': "I found <b>${num}</b> different ${sub} ${prep} ${obj}. You can ask to perform another "
                   "query on the data contained in the AIDA database or ask for Help. What would you like to "
                   "try?",

    'REPROMPT_MSG': 'So, what would you like to ask?',
    'NO_QUERY_MSG': 'Sorry, you asked for a query that is not yet implemented. You can ask to perform another '
                    'query on the data contained in the AIDA database or ask for Help. What would you like to '
                    'try?',

    'REPROMPT_END_MSG': 'You could ask me for another query or say stop to quit',
    'NO_SENSE_MSG': 'I\'m sorry but the query resulting from the chosen options doesn\'t make sense. Try '
                    'again. You can ask to perform another query on the data contained in the AIDA database '
                    'or ask for help. What would you like to try?',

    'LIST_WRONG_NUMBER_MSG': 'The number ${num} , you should tell me a number higher than one and smaller '
                             'than eleven.',
    'LIST_SUBJECT_REQUEST_MSG': 'I can list <b>papers</b>, <b>authors</b>, <b>conferences</b>, '
                                '<b>organizations</b> and <b>topics</b>. What do you want me to list?',
    'LIST_SUBJECT_WRONG_MSG': 'Sorry, I can\'t list <b>${sub}</b>. I can list <b>papers</b>, <b>authors</b>, '
                              '<b>conferences</b>, <b>organizations</b> and <b>topics</b>. What do you prefer?',
    'LIST_SUBJECT_REQUEST_REPROMPT_MSG': 'I can list <b>papers</b>, <b>authors</b>, <b>conferences</b>, '
                                         '<b>organizations</b> and <b>topics</b>. What do you prefer?',
    'LIST_ORDER_MSG': 'Which sorting option do you prefer between: (1) <b>publications</b>, '
                      '(2) <b>citations</b>, (3) <b>publications in the last 5 years</b> and (4) <b>citations '
                      'in the last 5 years</b>?',
    'LIST_PAPERS_ORDER_MSG': 'Which sorting option do you prefer between: (1) <b>citations</b> and (2) '
                             '<b>citations in the last 5 years?</b>',

    'LIST_PAPERS_ORDER_WRONG_MSG': 'Sorry, I can\'t list <b>${sub}</b> sorted by <b>${order}</b>. I can sort '
                                   'them by (1)  <b>citations</b> and by (2) <b>citations in the last 5 '
                                   'years</b>. What do you prefer?',
    'LIST_ORDER_WRONG_MSG': 'Sorry, I can\'t list <b>${sub}</b> sorted by <b>${order}</b>. I can sort them '
                            'by: (1) <b>publications</b>, (2) <b>publications in the last 5 years</b>, '
                            '(3) <b>citations</b>, (4) <b>citations in the last 5 years</b>. What do you '
                            'prefer?',
    'LIST_INSTANCE_MSG': 'what is the <b>name</b> of the ${list} for which <b>${sub}</b> should be in the top '
                         '${num}? You can say <b>all</b> for the full list',
    'LIST_INTENT_CONFIRMATION_1_MSG': 'Do you want to know which are the top ${num} <b>${sub}</b> ${prep} ${'
                                      'obj}, by number of <b>${order}</b>, in the AIDA database?',
    'LIST_INTENT_CONFIRMATION_2_MSG': 'Do you want to know which are the top ${num} <b>${sub}</b>, by number '
                                      'of <b>${order}</b>, ${prep} <b>${ins}</b> ${obj} in the Aida database?',
    'LIST_QUERY_MSG': 'In the AIDA database, the top ${num} <b>${sub}</b> requested ${props} <b>${order}</b> ${verb}: '
                      '${lst} You can ask to perform another query or ask for Help. What would you like to try?',
    'LIST_NO_RESULT_MSG': 'I\'m sorry but in the AIDA database, there are no <b>${sub}</b> with the requested '
                          'properties ${props}. You can ask another query or ask for help. What would you like to try?',
    'DSC_TOO_GENERIC_MSG': 'Your search for <b>${ins}</b> got ${results}. You need to try again and to be more '
                           'specific. What is the <b>name</b> of the <b>author</b> or <b>conference</b> or '
                           '<b>organization</b> you want to know about?',
    'DSC_NO_RESULT_MSG': 'Your search for <b>${ins}</b> got no result. You need to try again. What is the '
                         'name of the author or conference you want to know about?',
    'DESCRIBE_INSTANCE_MSG': 'What is the <b>name</b> of the <b>author</b> or <b>conference</b> or '
                             '<b>organization</b> you want to know about?',
    'DESCRIBE_CONFIRM_MSG': 'Do you want to know something about ${ins}?',

    'COMPARE_FIRST_INSTANCE': 'What is the <b> name </b> of the <b> author </b> or <b> conference </b> or <b> '
                              'organization </b> that you want to use as the first term for the comparison?',
    'COMPARE_SECOND_INSTANCE': [
        'What is the <b> name </b> of the <b> ${obj} </b> that you want to use as the second term for the '
        'comparison?',
        'What is the <b> name </b> of the <b> ${obj} </b> that you want to compare to ${ins}?'],
    'COMPARE_CONFIRM_MSG': 'Do you want to compare ${ins} to ${ins2}?',
    'COMPARE_TOO_GENERIC_FIRST_MSG': 'Your search for <b>${ins}</b> got ${results}. You need to try again and '
                                     'to be more specific. What is the <b> name </b> of the <b> author </b> '
                                     'or <b> conference </b> or <b> organization </b> that you want to use as '
                                     'the first term for the comparison? <br/>(for organizations you can '
                                     'enter the grid id)',
    'COMPARE_TOO_GENERIC_SECOND_MSG': 'Your search for <b>${ins}</b> got ${results}. You need to try again '
                                      'and to be more specific. What is the <b> name </b> of the <b> ${obj} '
                                      '</b> that you want to use as the second term for the comparison? '
                                      '<br/>(for organizations you can enter the grid id)',
    'COMPARE_NO_RESULT_FIRST_MSG': 'Your search for <b>${ins}</b> got no result. You need to try again. What '
                                   'is the <b> name </b> of the <b> author </b> or <b> conference </b> or <b> '
                                   'organization </b> that you want to use as the first term for the '
                                   'comparison? <br/>(for organizations you can enter the grid id)',
    'COMPARE_NO_RESULT_SECOND_MSG': 'Your search for <b>${ins}</b> got no result. You need to try again. What '
                                    'is the <b> name </b> of the <b> ${obj} </b> that you want to use as the '
                                    'second term for the comparison? <br/>(for organizations you can enter the '
                                    'grid id)',
    'COMPARE_SAME_OBJ_MSG': "Hey, it's the same <b>${obj1}</b>! You can try two different ones or ask for "
                            "Help. <br/>What would you like to try?",
    'COMPARE_WRONG_PAIR_MSG': "Hey, I can't compare <b>${obj1}</b> to <b>${obj2}</b>! You can try two "
                              "entities of the same type or ask for Help. <br/>What would you like to try?",
    'COMPARE_AUTHORS_MSG': "<b>${author1}</b> has ${publications}, ${citations} and ${h_index} <b>${"
                           "author2}</b> ${h_index_compare}. ${topics}<br/>You can ask to perform another "
                           "query on the data contained in the AIDA database or ask for Help. <br/>What would "
                           "you like to try?",
    'COMPARE_CONFERENCE_MSG': "${conf1} started ${years} ${conf2} and has ${citations} citations in the last "
                              "5 years. ${name1} also has ${h5_index} ${name2} ${h_index_compare}. ${"
                              "topics}<br/>You can ask to perform another query on the data contained in the "
                              "AIDA database or ask for help. <br/>What would you like to try?",
    'COMPARE_ORGANIZATIONS_MESSAGE': "${org1} has ${publications} and ${citations} in the last 5 years. It "
                                     "also has ${h5_index} ${org2} ${h_index_compare}. ${topics}<br/>You can "
                                     "ask to perform another query on the data contained in the AIDA database "
                                     "or ask for Help. <br/>What would you like to try?",
    'COUNT1': ["The <b>${sub}</b> with the required properties ${props} ${verb} <b>${num}</b>. "
               "What else would you like to try?",
               "In the AIDA database there ${verb} <b>${num} ${sub}</b> with the required properties ${props}. "
               "What else would you like to ask?"],
    'COUNT2': ["In the AIDA database there are <b>${num} ${sub}</b>. You can ask another query or ask for help. "
               "What would you like to try?", "I found <b>${num} ${sub}</b>. What else would you like to ask?"],
    'COUNT0': "I'm sorry. There is <b>no ${sub}</b> with the properties you required ${props}. "
              "What else would you like to try?",

    'FREE': '${msg}'
}
dsc_list = [' is an author', ' affiliated to ', ' affiliated to the ', 'Author rating: ', 'Publications: ',
            'Citations: ', 'Total number of co-authors: ', 'The top topic in terms of publications is: ',
            'The top topics in terms of publications are: ', 'The top conference in terms of publications is: ',
            'The top conferences in terms of publications are: ', 'The top journal in terms of publications is: ',
            'The top journals in terms of publications are: ', ', acronym of ', ', is a conference',
            'The rankings are: ', 'citations in the last 5 years: ', 'Years of activity: from ', ' to ',
            'Number of publications in the last year: ', 'The top country in terms of publications is: ',
            'The top countries in terms of publications are: ', 'The top organization in education is: ',
            'The top organizations in education are: ', 'The top organization in industry is: ',
            'The top organizations in industry are: ', 'publications in the last 5 years: ',
            'number of affiliated authors: ', ', active between ', ' whose main topics are: ', ' active since ']

numbers = [
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
    'eight',
    'nine', 'ten', '1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th', 'first', 'second',
    'third',
    'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth'
]

obj_cat = [
    ['authors', 'conference acronyms', 'conference names', 'organizations'],
    ['topics', 'conferences', 'organizations', 'authors', 'papers', 'journals']
]

homonyms_list = [' affiliated with ', ' author of the paper: ', ', author with ', ' publications']


def upper_first(str_):
    s = str_.split(' ')
    return " ".join([x.capitalize() for x in s])


def set_message(msg, options=None):
    value = templates[msg]
    if isinstance(value, list):
        value = value[int(random.randint(0, len(value) - 1))]
    lst_ = []
    if options is not None and len(options) > 0:
        n = -1
        str0 = value
        while '$' in str0:
            n = str0.index('$', n + 1)
            n1 = str0.index("}", n) + 1
            str1 = str0[n: n1]
            lst_.append(str1)
            str0 = str0[0:n] + str0[n + 1: len(str0)]

        for i in options:
            if isinstance(options[i], list):
                value = value.replace('${' + i + '}', str(options[i][0]))
            else:
                value = value.replace('${' + i + '}', str(options[i]))
        for i in lst_:
            value = value.replace(i, '')
    while '  ' in value:
        value = value.replace('  ', ' ')

    value = value.replace(' . ', '. ')
    value = value.replace(' , ', ', ')
    return value


def list2string(word_list):
    n = len(word_list)
    word_string = ''
    for i in range(len(word_list)):
        if i < n - 2:
            word_string += word_list[i] + ', '
        elif i < n - 1:
            word_string += word_list[i] + ', and '
        else:
            word_string += word_list[i]
    return word_string


def cmp(term1, term2):
    if term1['result'] != 'ok' or term1.get('k2') is not None or term2['result'] != 'ok' or term2.get('k2') is not None:
        return ""
    comb = str(term1['obj_id']) + '-' + str(term2['obj_id'])
    if comb not in compare_valid_combinations:
        params = {'obj1': term1['object'], 'obj2': term2['object']}
        return set_message('COMPARE_WRONG_PAIR_MSG', params)
    if term1['item'] == term2['item']:
        params = {'obj1': compare_categories[term1['obj_id'] - 1]}
        return set_message('COMPARE_SAME_OBJ_MSG', params)

    parameters = {}

    if term1['obj_id'] == 1:
        pub_diff = term1['item']['publications'] - term2['item']['publications']
        parameters['author1'] = upper_first(term1['item']['name'])
        parameters['author2'] = upper_first(term2['item']['name'])
        if pub_diff != 0:
            publications = str(abs(pub_diff)) + (' fewer ' if pub_diff < 0 else ' more ') + 'publications ('
            publications += str(term1['item']['publications']) + ' vs ' + str(term2['item']['publications']) + ')'
        else:
            publications = 'the same number of publications ' + '(' + str(term1['item']['publications']) + ')'
        parameters['publications'] = publications

        cit_diff = term1['item']['citations'] - term2['item']['citations']
        if cit_diff != 0:
            citations = str(abs(cit_diff)) + (' fewer ' if cit_diff < 0 else ' more ') + 'citations ('
            citations += str(term1['item']['citations']) + ' vs ' + str(term2['item']['citations']) + ')'
        else:
            citations = 'the same number of citations ' + '(' + str(term1['item']['citations']) + ')'

        parameters['citations'] = citations

        h_index_diff = term1['item']['h-index'] - term2['item']['h-index']
        if h_index_diff != 0:
            h_index = 'an h-index ' + str(abs(h_index_diff)) + ' points'
            h_index += ' lower than ' if h_index_diff < 0 else ' higher than '
            h_index_compare = ' (' + str(term1['item']['h-index']) + ' vs '
            h_index_compare += str(term2['item']['h-index']) + ') '
        else:
            h_index = 'the same h-index as '
            h_index_compare = ' (' + str(term1['item']['h-index']) + ') '

        parameters['h_index'] = h_index
        parameters['h_index_compare'] = h_index_compare

        topics = ''
        name1 = '<b>' + parameters['author1'].split(' ')[0] + '</b>'
        name2 = '<b>' + parameters['author2'].split(' ')[0] + '</b>'
        topics1 = term1['item']['top_pub_topics']
        topics2 = term2['item']['top_pub_topics']
        topic_analysis = commons(topics1, topics2, term1['obj_id'])

        if len(topic_analysis['commons']) > 0:
            topics = 'Both of them work in ' + list2string(topic_analysis['commons']) + '. '

        if len(topic_analysis['first']) > 0 and len(topic_analysis['second']) > 0:
            topics += name1 + ' focuses more on ' + list2string(topic_analysis['first'])
            topics += ' while ' + name2 + ' focuses more on ' + list2string(topic_analysis['second']) + '. '
        elif len(topic_analysis['first']) > 0:
            topics += name1 + ' focuses more on ' + list2string(topic_analysis['first']) + '. '
        elif len(topic_analysis['second']) > 0:
            topics += name2 + ' focuses more on ' + list2string(topic_analysis['second']) + '. '

        parameters['topics'] = topics
        return set_message('COMPARE_AUTHORS_MSG', parameters)

    if term1['obj_id'] == 2 or term1['obj_id'] == 3:
        conf1 = '<b>'
        conf2 = '<b>'
        if 'conference' in term1['item']['name'].lower():
            conf1 += term1['item']['name'] + '</b>'
        else:
            conf1 += term1['item']['name'] + '</b> conference'

        if 'conference' in term2['item']['name'].lower():
            conf2 += term2['item']['name'] + '</b>'
        else:
            conf2 += term2['item']['name'] + '</b> conference'

        parameters['conf1'] = conf1
        parameters['conf2'] = conf2
        parameters['name1'] = '<b>' + term1['item']['acronym'] + '</b>'
        parameters['name2'] = '<b>' + term2['item']['acronym'] + '</b>'

        activity_diff = term1['item']['activity_years']['from'] - term2['item']['activity_years']['from']
        num = str(abs(activity_diff)) + (' years after ' if activity_diff < 0 else ' years before ')
        if activity_diff == 0:
            num = 'the same year as'

        parameters['years'] = num

        cit_diff = term1['item']['citationcount_5'] - term2['item']['citationcount_5']
        citations = str(abs(cit_diff)) + (' less ' if cit_diff < 0 else ' more ')
        if cit_diff == 0:
            citations = 'the same number of'

        parameters['citations'] = citations

        h_index_diff = term1['item']['h5_index'] - term2['item']['h5_index']
        if h_index_diff != 0:
            h_index = 'an h5-index ' + str(abs(h_index_diff)) + ' points'
            h_index += (' lower than ' if h_index_diff < 0 else ' higher than ')
            h_index_compare = '(' + str(term1['item']['h5_index']) + ' vs ' + str(term2['item']['h5_index']) + ') '
        else:
            h_index = 'the same h-index as '
            h_index_compare = ' (' + str(term1['item']['h5_index']) + ') '

        parameters['h5_index'] = h_index
        parameters['h_index_compare'] = h_index_compare

        topics1 = term1['item']['topics']
        topics2 = term2['item']['topics']
        topic_analysis = commons(topics1, topics2, term1['obj_id'])
        topics = ''
        if len(topic_analysis['commons']) > 0:
            topics = 'Both have ' + list2string(topic_analysis['commons']) + ' as their main topic'

        if len(topic_analysis['commons']) > 1:
            topics += 's. '
        elif len(topic_analysis['commons']) > 0:
            topics += '. '

        if len(topic_analysis['first']) > 0 and len(topic_analysis['second']) > 0:
            topics += parameters['name1'] + ' focuses more on '
            topics += list2string(topic_analysis['first']) + ' while ' + parameters['name2']
            topics += ' focuses more on ' + list2string(topic_analysis['second']) + '. '
        elif len(topic_analysis['first']) > 0:
            topics += parameters['name1'] + ' focuses more on ' + list2string(topic_analysis['first']) + '. '
        elif len(topic_analysis['second']) > 0:
            topics += parameters['name2'] + ' focuses more on ' + list2string(topic_analysis['second']) + '. '

        parameters['topics'] = topics

        return set_message('COMPARE_CONFERENCE_MSG', parameters)

    if term1['obj_id'] == 4:
        parameters['org1'] = '<b>' + term1['item']['name'] + '</b>'
        parameters['org2'] = '<b>' + term2['item']['name'] + '</b>'

        pub_diff = term1['item']['publications_5'] - term2['item']['publications_5']

        if pub_diff != 0:
            publications = str(abs(pub_diff)) + (' fewer ' if pub_diff < 0 else ' more ') + 'publications ('
            publications += str(term1['item']['publications_5']) + ' vs '
            publications += str(term2['item']['publications_5']) + ')'
        else:
            publications = 'the same number of publications ' + '(' + str(term1['item']['publications_5']) + ')'

        parameters['publications'] = publications

        cit_diff = term1['item']['citations_5'] - term2['item']['citations_5']

        if cit_diff != 0:
            citations = str(abs(cit_diff)) + (' fewer ' if cit_diff < 0 else ' more ') + 'citations ('
            citations += str(term1['item']['citations_5']) + ' vs ' + str(term2['item']['citations_5']) + ')'
        else:
            citations = 'the same number of citations ' + '(' + term1['item']['citations_5'] + ')'

        parameters['citations'] = citations

        h_index_diff = term1['item']['h5-index'] - term2['item']['h5-index']
        if h_index_diff != 0:
            h_index = 'an h-index ' + str(abs(h_index_diff)) + ' points'
            h_index += (' lower than ' if h_index_diff < 0 else ' higher than ')
            h_index_compare = '(' + str(term1['item']['h5-index']) + ' vs ' + str(term2['item']['h5-index']) + ') '
        else:
            h_index = 'the same h-index as '
            h_index_compare = ' (' + str(term1['item']['h-index']) + ') '

        parameters['h5_index'] = h_index
        parameters['h_index_compare'] = h_index_compare

        topics = ''
        topics1 = term1['item']['top_3_topics']
        topics2 = term2['item']['top_3_topics']
        topic_analysis = commons(topics1, topics2, term1['obj_id'])

        if len(topic_analysis['commons']) > 0:
            topics = 'Both have ' + list2string(topic_analysis['commons']) + ' as their main topic'

        if len(topic_analysis['commons']) > 1:
            topics += 's. '
        elif len(topic_analysis['commons']) > 0:
            topics += '. '

        if len(topic_analysis['first']) > 0 and len(topic_analysis['second']) > 0:
            topics += parameters['org1'] + ' focuses more on ' + list2string(topic_analysis['first'])
            topics += ' while ' + parameters['org2'] + ' focuses more on '
            topics += list2string(topic_analysis['second']) + '. '
        elif len(topic_analysis['first']) > 0:
            topics += parameters['org1'] + ' focuses more on ' + list2string(topic_analysis['first']) + '. '
        elif len(topic_analysis['second']) > 0:
            topics += parameters['org2'] + ' focuses more on ' + list2string(topic_analysis['second']) + '. '

        parameters['topics'] = topics

        return set_message('COMPARE_ORGANIZATIONS_MESSAGE', parameters)
    return ""


def commons(l1, l2, obj_id):
    common_topics = []
    topics1 = []
    topics2 = []
    if obj_id == 1:
        for topic in l1:
            for topic2 in l2:
                if topic['topic'] == topic2['topic']:
                    common_topics.append(topic['topic'])

        for topic in l1:
            if topic['topic'] not in common_topics:
                topics1.append(topic['topic'])

        for topic in l2:
            if topic['topic'] not in common_topics:
                topics2.append(topic['topic'])

    else:
        for topic in l1:
            if topic in l2:
                common_topics.append(topic)

        for topic in l1:
            if topic not in common_topics:
                topics1.append(topic)

        for topic in l2:
            if topic not in common_topics:
                topics2.append(topic)

    return {'commons': common_topics, 'first': topics1, 'second': topics2}


def answer_clean(answer):
    for i in range(len(tags_list)):
        answer = answer.replace(tags_list[i], '')
    answer = answer.replace("[$]", ', ')
    answer = answer.replace("[$1]", '. ')
    return answer


def dsc(query):
    msg = ''
    item = query['item']
    if query['obj_id'] == 1:
        msg += '<b>' + upper_first(item['name']) + '</b>' + dsc_list[0]
        if item['last_affiliation'].get('affiliation_name') is not None:
            s = item['last_affiliation']['affiliation_name'].split(' ')
            msg += (dsc_list[1] if (s[0] == 'the' or s[0] == 'The') else dsc_list[2])
            msg += '<b>' + item['last_affiliation']['affiliation_name'] + '</b>'
            if item['last_affiliation'].get('affiliation_country') is not None:
                msg += ', ' + item['last_affiliation']['affiliation_country'] + '. <br><br>'
        else:
            msg += '. <br><br>'

        msg += dsc_list[3]
        msg += "<ul>"
        if item.get('publications') is not None:
            msg += "<li>"
            msg += dsc_list[4] + '<b>' + str(item['publications']) + '</b>' + '</li>'
        if item.get('citations') is not None:
            msg += "<li>"
            msg += dsc_list[5] + '<b>' + str(item['citations']) + '</b>' + '</li>'
        if item.get('h-index') is not None:
            msg += "<li>"
            msg += 'h-index: ' + '<b>' + str(item['h-index']) + '</b>' + '</li>'
        if item.get('h5-index') is not None:
            msg += "<li>"
            msg += 'h5-index: ' + '<b>' + str(item['h5-index']) + '</b>' + '</li>'
        if item.get('co_authors') is not None and item['co_authors'] > 0:
            msg += "<li>"
            msg += dsc_list[6] + '<b>' + str(item['co_authors']) + '</b>' + '</li>'

        msg += "</ul>"

        if item.get('top_pub_topics') is not None and len(item['top_pub_topics']) > 0:
            msg += dsc_list[7] if len(item['top_pub_topics']) == 1 else dsc_list[8]
            msg += list_elements(item['top_pub_topics'], 'topic')
        if item.get('top_pub_conf') is not None and len(item['top_pub_conf']) > 0:
            msg += dsc_list[9] if len(item['top_pub_conf']) == 1 else dsc_list[10]
            msg += list_elements(item['top_pub_conf'], 'name')
        if item.get('top_journals') is not None and len(item['top_journals']) > 0:
            msg += dsc_list[11] if len(item['top_journals']) == 1 else dsc_list[12]
            msg += list_elements(item['top_journals'], 'name')

    elif query['obj_id'] == 2 or query['obj_id'] == 3:
        msg += '<b>' + item['acronym'] + '</b>' + dsc_list[13] + '<b>' + item['name'] + '</b>' + dsc_list[
            14]
        if item.get('activity_years') is not None:
            msg += dsc_list[30] + '<b>' + str(item['activity_years']['from']) + '</b>,'
            # msg += ' and ' + '<b>' + str(item['activity_years']['to']) + '</b>, '
        msg += dsc_list[29]
        msg += list_elements(item['topics'], '')

        msg += dsc_list[15] + '<ul>'

        if item.get('h5_index') is not None:
            msg += "<li>"
            msg += 'h5-index: ' + '<b>' + str(item['h5_index']) + '</b> ' + '</li>'
        if item.get('citationcount_5') is not None:
            msg += "<li>"
            msg += dsc_list[16] + '<b>' + str(item['citationcount_5']) + '</b> ' + '</li>'
        if item.get('last_year_publications') is not None and item['last_year_publications'] > 0:
            msg += "<li>"
            msg += dsc_list[19] + '<b>' + str(item['last_year_publications']) + '</b> ' + '</li>'

        msg += '</ul>'

        if len(item['top_3_country']) > 0:
            msg += dsc_list[20] if len(item['top_3_country']) == 1 else dsc_list[21]
            msg += list_elements(item['top_3_country'], '')
        if len(item['top_3_edu']) > 0:
            msg += dsc_list[22] if len(item['top_3_edu']) == 1 else dsc_list[23]
            msg += list_elements(item['top_3_edu'], '')
        if len(item['top_3_company']) > 0:
            msg += dsc_list[24] if len(item['top_3_company']) == 1 else dsc_list[25]
            msg += list_elements(item['top_3_company'], '')

    elif query['obj_id'] == 4:
        msg += '<b>' + item['name'] + '</b>'
        msg += ((' -<b> ' + item['country'] + ' </b>-') if item.get('country') is not None else '')
        msg += ' is an organization'
        if item.get('type') is not None and (item['type'] == 'academia' or item['type'] == 'industry'):
            msg += ' in <b>' + ('Edu' if item['type'] == 'academia' else 'Industry') + '</b> sector. ' + '<br><br>'
        else:
            msg += '. ' + '<br><br>'
        msg += dsc_list[15] + '<br>' + "<ul>"
        if item.get('h5-index') is not None:
            msg += "<li>"
            msg += 'h5-index: <b>' + str(item['h5-index']) + '</b>. ' + '</li>'
        if item.get('publications_5') is not None:
            msg += "<li>"
            msg += dsc_list[26] + '<b>' + str(item['publications_5']) + '</b> ' + '</li>'
        if item.get('citations_5') is not None:
            msg += "<li>"
            msg += dsc_list[16] + '<b>' + str(item['citations_5']) + '</b> ' + '</li>'
        if item.get('authors_number') is not None:
            msg += "<li>"
            msg += dsc_list[27] + '<b>' + str(item['authors_number']) + '</b> ' + '</li>'
        msg += "</ul>"
        if item.get('top_3_topics') is not None and len(item['top_3_topics']) > 0:
            msg += dsc_list[7] if len(item['top_3_topics']) == 1 else dsc_list[8]
            msg += list_elements(item['top_3_topics'], '')
        if item.get('top_3_conferences') is not None and len(item['top_3_conferences']) > 0:
            msg += dsc_list[9] if len(item['top_3_conferences']) == 1 else dsc_list[10]
            msg += list_elements(item['top_3_conferences'], '')
        if item.get('top_3_journals') is not None and len(item['top_3_journals']) > 0:
            msg += dsc_list[11] if len(item['top_3_journals']) == 1 else dsc_list[12]
            msg += list_elements(item['top_3_journals'], '')
    else:
        msg = {'result': 'ko', 'msg': 'Sorry, Query not yet implemented!'}
        return msg

    msg += 'You can ask to perform another query or ask for Help. ' + 'What would you like to try?'
    return msg


def list_elements(lst1, element):
    blacklist = ['computer science', 'lecture notes in computer science', 'arxiv software engineering']
    lst_ = []
    if len(element) > 0:
        for i in lst1:
            if i[element] not in blacklist:
                lst_.append(i)
    else:
        for i in lst1:
            if i not in blacklist:
                lst_.append(i)

    i = len(lst_)
    if i > 3:
        i = 3
    msg = '<ul>'
    j = 0
    while j < i:
        if len(element) > 0:
            msg += '<li><b>"' + upper_first(lst_[j][element]) + '"</b>' + "</li>"
        else:
            msg += '<li><b>"' + upper_first(lst_[j]) + '"</b> ' + "</li>"
        j += 1
    return msg + '</ul>'


def list_query_answer(result):
    # print(result)
    sub = result.get("subject")
    # num = result.get("num")
    order = orders[int(result.get("order")) - 1]
    req_props = result.get("req_props")

    if result['result'] == 'ok' and result.get('lst') is not None and len(result['lst']) > 0:
        msg_num = '' if len(result['lst']) == 1 else len(result['lst'])
        msg_sub = sub[:-1] if len(result['lst']) == 1 else sub
        ord_ = "" if len(result['lst']) == 1 else "- sorted by number of " + order + " - "
        # noinspection PyTypeChecker
        options = {'order': ord_, 'num': msg_num, 'sub': msg_sub,
                   'verb': (list_verbs[0] if len(result['lst']) == 1 else list_verbs[1]),
                   'lst': get_lst_str(result, order, sub),
                   "props": get_req_props(req_props)}
        return set_message('LIST_QUERY_MSG', options)

    elif result.get("lst") is not None and len(result['lst']) == 0:
        return set_message('LIST_NO_RESULT_MSG', {'sub': sub, "props": get_req_props(req_props)})

    else:
        return {'result': 'ko', 'msg': 'Sorry, Query not yet implemented!'}  # set_message('NO_QUERY_MSG')


def get_lst_str(result, order, sub):
    o = orders.index(order)
    msg = '<ul>'
    lst_ = result['lst']
    if o == 1 or o == 3:
        for i in lst_:
            author = ''
            if i.get('author') is not None and len(i['author']) > 0:
                author = '</b> by <b>' + upper_authors(i['author']) + '</b>'
            if i['citations'] == 1:
                i['citations'] = '1'
                ord_ = list_order[o]
            else:
                ord_ = order
            msg += '<li><b>'
            msg += upper_first(i['name']) if sub == 'authors' else i['name'][0].upper() + i['name'][1:]
            msg += author + '</b> with <b>' + str(int(i['citations'])) + '</b> ' + ord_.split(' ')[0] + '[$]</li>'
    else:
        for i in lst_:
            if i['papers'] == 1:
                i['papers'] = '1'
                ord_ = list_order[o]
            else:
                ord_ = order
            msg += '<li><b>'
            msg += upper_first(i['name']) if sub == 'authors' else i['name'][0].upper() + i['name'][1:]
            msg += '</b> with <b>' + str(i['papers']) + '</b> ' + ord_.split(' ')[0] + '[$]</li>'
    return msg[:-8] + '[$1]</li></ul>'


def upper_authors(string):
    if ' et al.' in string:
        author = string.replace(' et al.', '')
        author = upper_first(author)
        return author + ' et al.'
    return upper_first(string)


def count_query_answer(result):
    if result.get('result') == 'ok':
        sub = result.get("subject")
        num = int(result.get("hits"))
        req_props = result.get("req_props")
        options = {"sub": sub if num != 1 else sub[:-1],
                   "num": num if num != 1 else "only one",
                   "verb": (list_verbs[0] if num == 1 else list_verbs[1]),
                   "props": get_req_props(req_props)
                   }
        if result["all"]:
            return set_message('COUNT2', options)
        if num > 0:
            return set_message('COUNT1', options)
        if num == 0:
            return set_message('COUNT0', options)


def choice_list(speak):
    message = ''
    n = 0
    cmd = 0 if speak.get('cmd') is not None else 1
    for i in range(len(speak['num'])):
        if speak['num'][i] > 0:
            for j in range(len(speak['keys'][i])):
                message += numbers[n] + ', <b>'
                item = speak['keys'][i][j]['name'] if cmd == 0 else speak['keys'][i][j]
                if (cmd == 1 and i == 3) or (cmd == 0 and i == 0):
                    item = upper_first(item)
                message += item + '</b>; '
                n += 1
            message += ' among the ' + obj_cat[cmd][i] + '. '
    message = message[0: len(message) - 2]
    return message


def k2_message(msg):
    ins = msg.get("orig_ins")
    return set_message('ITEM_MSG', {'ins': ins, 'msg': choice_list(msg)})


def ka_message(msg):
    return set_message('HOMONYMS_MSG', {'msg': homonyms(msg)})


def homonyms(speak):
    msg = ''
    item = speak['item']
    for i in range(len(item)):
        num = 'say <b>' + numbers[i] + '</b> for <b>'
        name = item[i]['name']
        if speak.get('object') == 'authors':
            name = upper_first(name)
        msg += num + name + '</b>'
        if item[i].get('affiliation') is not None:
            msg += homonyms_list[0] + item[i]['affiliation'] + "; "
        elif item[i].get('country') is not None:
            msg += ' - ' + item[i]['country'] + "; "
        elif item[i].get('paper') is not None:
            msg += homonyms_list[1] + item[i]['paper'] + "; "
        elif item[i].get('publications') is not None:
            msg += homonyms_list[2] + str(item[i]['publications']) + homonyms_list[3] + "; "
        else:
            msg += "; "
        if i > 9:
            return msg
    return msg


def ko_message():
    return {"result": "ko", "answer": set_message("FALLBACK_MSG")}


def get_req_props(req_props):
    ans = '('
    for key in req_props:
        ans += key + ": " + ", ".join(req_props.get(key)) + " - "
    ans = ans[:-3] + ") "
    if "(" not in ans:
        return ""
    return ans


def get_biblio(papers=None):
    if papers is None:
        return []

    paper_titles = []
    paper_authors = []
    paper_dois = []
    paper_urls = []
    paper_years = []

    for pt in papers:
        paper_titles.append((pt.get("title")))
        if pt.get("authors") is not None:
            for author in pt.get("authors"):
                if author.get("order") == 1:
                    name = author.get("name")
                    if len(pt.get("authors"))>1:
                        name += ' et al.'
                    paper_authors.append(upper_authors(name))
        paper_dois.append(pt.get("doi"))
        paper_urls.append(pt.get("urls"))
        paper_years.append(pt.get("year"))

    biblio = []
    for i, title in enumerate(paper_titles):
        if i < len(paper_dois) and i < len(paper_authors) and paper_dois[i] is not None and paper_dois[i] != "":
            row = '<a target = "_blank" href="https://doi.org/'+ paper_dois[i] + '"><b>' + paper_authors[i] + '</b> - <i>' + title.capitalize() + '</i></a>'

        elif i < len(paper_authors):
            row = "<b>" + paper_authors[i] + "</b> - <i>" + title.capitalize() + "</i>"
        else:
            row = "<i>" + title.capitalize() + "</i>"

        if paper_years[i] is not None and len(paper_years[i])>4:
            row += " - " + paper_years[i][:4]

        if paper_urls[i] is not None and len(paper_urls[i]) > 0:
            row += '<br>Links: '
            u = []
            for j, url in enumerate(paper_urls[i]):
                u.append('<a target = "_blank" href="' + url + '">['+str(j+1)+']</a>')
            row += " ".join(u)

        biblio.append(row)

    return biblio

def get_neg_ans():
    negative_answers = ["Unfortunately, I have no information on that.",
                        "I apologize, but I couldn't find anything relevant to your query.",
                        "That's a tough one. I searched everywhere but came up empty-handed.",
                        "No luck. I tried my best but there's nothing about it.",
                        "I'm sorry to disappoint you, but I have no clue about that.",
                        "I wish I could help you, but that's beyond my knowledge.",
                        "I feel bad for not knowing, but that's out of my scope.",
                        "I regret to inform you that I have no idea about it"]
    return negative_answers[random.randint(0,len(negative_answers)-1)]