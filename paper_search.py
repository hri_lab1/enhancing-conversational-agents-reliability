import language


def get_paper_info(data=None):
    if data is None:
        return "Sorry, I didn't find anything about it!"

    paper_texts = []
    paper_urls = []
    paper_titles = []
    paper_abstract = []
    paper_authors = []
    paper_dois = []
    if data is not None:
        papers = data.get("papers").get("papers")
        for pt in papers:
            paper_texts.append(pt.get("title") + " - " + pt.get("abstract"))
            paper_urls.append(pt.get("urls"))
            paper_titles.append((pt.get("title")))
            paper_abstract.append(pt.get("abstract"))
            for author in pt.get("authors"):
                if author.get("order") == 1:
                    name = author.get("name")
                    if len(pt.get("authors"))>1:
                        name += ' et al.'
                    paper_authors.append(language.upper_authors(name))
            paper_dois.append(pt.get("doi"))
    if len(paper_texts) == 0:
        return "Sorry, I didn't find anything about it!"

    texts = paper_texts
    urls = paper_urls

    msg = 'I found the following papers: <ul>'

    for i, title in enumerate(paper_titles):
        msg += "<li>"
        if paper_dois[i] is not None and paper_dois[i] != "":
            msg += '<a target = "_blank" href="https://doi.org/' + paper_dois[i] + '"><b>' + paper_authors[
                i] + '</b> - <i>' + title.capitalize() + '</i></a>'
        else:
            msg += "<b>" + paper_authors[i] + "</b> - <i>" + title.capitalize() + "</i>"
        msg += "</li>"

    msg+="</ul> What else can i do for you?"
    final_result = {"text": texts, "source": "Papers", "url": urls, "answer": msg}
    return final_result


