# Enhancing Conversational Agents Reliability and Accuracy by Integrating Scientific Knowledge Graphs



## A method to add "knowledge plugins" to existing chatbots, supporting a set of pre-defined question types by automatically converting them to formal knowledge graph queries, and resolving open questions by incorporating data from pertinent wiki pages, search engines, and KG documents.
