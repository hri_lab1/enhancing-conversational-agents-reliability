import torch

from transformers import pipeline
from sentence_transformers import SentenceTransformer

model = SentenceTransformer('all-MiniLM-L6-v2', device='cuda' if torch.cuda.is_available() else "cpu")

question_answerer = pipeline("question-answering",
                             model='distilbert-base-cased-distilled-squad',
                             device=0 if torch.cuda.is_available() else -1)

summarizer = pipeline("summarization",
                      model="sshleifer/distilbart-cnn-12-6",
                      device=0 if torch.cuda.is_available() else -1)



