import json
import urllib.parse
import urllib.request
import wikipediaapi
from duckduckgo_search import ddg
from sentence_transformers.util import cos_sim

from models import *
from spc_parser import get_sentences


open_domain_threshold = .70


def get_wiki_pages(query):
    try:
        url = "https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&utf8=1&srsearch="
        response = urllib.request.urlopen(url + urllib.parse.quote(str(query)))
        data = json.load(response)
        titles = [{"title": x.get("title"), "pageid": x.get("pageid")} for x in data.get("query").get("search")]
        return titles
    except OSError:
        return []


def wiki(query):
    try:
        wiki_wiki = wikipediaapi.Wikipedia('en')
        titles = get_wiki_pages(query)
        if len(titles) > 0:
            summaries = []
            urls = []
            for title in titles[:5]:
                page_py = wiki_wiki.page(title.get("title"))
                urls.append("http://en.wikipedia.org/?curid={pageid}".format(pageid=title.get("pageid")))
                summaries.append(page_py.summary.replace("\n", " "))

            return [summaries,urls]
        return []
    except:
        return []


def summarize(text):
    text = text[:1500]
    if len(text) > 250:
        max_length = int(len(text) / 6)
        min_length = int(len(text) / 9)
        txt = summarizer(text, max_length=max_length, min_length=min_length)[0]['summary_text']
        txt = get_sentences(txt)

        txt2 = [x for x in txt if not x.endswith("...") and x.endswith(".")]
        if txt2 =="":
            txt = [x for x in txt if not x.endswith("...")]
        else:
            txt = txt2
        txt = [x.replace(" .", ". ").replace("  ", " ").strip().capitalize() for x in txt]
        txt = " ".join(txt)
    else:
        txt = text

    return txt


def open_domain(data=None, question=None, ai="odt"):

    if data is None and question is None:
        return "Sorry I don't know that!"
    elif data is None and question is not None:
        a_ins = [question]
    else:
        a_ins = data.get("sentences")

    query = " ".join(a_ins)

    wiki_data = []
    wiki_data_complete = []
    wiki_data_lists = [wiki(x) for x in a_ins]

    wiki_urls = [x[1] for x in wiki_data_lists if len(x)>1]
    if len(wiki_urls)>0:
        wiki_urls = wiki_urls[0]

    wiki_data_lists = [x[0] for x in wiki_data_lists if len(x)>0]

    min_len = 9999
    wiki_data_sentences = []
    for element in wiki_data_lists:
        for w in element:
            if w not in wiki_data_complete:
                wiki_data_complete.append(w)
                sentences = get_sentences(w)
                lll = len(sentences)
                rimpiazzo = ["", "", ""]
                if lll < 3:
                    sentences = (sentences + rimpiazzo)[:3]
                if len(sentences) < min_len:
                    min_len = len(sentences)
                wiki_data_sentences.append(sentences)

    for element in wiki_data_sentences:
        wiki_data.append(" ".join(element[:min_len]))

    paper_texts = []
    paper_urls = []
    if data is not None:
        papers = data.get("papers").get("papers")
        for pt in papers:
            paper_text = pt.get("title") + " - " + pt.get("abstract")
            paper_urls.append(pt.get("urls"))
            r = get_sentences(paper_text)
            sentences = [str(x) for x in r if
                         ("this paper" not in str(x).lower()) and ("this section" not in str(x).lower())]
            paper_text = " ".join(sentences)
            paper_texts.append(paper_text)


    r = ddg(query, region='wt-wt', safesearch='On', max_results=5)  # time='y'

    if r is None and len(paper_texts) == 0 and len(wiki_data) == 0:
        return "Sorry, I didn't find anything about it!"

    texts = [x.get('title') + " - " + x.get('body') for x in r]
    ddg_urls = [x.get("href") for x in r]

    len_internet = len(texts)

    len_paper = len(paper_texts)

    texts = texts + paper_texts + wiki_data[:len(wiki_urls)]
    urls = ddg_urls + paper_urls + wiki_urls

    if len(texts) == 0:
        return "Sorry, I didn't find anything about it!"

    results = []
    for i, t in enumerate(texts):
        if len(t) == 0:
            result = {"answer": "", "score": 0.0, "text": "", "source":"", "url":""}
            results.append(result)
            continue
        result = question_answerer(question=query, context=t)

        if i < len_internet:
            source = "Internet"
        elif len_internet+len_paper > i >= len_internet:
            source = "Papers"
        else:
            source = "Wikipedia"

        result["text"] = texts[i]
        result["source"] = source
        if i < len(urls):
            result["url"] = urls[i]
        else:
            result["url"]=[]
        results.append(result)

    sorted_results = sorted(results, key=lambda x: x["score"], reverse=True)

    answers = [x.get("answer") for x in sorted_results]
    emb_ans = model.encode(answers)
    scores = cos_sim(emb_ans, emb_ans)

    for i, row_score in enumerate(scores):
        acc = 0
        acc_text = ""
        for j, score in enumerate(row_score):
            if score.item() > open_domain_threshold and j >= i:
                acc += sorted_results[j].get("score")
                acc_text += sorted_results[j].get("text") + " - "
        sorted_results[i]["sum_score"] = acc
        sorted_results[i]["txt"] = acc_text

    sorted_results = sorted(sorted_results, key=lambda x: x["sum_score"], reverse=True)

    if sorted_results[0]["sum_score"] > open_domain_threshold:
        answer = ""
        if sorted_results[0]["score"] > open_domain_threshold:
            answer = sorted_results[0].get("answer") + " - "
        text = sorted_results[0]["txt"]
        answer += summarize(text)
        source = sorted_results[0]["source"]
        url = sorted_results[0]["url"]
        final_result = {"text": text, "source": source, "url": url, "answer": answer}
    else:
        emb_texts = model.encode(texts)
        emb_query = model.encode(query)
        scores = cos_sim(emb_query, emb_texts)
        maximus = torch.max(scores, 1)
        m = float(maximus.values[0])
        i = int(maximus.indices[0])
        if i < len_internet:
            source = "Internet"
        elif len_internet+len_paper > i >= len_internet:
            source = "Papers"
        else:
            source = "Wikipedia"

        answer = summarize(texts[i])
        final_result = {"text": texts[i], "source": source, "url": urls[i], "answer": answer}

    final_result["answer"] = final_result["answer"].strip()
    if final_result.get("answer").endswith("-"):
        final_result["answer"] = final_result["answer"][:-1].strip()

    return final_result

