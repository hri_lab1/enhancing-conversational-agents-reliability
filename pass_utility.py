import json

from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def load_json(file__name):
    try:
        data_file = open(file__name, "r", encoding='utf-8')
        file = json.loads(data_file.read())
        data_file.close()
        return file
    except IOError:
        return {}


def main():
    users = load_json("users.json")
    new_user = {
        "username": "",
        "full_name": "",
        "email": "",
        "hashed_password": "",
        "disabled": False,
    }
    user = ""
    while user != 'exit':
        user = input('Username ')
        if user == 'exit':
            break
        new_user["username"] = user
        while True:
            password = input('Password ')
            re_password = input('Retype Password ')
            if password == re_password:
                break
        new_user["hashed_password"] = pwd_context.hash(password)
        new_user["full_name"] = input('Full name ')
        new_user["email"] = input('e-mail ')
        users[user] = new_user
        with open("users.json", "w", encoding="utf-8") as text_file:
            print(json.dumps(users), file=text_file)


if __name__ == '__main__':
    main()
