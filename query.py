# query the database
from query_parser import *
from aida_parser import Parser
from language import *
parser = Parser(aidabot_grammar, data_server_url, words_to_verify, personal_questions, 0.70)


def query(cmd, sub, obj, ins, order, num, lng, ins2, obj2, ins3, obj3, old, ai="odt", cu=None, expand=False):
    if cu is None:
        pass

    result = json.dumps({'result': 'ko', 'msg': 'Query not implemented yet'})

    if cmd == 'how':
        result = how(sub, obj, ins, ins2, obj2, ins3, obj3)

    elif cmd == 'lst':
        result = lst(sub, obj, ins, ins2, obj2, ins3, obj3, num, order)

    elif cmd == 'dsc':
        result = dsc_finder(ins)

    elif cmd == 'fnd':
        result = find_match(ins)
        data = json.loads(result)
        if data['result'] == 'ok' and data['obj_id'] == 4:
            result = check_author(data)
        if data['result'] == 'ok' and (data['obj_id'] == 2 or data['obj_id'] == 6):
            result = check_conference(data)
        if data['result'] == 'k2':
            result = check_topic(data, ins, old)
        if data['result'] == 'ok' and data['obj_id'] == 1:
            result = check_topic_for_conference(data)

    elif cmd == 'parser':
        result = query_parser(ins, lng)

    elif cmd == "parse":
        try:
            result = json.dumps(parser.parse(ins, ai=ai, expand=expand))
        except Exception as e:
            print(e)
            result = json.dumps({'result': 'ko',
                                 'answer': get_neg_ans()})


    elif cmd == "search":
        result = find(ins)


    return result
